#include "src/file_util.hpp"
#include <atomic>
#include <cassert>
#include <iostream>
#include <numeric>
#include <seqan/parallel/parallel_macros.h>
#include <vector>

using namespace std;

int main(int argc, char** argv)
{
	if (argc != 2)
		exit(1);
	std::vector<std::string> alignment;
	{
		std::ifstream in(argv[1]);
		if (not in.is_open() or in.bad())
			throw std::runtime_error("could not open " + std::string(argv[1]));
		read_fasta(in, [&](std::string&& header, std::string&& ali) {
			alignment.emplace_back(std::move(ali));
		});
	}
	const size_t num_seq = alignment.size();
	if (num_seq == 0)
		return 0;
	std::cout << num_seq << " sequences" << std::endl;
	for (size_t i = 1; i < num_seq; i++)
		if (alignment[i].size() != alignment[0].size())
			throw std::runtime_error("alignment widths differ: seq 0 has "
				+ std::to_string(alignment[0].size())
				+ ", and seq " + std::to_string(i) + " has " + std::to_string(alignment[i].size()));
	struct stats_t {
		size_t num_equal = 0;
		size_t chars_in_equal_cols = 0;
		size_t total_chars = 0;
		stats_t& operator+=(const stats_t& rhs)
		{
			num_equal += rhs.num_equal;
			chars_in_equal_cols += rhs.chars_in_equal_cols;
			total_chars += rhs.total_chars;
			return *this;
		}
		stats_t operator+(const stats_t& rhs) const
		{
			return stats_t { *this } += rhs;
		}
	};
	std::vector<stats_t> stats(omp_get_max_threads());
#pragma omp parallel for
	for (size_t i = 0; i < alignment[0].size(); i++) {
		stats_t& st = stats[omp_get_thread_num()];
		bool equal = true;
		for (size_t s = 0; s < num_seq; s++) {
			if (alignment[s][i] != alignment[0][i])
				equal = false;
			if (alignment[s][i] != '-') {
				st.total_chars++;
			}
		}
		if (equal)
			st.chars_in_equal_cols += num_seq;
		st.num_equal += equal;
	}
	const stats_t res = std::accumulate(stats.begin(), stats.end(), stats_t {});
	std::cout << res.num_equal << " equal columns" << std::endl;
	std::cout << alignment[0].size() << " columns" << std::endl;
	std::cout << (res.num_equal * 100. / alignment[0].size()) << "% identities" << std::endl;
	std::cout << res.chars_in_equal_cols << "/" << res.total_chars << " chars in equal cols ("
			  << (res.chars_in_equal_cols * 100. / res.total_chars) << "%)"
			  << std::endl;
	return 0;
}
