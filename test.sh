#!/bin/bash

mdir=$( dirname -- "$( readlink -f -- "$0"; )"; )

prefix=$1;
shift
w=$1
shift
l=$1
shift
files="$@";

{ /bin/time "$mdir/pfp.bin" $w $l $prefix $(echo $files)      ; } 2>&1 | tee $prefix.pfp.log
{ /bin/time "$mdir/gen_st.bin" $prefix                        ; } 2>&1 | tee $prefix.gen_st.log
{ /bin/time "$mdir/calc_mums.bin" $prefix                     ; } 2>&1 | tee $prefix.calc_mums.log
{ /bin/time "$mdir/chain2.bin" $prefix $prefix.mums           ; } 2>&1 | tee $prefix.chain2.log
{ /bin/time "$mdir/chain2blocks.bin" $prefix                  ; } 2>&1 | tee $prefix.chain2blocks.log
# { /bin/time "$mdir/check_blocks.bin" $prefix                  ; } 2>&1 | tee $prefix.check_blocks.log # optional
{ /bin/time "$mdir/add_adjacent_blocks.bin" $prefix           ; } 2>&1 | tee $prefix.add_adjacent_blocks.log
# { /bin/time "$mdir/check_blocks.bin" $prefix                  ; } 2>&1 | tee $prefix.check_blocks.ext_heuristic.log # optional

{ /bin/time "$mdir/add_blocks_in_gaps.bin" $prefix -d /mnt/ramdisk -fmalign ~/FMAlign2 -minimap ~/minimap2-2.26_x64-linux ; } 2>&1 | tee $prefix.add_blocks_in_gaps.log
