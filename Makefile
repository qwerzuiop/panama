sdsl_dir     = ~/sdsl-lite

include $(sdsl_dir)/Make.helper
LIBS = -lsdsl -ldivsufsort -ldivsufsort64 -lstdc++fs
SRC = src
OPTIONS = $(MY_CXX) $(MY_CXX_FLAGS) $(MY_CXX_OPT_FLAGS) -no-pie $(C_OPTIONS) -I$(INC_DIR) -L$(LIB_DIR) -std=c++2a -fopenmp -pthread -Wno-deprecated-declarations -Wno-ignored-qualifiers

bin: pfp.bin gen_st.bin calc_mums.bin chain2.bin chain2blocks.bin check_blocks.bin add_adjacent_blocks.bin add_blocks_in_gaps.bin

all: bin
 
pfp.bin: $(SRC)/*
	$(OPTIONS)  \
	$(SRC)/main_pfp.cpp -o pfp.bin \
	$(LIBS)
	
gen_st.bin: $(SRC)/*
	$(OPTIONS) \
	$(SRC)/main_suff_tree.cpp -o gen_st.bin \
	$(LIBS)

calc_mums.bin: $(SRC)/*
	$(OPTIONS) \
	$(SRC)/main_mums.cpp -o calc_mums.bin \
	$(LIBS)

chain2.bin: $(SRC)/*
	$(OPTIONS) \
	$(SRC)/main_chaining_2.cpp -o chain2.bin \
	$(LIBS)
	
chain2blocks.bin: $(SRC)/*
	$(OPTIONS) \
	$(SRC)/main_chain_to_blocks.cpp -o chain2blocks.bin \
	$(LIBS)

check_blocks.bin: $(SRC)/*
	$(OPTIONS) \
	$(SRC)/main_check_blocks.cpp -o check_blocks.bin \
	$(LIBS)

add_adjacent_blocks.bin: $(SRC)/*
	$(OPTIONS) \
	$(SRC)/main_add_adjacent_blocks.cpp -o add_adjacent_blocks.bin \
	$(LIBS)
	
add_blocks_in_gaps.bin: $(SRC)/*
	$(OPTIONS) \
	$(SRC)/main_add_blocks_in_gaps.cpp -o add_blocks_in_gaps.bin \
	$(LIBS)