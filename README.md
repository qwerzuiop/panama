# PANAMA (PANgenomic Anchor-based Multiple Alignment)

This repository contains an implementation of the algorithms from
> Jannik Olbrich, Thomas Büchler, Enno Ohlebusch *Generating Multiple Alignments on a Pangenomic Scale*, 2025

This has only been tested on GNU/Linux with GCC-11. **Other operating systems and compilers may or may not work.**

## Requirements
- the [SDSL-Lite library](https://github.com/simongog/sdsl-lite)
- [FMAlign2](https://github.com/metaphysicser/FMAlign2)
- [minimap2](https://github.com/lh3/minimap2)
- A recent C++ compiler (we only tested with GCC-11.4.0)

## Installation
- in the first line of `Makefile`, change `~/sdsl-lite` to the install directory of `SDSL-Lite` if necessary
- run `make -j`
- please ensure that all executables shipped with FMAlign2 are marked executable (these lie in the `ext/`-subdirectory)

## Usage
Our program consists of multiple stages. To run all stages, use the `test.sh` script.
(You have to change the *directories* of `FMAlign2` and `minimap2` in the last line of this script, and change or remove the given directory for temporary files (`/mnt/ramdisk` is given as default in `test.sh`).)
Run as `./test.sh <name> 10 100 <FASTA-files>`,
where `<name>` can be an arbitrary alphanumeric string, `10` is the window width for PFP, `100` is the modulus for PFP, and `FASTA-files` is a list of input files as arguments.
The generated alignment will be written to `<name>.aligned.fa`.
(You may want to use e.g. GNU Time to measure the total wall clock time via `/bin/time ./test.sh ...`.)
**Please ensure that the stack size is unlimited. This can be achieved by invoking `ulimit -s unlimited` before execution.**  


You may use `count_equal.cpp` to evaluate the generated alignment.
