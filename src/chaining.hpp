#pragma once

#include "common.hpp"
#include "mems.hpp"

#include "periodic.hpp"
#include "segtree.hpp"
#include "sorting.hpp"
#include "tui.hpp"

#include <limits>
#include <map>
#include <memory>
#include <numeric>
#include <queue>
#include <vector>

// simpler O(n^2) dynamic programming approach
template <typename mem_t, typename Out, typename Sc>
void chain_mems_simple(const std::vector<mem_t>& mems, const Sc& score, const Out& out)
{
	const size_t n = mems.size();
	if (n == 0)
		return;
	const size_t k = mems[0].coords.size();

	std::vector<size_t> dp(n), order(n);
	std::iota(order.begin(), order.end(), 0);
	std::sort(order.begin(), order.end(), [&](const auto& lhs, const auto& rhs) {
		return mems[lhs].coords < mems[rhs].coords;
	});
	for (size_t i = 0; i < n; i++) {
		size_t predecessor = std::numeric_limits<size_t>::max();
		const auto mem = order[i];
		for (size_t j = i; j-- > 0;) {
			const auto other = order[j];
			if (dp[j] <= dp[i])
				continue; // this cannot improve dp[i]

			bool ok = true;
			for (size_t l = 0; l < k; l++)
				if (mems[mem].coords[l] < mems[other].pos(l) + mems[other].length(l)) {
					ok = false;
					break;
				}
			if (not ok)
				continue;

			dp[i] = dp[j];
			predecessor = other;
		}
		dp[i] += score(mems[mem]);
		out(mem, dp[i], predecessor);
	}
}
// should be good on almost ordered data
template <typename Out, typename Sc>
void chain_mems_ordered(const chain_t& mems, const Sc& score, const Out& out)
{
	const size_t n = mems.size();
	if (n == 0)
		return;
	const size_t k = mems[0].coords.size();

	std::vector<size_t> order(n);
	std::iota(order.begin(), order.end(), 0);
	std::sort(order.begin(), order.end(), [&](const auto& lhs, const auto& rhs) {
		return mems[lhs].coords[0] < mems[rhs].coords[0];
	});

	std::vector<std::pair<size_t, size_t>> done; // (val, mem), sorted by increasing value
	done.reserve(n);

	size_t i = 0, last_steps = 0, sum_steps = 0;
	auto rep = repeat([&] { reset_line(std::cout) << "processed " << i << " / " << n << "   last steps: " << last_steps << "   avg steps: " << (double(sum_steps) / i) << std::flush; }, [] { reset_line(std::cout) << std::flush; });
	for (; i < n; i++) {
		size_t predecessor = std::numeric_limits<size_t>::max();
		const auto mem = order[i];
		size_t val = 0;

		// TODO: this can be parallelized a bit
		size_t steps = 0;
		for (size_t it = done.size(); it-- > 0; steps++) {
			const auto& [val_other, other] = done[it];

			bool ok = true;
			for (size_t l = 0; l < k; l++)
				if (mems[mem].pos(l) < mems[other].pos(l) + mems[other].length(l)) {
					ok = false;
					break;
				}
			if (not ok)
				continue;

			val = val_other;
			predecessor = other;

			break;
		}
		last_steps = steps, sum_steps += steps;
		val += score(mems[mem]);

		// INSERT
		const auto it = std::upper_bound(done.begin(), done.end(), val, [&](const auto& val, const auto& cmp) {
			return val < cmp.first;
		});
		done.insert(it, std::make_pair(val, mem));

		out(mem, val, predecessor);
	}
}
// enforce that the smallest k (k >= min_coverage) are in a chain
template <typename mem_t, typename Out, typename Sc>
void chain_mems_ordered_allow_gaps(const std::vector<mem_t>& mems, Sc&& score, Out&& out, const size_t min_coverage, const std::vector<uint32_t>& block_id)
{
	const size_t n = mems.size();
	if (n == 0)
		return;

	const size_t k = mems[0].num_seq();
	// we assume that MEM i occurs at the lowest num_present[i] positions
	std::vector<uint32_t> num_present(n);
	for (size_t i = 0; i < n; i++) {
		size_t j;
		for (j = 0; j < k and mems[i].pos(j) != MEM_t::NOT_PRESENT; j++) { }
		num_present[i] = j;
	}

	std::vector<size_t> order(n);
	std::iota(order.begin(), order.end(), 0);
	std::sort(order.begin(), order.end(), [&](const auto& lhs, const auto& rhs) {
		if (block_id[lhs] != block_id[rhs])
			return block_id[lhs] < block_id[rhs];
		return mems[lhs].pos(0) < mems[rhs].pos(0);
	});

	const auto done = std::make_unique<std::vector<std::pair<size_t, uint32_t>>[]>(k + 1 - min_coverage); // (val, mem), sorted by increasing value

	size_t o_i = 0, last_steps = 0, sum_steps = 0;
	auto rep = repeat([&] { reset_line(std::cout) << "processed " << o_i << " / " << n
												  << "   last steps: " << last_steps
												  << "   avg steps: " << (double(sum_steps) / o_i)
												  << std::flush; }, [] { reset_line(std::cout) << std::flush; });
	for (; o_i < n; o_i++) {
		size_t pred_value = std::numeric_limits<size_t>::max();
		const auto mem = order[o_i];
		for (size_t m = min_coverage; m <= num_present[mem]; m++) {
			const size_t m_i = m - min_coverage;
			uint32_t predecessor = std::numeric_limits<uint32_t>::max(), val = 0;

			// use pred_value to decrease the possible range of predecessors
			// this is possible, because the total length of a chain can only decrease when we add more sequences
			size_t it = [&] {
				auto it = std::upper_bound(done[m_i].begin(), done[m_i].end(), pred_value, [](const size_t l, const auto& r) -> bool {
					return l < r.first;
				});
				assert(it == done[m_i].end() or it->second > pred_value);
				return std::distance(done[m_i].begin(), it);
			}();
			size_t num_steps = 0; // just for keeping some stats...
			while (it-- > 0) {
				num_steps++;

				const auto [val_other, other] = done[m_i][it];
				bool ok = true;
				// compare the m first coords
				for (size_t j = 0; j < m; j++)
					if (mems[mem].pos(j) < mems[other].pos(j) + mems[other].length(j)) {
						ok = false;
						break;
					}
				if (not ok)
					continue;

				val = val_other, predecessor = other;

				break;
			}
			last_steps = num_steps, sum_steps += num_steps;
			val += score(mems[mem], m);

			const auto pos = std::upper_bound(done[m_i].begin(), done[m_i].end(), val, [](const size_t l, const auto& r) -> bool {
				return l < r.first;
			});
			done[m_i].emplace(pos, val, mem);

			out(m, mem, val, predecessor);
		}
	}
}
template <typename mem_t, typename Sc>
std::pair<std::vector<mem_t>, std::vector<mem_t>> chain_mems_ordered_allow_gaps(
	std::vector<mem_t>&& mems, Sc&& score, const size_t min_coverage, std::vector<uint32_t>&& block_id)
{
	assert(min_coverage >= 2);
	const size_t n = mems.size();
	const size_t k = mems[0].num_seq();

	constexpr uint32_t NO_PRED = std::numeric_limits<uint32_t>::max();

	auto pred = std::make_unique<std::vector<uint32_t>[]>(k + 1 - min_coverage);
	for (size_t i = min_coverage; i <= k; i++)
		pred[i - min_coverage].resize(n, NO_PRED);

	size_t best_val = 0;
	uint32_t best_mem = NO_PRED, best_m = 0;
	chain_mems_ordered_allow_gaps(
		mems, std::forward<Sc>(score), [&](size_t m, uint32_t mem_i, size_t value, uint32_t pre) {
			assert(m >= min_coverage);
			pred[m - min_coverage][mem_i] = pre;
			if (std::tie(best_val, best_m) < std::tie(value, m))
				best_val = value, best_mem = mem_i, best_m = m;
		},
		min_coverage, block_id);
	assert(best_val == 0 or best_m >= min_coverage);

	std::vector<mem_t> res;
	for (; best_mem != NO_PRED; best_mem = pred[best_m - min_coverage][best_mem]) {
		for (size_t i = 0; i < best_m; i++)
			assert(mems[best_mem].present(i));
		res.emplace_back(std::move(mems[best_mem]));
		mems[best_mem].invalidate();
	}
	std::reverse(res.begin(), res.end());
	{
		auto tmp = std::move(pred);
	} // clear pred
	std::vector<mem_t> rest;
	for (auto& mem : mems)
		if (mem.valid())
			rest.emplace_back(std::move(mem));

	// make MEMs in remaining sequences consistent
	// TODO: do something better than greedy, a short sketch:
	// we can treat each sequence >= best_m individually, and on a single sequence
	// this is equivalent to finding a maximum set of non-intersecting intervals.
	// This is a standard problem (see e.g. https://leetcode.com/problems/maximum-profit-in-job-scheduling)
	// and can be solved using a simple sweep-line algorithm
	const auto last_pos = std::make_unique<size_t[]>(k);
	for (mem_t& mem : res) {
		assert(mem.num_seq() == k);
		assert(mem.valid());
		for (size_t i = 0; i < k; i++) {
			if (mem.pos(i) == MEM_t::NOT_PRESENT) {
				assert(i >= best_m);
				continue;
			}
			if (mem.pos(i) < last_pos[i]) {
				assert(i >= best_m);
				mem.pos(i) = MEM_t::NOT_PRESENT;
				continue;
			}
			last_pos[i] = mem.pos(i) + mem.length(i);
		}
	}
	return std::make_pair(std::move(res), std::move(rest));
}

template <typename S, typename P>
struct rmq {
	fenwick_tree_t<S> m_tree;
	std::vector<uint32_t> m_pos;
	std::vector<P> m_sorted_positions;
	void set_value(const size_t i, const S score)
	{
		const auto p = m_pos[i];
		// assert(m_tree.get_cur(p) == 0);
		m_tree.update(p, score);
	}
	// return max value where pos < p
	S get_max(const P& p) const
	{
		const auto it = std::lower_bound(m_sorted_positions.begin(), m_sorted_positions.end(), p);
		return m_tree.query(std::distance(m_sorted_positions.begin(), it));
	}
	template <typename Q>
	rmq(size_t n, const Q& get_pos)
		: m_tree(n, [](const auto& lhs, const auto& rhs) { return std::max(lhs, rhs); })
		, m_pos(n)
		, m_sorted_positions(n)
	{
		assert(n < std::numeric_limits<uint32_t>::max());
		// fill m_pos and m_sorted_positions
		std::vector<uint32_t> sorted(n);
		std::iota(sorted.begin(), sorted.end(), 0);
		radix_sort(sorted.begin(), sorted.end(), [&](const auto& i) { return get_pos(i); });
		for (size_t i = 0; i < n; i++) {
			m_pos[sorted[i]] = i;
			m_sorted_positions[i] = get_pos(sorted[i]);
		}
		assert(std::is_sorted(m_sorted_positions.begin(), m_sorted_positions.end()));
	}
};

// better on more noisy data
template <typename mem_type, typename Out, typename Sc>
void chain_mems_ordered_greedy(const std::vector<mem_type>& mems, const Sc& score, const Out& out, bool print_updates = true)
{
	const size_t n = mems.size();
	if (n == 0)
		return;
	const size_t k = mems[0].coords.size();

	// sort by first coord
	std::vector<size_t> order(n);
	std::iota(order.begin(), order.end(), 0);
	radix_sort(order.begin(), order.end(), [&](const auto& i) { return mems[i].coords[0]; });

	// constructe bounding data structure
	std::vector<rmq<size_t, size_t>> score_bound;
	for (size_t d = 0; d < k; d++)
		score_bound.emplace_back(n, [&](const auto& i) { return mems[i].coords[d]; });

	size_t i = 0, last_steps = 0, sum_steps = 0;
	auto rep = not print_updates
		? periodic_control_t()
		: repeat([&] { reset_line(std::cout) << "processed " << i << " / " << n
											 << "   last steps: " << last_steps
											 << "   avg steps: " << (double(sum_steps) / i)
											 << std::flush; }, [] { reset_line(std::cout) << std::flush; });
	std::map<size_t, std::vector<size_t>> done;
	for (; i < n; i++) {
		size_t predecessor = std::numeric_limits<size_t>::max();
		const auto mem = order[i];
		size_t val = 0;

		const auto bound = [&] {
			size_t bound = std::numeric_limits<size_t>::max();
			for (size_t d = 0; d < k; d++)
				bound = std::min(bound, score_bound[d].get_max(mems[mem].coords[d] + 1));
			return bound;
		}();

		size_t steps = 0;
		for (auto it = done.upper_bound(bound); it != done.begin();) {
			it--;

			const auto& val_other = it->first;
			for (const auto& other : it->second) {
				steps++;

				bool ok = true;
				for (size_t l = 0; l < k; l++)
					if (mems[mem].coords[l] < mems[other].coords[l] + mems[other].len) {
						ok = false;
						break;
					}
				if (not ok)
					continue;

				val = val_other;
				predecessor = other;

				goto predecessor_found;
			}
		}
	predecessor_found:

		last_steps = steps, sum_steps += steps;
		val += score(mems[mem]);
		for (size_t d = 0; d < k; d++)
			score_bound[d].set_value(mem, val);

		done[val].emplace_back(mem);

		out(mem, val, predecessor);
	}
}
