#pragma once

#include "common.hpp"
#include <cassert>
#include <string>
#include <vector>

size_t find_longest_prefix(const std::vector<std::string_view>& sequences)
{
	size_t longest_prefix = sequences[0].size();
	for (size_t i = 1; longest_prefix > 0 and i < sequences.size(); i++) {
		size_t lcp = 0;
		while (lcp < longest_prefix and lcp < sequences[i].size() and sequences[i][lcp] == sequences[0][lcp])
			lcp++;
		longest_prefix = lcp;
	}
	return longest_prefix;
}

// align prefixes, because fmalign2 segfaults when there is a string such that every string is prefix of it
// returns (alignment of prefixes, rest)
std::vector<std::pair<std::string, std::string_view>> align_prefixes(const std::vector<std::string_view>& gap)
{
	const auto longest_prefix = find_longest_prefix(gap);
	std::vector<std::pair<std::string, std::string_view>> res(gap.size());
	if (longest_prefix == 0) {
		for (size_t i = 0; i < gap.size(); i++)
			res[i].second = gap[i];
		return res;
	}

	std::vector<size_t> remaining;
	std::vector<std::string_view> rest;
	for (size_t i = 0; i < gap.size(); i++) {
		res[i].first = gap[i].substr(0, longest_prefix);
		std::string_view rst = gap[i].substr(longest_prefix);
		if (not rst.empty()) {
			remaining.emplace_back(i);
			rest.emplace_back(rst);
		} else
			res[i].second = rst;
	}

	if (rest.empty())
		return res;
	auto tmp = align_prefixes(std::move(rest));
	for (size_t i = 0; i < tmp.size(); i++) {
		const auto j = remaining[i];
		res[j].first += tmp[i].first;
		res[j].second = tmp[i].second;
	}
	const size_t aligned_length = res[remaining[0]].first.size();
	for (size_t i = 0; i < gap.size(); i++)
		if (aligned_length > res[i].first.size()) {
			assert(res[i].second.empty());
			res[i].first.resize(aligned_length, '-');
		} else
			assert(res[i].first.size() == aligned_length);
	return res;
}
struct prefix_align_res_t {
	size_t sequence_id;
	std::string alignment;
	std::string_view rest;
	prefix_align_res_t(size_t id, std::string_view rst)
		: sequence_id(id)
		, alignment("")
		, rest(rst)
	{
	}
};
void align_prefixes(std::vector<prefix_align_res_t>& res)
{
	std::vector<std::string_view> gaps(res.size());
	for (size_t i = 0; i < res.size(); i++)
		gaps[i] = res[i].rest;
	auto tmp = align_prefixes(std::move(gaps));
	assert(tmp.size() == res.size());
	for (size_t i = 0; i < res.size(); i++) {
		assert(res[i].alignment.empty());
		res[i].alignment = std::move(tmp[i].first);
		res[i].rest = std::move(tmp[i].second);
	}
}
