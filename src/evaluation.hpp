#pragma once

#include <algorithm>
#include <cassert>
#include <cmath>
#include <filesystem>
#include <type_traits>
#include <vector>
#include <fstream>

// returns 2 times the median
template<typename T>
T median(const T* ptr, size_t n)
{
	assert(n > 0);
	std::vector<T> data(ptr, ptr + n);
	std::sort(data.begin(), data.end());
	// if n is odd, n/2 == (n-1)/2
	return data[n / 2] + data[(n - 1) / 2];
}
template<typename T>
T median(const std::vector<T>& data)
{
	return median<T>(data.data(), data.size());
}

template<typename T>
double standard_deviation(const T* ptr, size_t n, double med)
{
	double res = 0;
	for (size_t i = 0; i < n; i++) {
		const double d = ptr[i] - med;
		res += d * d;
	}
	return std::sqrt(res / n);
}
template<typename T>
double standard_deviation(const T* ptr, size_t n)
{
	return standard_deviation<T>(ptr, n, median(ptr, n) / 2.0);
}
template<typename T>
double standard_deviation(const std::vector<T>& data)
{
	return standard_deviation<T>(data.data(), data.size());
}

template<typename It>
void write_histogram(const std::filesystem::path& path, It b, It e)
{
	using K = std::remove_cv_t<std::remove_reference_t<decltype(b->first)>>;
	using V = std::remove_cv_t<std::remove_reference_t<decltype(b->second)>>;
	std::vector<std::pair<K, V>> vals(b, e);
	std::sort(vals.begin(), vals.end());
	std::ofstream out(path, std::ios::out);
	if (not out.is_open() or out.bad())
		throw std::runtime_error("could not open " + path.string());

	out << "x y\n";
	for (const auto&[k,n] : vals)
		out << k << ' ' << n << '\n';

	out.close();
}
