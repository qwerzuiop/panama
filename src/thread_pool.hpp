#pragma once

#include <cassert>
#include <functional>
#include <future>
#include <iostream>
#include <list>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>
#include <type_traits>
#include <vector>

class thread_pool_t {
	using task_t = std::packaged_task<void()>;

	std::mutex m_threads_mutex;
	size_t m_threads_terminate = 0;
	std::list<std::shared_ptr<std::jthread>> m_threads;
	std::vector<std::list<std::shared_ptr<std::jthread>>::iterator> m_thread_rm;

	std::deque<task_t> m_queue;
	std::mutex m_queue_mutex;
	std::condition_variable m_cv;
	std::atomic<bool> terminate = false;

	void add_thread()
	{
		std::lock_guard<std::mutex> lock(m_threads_mutex);
		m_threads.emplace_back();
		auto it = std::prev(m_threads.end());
		*it = std::make_shared<std::jthread>([this, it] {
			std::optional<task_t> f;
			while (true) {
				{
					std::unique_lock<std::mutex> lock { m_queue_mutex };
					while (not terminate and m_queue.empty() and m_threads_terminate == 0)
						m_cv.wait(lock);
					{
						std::unique_lock<std::mutex> lock { m_threads_mutex };
						for (const auto it : m_thread_rm)
							m_threads.erase(it);
						m_thread_rm.clear();
						if (m_threads_terminate > 0) {
							m_threads_terminate--;
							m_thread_rm.emplace_back(std::move(it));
							return;
						}
					}
					if (terminate)
						return;
					assert(not m_queue.empty());
					f = std::move(m_queue.front());
					m_queue.pop_front();
				}
				assert(f);
				(*f)();
				f = std::nullopt;
			}
		});
	}

	template <typename F>
	auto emplace(F&& fn, bool priority = false) -> std::future<decltype(fn())>
	{
		static_assert(std::is_void_v<decltype(fn())>);
		task_t task { std::forward<F>(fn) };
		auto res = task.get_future();

		{
			std::lock_guard<std::mutex> lock(m_queue_mutex);
			if (priority)
				m_queue.emplace_front(std::move(task));
			else
				m_queue.emplace_back(std::move(task));
		}
		m_cv.notify_one();

		return res;
	}

public:
	void stop()
	{
		terminate = true;
		{
			std::lock_guard<std::mutex> lock { m_queue_mutex };
			while (not m_queue.empty())
				m_queue.pop_front();
		}
		m_cv.notify_all();
		m_threads.clear();
	}
	~thread_pool_t()
	{
		stop();
	}
	thread_pool_t(const thread_pool_t&) = delete;
	thread_pool_t(thread_pool_t&&) = delete;
	thread_pool_t(size_t num_threads = 0)
	{
		if (num_threads == 0)
			num_threads = std::thread::hardware_concurrency();
		for (size_t i = 0; i < num_threads; i++)
			add_thread();
	}
	template <typename F>
	void for_each_in_range(size_t n, const F& f, bool priority = false)
	{
		// for (size_t i = n; i-- > 0;)
		// 	f(i);
		// return;

		if (n == 0)
			return;
		if (n == 1) {
			f((size_t)0);
			return;
		}
		std::vector<std::future<void>> futures(n);
		for (size_t i = 0; i < n; i++) {
			futures[i] = emplace(std::bind(f, i), priority);
		}
		add_thread();

		for (size_t i = 0; i < n; i++) {
			futures[i].get();
		}

		{
			std::lock_guard<std::mutex> lock(m_threads_mutex);
			m_threads_terminate++;
		}
		m_cv.notify_all();
	}
};
