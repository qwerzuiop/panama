#include <iostream>
#include "common.hpp"
#include "system_timer.cpp"
#include "evaluation.hpp"


using namespace std;


bool all_eq(BLOCK_T & b, const my_index_t & I){
	string seq = I.substr(b[0], b.length);
	for(const auto& c : b.start_off){
		string seq2 = I.substr(c, b.length);
		if(seq != seq2)
			return false;
	}
	return true;
}

int64_t dist(coord_t c1, coord_t c2, vector<uint64_t> & pfx){
	return (pfx[c2.first] + c2.second) - (int64_t)(pfx[c1.first] + c1.second);
}

int64_t min_dist(BLOCK_T & b1, BLOCK_T & b2, vector<uint64_t> & pfx){
	int64_t min = dist(b1[0], b2[0], pfx) - b1.length;

	for(uint32_t i =1 ; i < b1.start_off.size(); i++){
		int64_t d = dist(b1[i], b2[i], pfx) - b1.length;
		if(d < min) min = d;
	}
	return min;
}


int main(int argc, char** argv){
	cout << "[1] prefix of input"  << endl;
	if(argc < 2) return 1;
	const string in_pref = argv[1];
	cout << " =" << in_pref << endl;

	const bool export_histogram = argc > 2 and argv[2] == std::string("--histogram");


	sys_timer st; st.start();
	cout << "load parse and dict"<< endl;
	my_index_t I(in_pref);

	cout << "check blocks"<< endl;
	size_t count = 0;
	size_t size = 0;
	bool first = true;
	BLOCK_T pred;
	checked_ifstream_t blocks_in(in_pref+FEX_BLOCK);

	while(true){
		BLOCK_T B(blocks_in);
		if(!blocks_in) break;

		if(!all_eq(B,I)) {
			cout << "\rNOT VALID BLOCK" << endl << B.length;
			for (size_t i = 0; i < B.start_off.size(); i++)
				cout << i << ": " << B.start_off[i] << " : " << I.substr(B[i], B.length) << std::endl;
		}

		if(!first && min_dist(pred , B, I.P_prefsum) < 0 ) {
			cout << "\rNOT VALID CHAIN" << endl << pred << B;
		}


		count++;
		size += B.length;

		cout << "\r" << count << flush;
		first = false;
		swap(pred, B);
	}
	blocks_in.check_and_close();
	cout << " blocks" << endl;

	if (export_histogram) {
		const size_t num_seq = I.boundaries.size();
		std::vector<BLOCK_T> blocks;
		{
			checked_ifstream_t blocks_in(in_pref+FEX_BLOCK);
			while (true) {
				BLOCK_T block(blocks_in);
				if (not blocks_in) break;
				blocks.emplace_back(std::move(block));
			}
			blocks_in.close();
		}
		size_t total_uncovered_bases = 0, total_covered_bases = 0;
		size_t max_gap_size = 0, largest_gap = -1;
		std::unordered_map<double, size_t> gap_count;
		std::unordered_map<size_t, size_t> block_count;
		std::vector<uint64_t> largest_gap_sizes;
		
		double max_std_deviation = 0;
		std::ofstream out(in_pref + ".hist");
		for (size_t i = 0; i <= blocks.size(); i++) {
			if (i < blocks.size()) {
				block_count[blocks[i].length]++;
			}
			
			auto gap = gap_strings(
				i == 0
					? dummy_block_front(I)
					: blocks[i - 1],
				i < blocks.size()
					? blocks[i]
					: dummy_block_back(I),
				I);

			std::vector<uint64_t> gap_sizes(num_seq);
			for (size_t s = 0; s < num_seq; s++)
				gap_sizes[s] = gap[s].seq.size();

			{
				uint64_t sum = std::accumulate(gap_sizes.begin(), gap_sizes.end(), 0ull);
				out << fixed << setprecision(5) << sum/(double)gap_sizes.size() << '\n';
			}
			gap_count[median(gap_sizes) / 2.0]++;

			max_std_deviation = std::max(max_std_deviation, standard_deviation(gap_sizes));
			if (const size_t gap_size = *std::max_element(gap_sizes.begin(), gap_sizes.end()); gap_size > max_gap_size) {
				max_gap_size = gap_size;
				largest_gap = i;
				largest_gap_sizes = gap_sizes;
			}
			total_uncovered_bases += std::accumulate(gap_sizes.begin(), gap_sizes.end(), (size_t)0);
			if (i < blocks.size())
				total_covered_bases += blocks[i].length;
		}
		out.close();

		blocks_in.check_and_close();
		write_histogram(in_pref + ".gaplength_histogram", gap_count.cbegin(), gap_count.cend());
		write_histogram(in_pref + ".blocklength_histogram", block_count.cbegin(), block_count.cend());
		{
			std::unordered_map<uint64_t, uint64_t> cnt_sz;
			for (const auto& c : largest_gap_sizes)
				cnt_sz[c]++;
			write_histogram(in_pref + ".largest_gaplength_histogram", cnt_sz.cbegin(), cnt_sz.cend());
		}
		std::cout << "total_uncovered_bases: " << total_uncovered_bases << std::endl;
		std::cout << "total_covered_bases:   " << total_covered_bases * num_seq << std::endl;
		std::cout << "num_blocks:            " << blocks.size() << std::endl;
		std::cout << "max_gap_size:          " << max_gap_size << " (before block " << largest_gap << ")" << std::endl;
		std::cout << "max_std_deviation:     " << max_std_deviation << std::endl;
	}

	{
		std::cout << I.boundaries.size() << " sequences" << std::endl;
		size_t total_bases = 0;
		for (size_t i = 0; i < I.boundaries.size(); i++) {
			const size_t start = I.boundaries[i];
			const size_t end = i + 1 < I.boundaries.size() ? I.boundaries[i+1] : I.P.size();
			const size_t num_bases = I.get_base_idx(end) - I.get_base_idx(start);
			if (I.boundaries.size() < 20)
				std::cout << i << ": [" << start << ", " << end << ") : " << num_bases << " bases" << std::endl;
			total_bases += num_bases;
		}
		std::cout << total_bases << " total bases" << std::endl;
		std::cout << size * I.boundaries.size() * 100. / total_bases << "%" << std::endl;
	}


	cout << size << " bases" << endl;
	cout << "BLOCKS OK" << endl;
	cout << "Time for checking to blocks: " << st.stop_and_get() << endl;
	return 0;
}









