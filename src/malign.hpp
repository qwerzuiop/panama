#pragma once

#include "chaining.hpp"
#include "common.hpp"
#include "config.hpp"
#include "external.hpp"
#include "file_util.hpp"
#include "mems.hpp"
#include "mums.cpp"
#include "pfp.cpp"
#include "system_timer.cpp"
#include "thread_pool.hpp"
#include "trivial.hpp"
#include "tui.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <limits>
#include <numeric>
#include <queue>
#include <stack>
#include <string>
#include <string_view>
#include <variant>

// The basic idea is:
// - find MEMs that occur at least in C% of sequences
// - find the longest best chain of MEMs occuring in the k longest sequences (k >= C% * #sequences)
// - extend this chain to the remaining sequences while keeping it consistent (this may mean that we ignore occurrences of MEMs)
// - repeat with the remaining MEMs and exclude the MEMs from the length-calculations (i.e. sort by total gap length). In all further iterations, the MEMs have to respect the chain built in this iteration

std::vector<std::string> malign(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>&& backbone,
	const size_t threshold,
	const std::string& id,
	thread_pool_t& thread_pool);

void remove_invalid(
	std::vector<profile_t>& backbone,
	const size_t threshold)
{
	backbone.erase(std::remove_if(backbone.begin(), backbone.end(), [threshold](const profile_t& p) {
		size_t num_present = 0;
		p.for_each_present_coord([&](size_t) {
			num_present++;
		});
		return num_present < threshold;
	}),
		backbone.end());
}
#ifndef NDEBUG
void check_profile(const profile_t& profile)
{
	size_t last = 0;
	bool first = true;
	size_t num = 0;
	profile.for_each_present_coord([&](size_t s) {
		num++;
		if (profile.aligned()) {
			if (not first)
				assert(profile.m_aligned[s].second.size() == profile.m_aligned[last].second.size());
			size_t bc = bases_consumed(profile.m_aligned[s].second);
			assert(profile.m_aligned[s].first == bc);
			last = s, first = false;
			assert(profile.m_aligned[s].second.size() > 0);
		}
	});
	assert(num * 2 > profile.num_seq());
}
void check_backbone(const size_t num_seq, const std::vector<profile_t>& backbone)
{
	std::vector<size_t> pos(num_seq);
	for (size_t i = 0; i < backbone.size(); i++) {
		backbone[i].for_each_present_coord([&](size_t s) {
			assert(pos[s] <= backbone[i].pos(s));
			pos[s] = backbone[i].pos(s) + backbone[i].length(s);
		});
		check_profile(backbone[i]);
	}
}
#else
void check_profile(const profile_t&)
{
}
void check_backbone(const size_t, const std::vector<profile_t>&) { }
#endif

void go2(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& backbone,
	const size_t threshold,
	const std::string& id,
	thread_pool_t& thread_pool,
	bool may_use_pfp,
	bool may_align,
	bool strict_bounded);

// when false, we replace each bases not covered by the chain with 'N'
constexpr bool output_remaining_gaps = true;

std::vector<profile_t> merge_chains(std::vector<profile_t>&& a, std::vector<profile_t>&& b)
{
	if (a.empty())
		return b;
	if (b.empty())
		return a;
	const size_t num_seq = a[0].num_seq();
	// we basically create a DAG and do topsort

	// indices smaller than a.size() point to "a", the rest to "b"
	const auto get_mem = [&](size_t i) -> profile_t& {
		return (i < a.size() ? a[i] : b[i - a.size()]);
	};
	std::vector<std::vector<std::pair<size_t, std::vector<size_t>>>> order_groups(num_seq);
	{
		for (size_t s = 0; s < num_seq; s++) {
			std::vector<size_t> order;
			for (size_t i = 0; i < a.size(); i++)
				if (a[i].present(s))
					order.emplace_back(i);
			const size_t mid = order.size();
			for (size_t i = 0; i < b.size(); i++)
				if (b[i].present(s))
					order.emplace_back(i + a.size());
			const auto comp = [&](const size_t l, const size_t r) {
				auto &mem_l = get_mem(l), mem_r = get_mem(r);
				assert(mem_l.present(s));
				assert(mem_r.present(s));
				auto lp = mem_l.pos(s), rp = mem_r.pos(s);
				if (lp == rp)
					assert(mem_l.length(s) == 0 or mem_r.length(s) == 0);
				if (lp != rp) [[likely]]
					return lp < rp;
				for (size_t s2 = 0; s2 < num_seq; s2++)
					if (mem_l.present(s2) and mem_r.present(s2) and mem_l.pos(s2) != mem_r.pos(s2))
						return mem_l.pos(s2) < mem_r.pos(s2);
				return mem_l.length(s) < mem_r.length(s);
			};
			// NOTE: if we wanted to sort an unsorted chain from scratch,
			// we could simply replace "inplace_merge" with a "sort"-call using "comp"
			assert(std::is_sorted(order.begin(), order.begin() + mid, comp));
			assert(std::is_sorted(order.begin() + mid, order.end(), comp));
			std::inplace_merge(order.begin(), order.begin() + mid, order.end(), comp);
			for (size_t i = 0; i < order.size();) {
				const auto p = get_mem(order[i]).pos(s);
				const bool zero = get_mem(order[i]).length(s) == 0;
				size_t j = i + 1;
				while (j < order.size() and p == get_mem(order[j]).pos(s) and (zero == (get_mem(order[j]).length(s) == 0)))
					j++;
				order_groups[s].emplace_back(j - i, std::vector<size_t>(&order[i], &order[j]));
				i = j;
			}
		}
	}

	std::vector<size_t> num_pred(a.size() + b.size());
	for (const auto& g : order_groups)
		for (size_t i = 1; i < g.size(); i++)
			for (const auto& x : g[i].second)
				num_pred[x]++;
	std::vector<size_t> cur_idx(num_seq);
	std::stack<size_t, std::vector<size_t>> ready;
	for (size_t i = 0; i < a.size() + b.size(); i++)
		if (num_pred[i] == 0)
			ready.push(i);
	assert(not ready.empty());

	std::vector<profile_t> res;
	while (not ready.empty()) {
		const size_t i = ready.top();
		ready.pop();
		auto& mem = get_mem(i);
		mem.for_each_present_coord([&](size_t s) {
			if (0 < --order_groups[s][cur_idx[s]].first)
				return;
			cur_idx[s]++;
			if (cur_idx[s] >= order_groups[s].size())
				return;
			for (const auto& j : order_groups[s][cur_idx[s]].second)
				if (0 == --num_pred[j])
					ready.push(j);
		});
		res.emplace_back(mem);
		// res.emplace_back(std::move(mem));
	}
	assert(res.size() == a.size() + b.size());

	return res;
}

void make_consistent(
	const size_t num_seq,
	const std::vector<profile_t>& reference,
	std::vector<profile_t>& added,
	const size_t threshold)
{
	if (reference.empty())
		return;

	std::vector<std::vector<my_int>> left_cut(reference.size() + 1);
	left_cut[0].resize(num_seq, 0);
	for (size_t i = 0; i < reference.size(); i++) {
		left_cut[i + 1] = left_cut[i];
		reference[i].for_each_present_coord([&](size_t s) {
			left_cut[i + 1][s] = reference[i].pos(s) + reference[i].length(s);
		});
	}

	struct cut_t {
		my_int start, len;
		size_t ref_id;
	};
	std::vector<std::vector<cut_t>> cut_idx(num_seq);
	for (size_t i = 0; i < reference.size(); i++)
		reference[i].for_each_present_coord([&](size_t s) {
			cut_idx[s].emplace_back(reference[i].pos(s), reference[i].length(s), i);
		});
	for (profile_t& prof : added) {
		// smallest index of a reference such that mem comes before it in every sequence
		size_t max_cut_id = 0;
		assert(prof.num_seq() == num_seq);
		bool illegal = false;
		prof.for_each_present_coord([&](size_t s) {
			const auto& c_i = cut_idx[s];
			if (c_i.empty())
				return;

			// "it" represents the cut at or after the mem in sequence "seq_id"
			auto it = std::lower_bound(c_i.begin(), c_i.end(), prof.pos(s),
				[](const cut_t& cut, my_int position) {
					return cut.start < position;
				});
			if (it != c_i.begin() and std::prev(it)->start + std::prev(it)->len > prof.pos(s)) {
				// this coord conflicts with the preceding cut
				if (prof.aligned() and prof.length(s) > 0)
					illegal = true;
				prof.pos(s) = MEM_t::NOT_PRESENT;
				return;
			}
			if (it != c_i.end() and prof.pos(s) + prof.length(s) > it->start) {
				// this coord conflicts with the cut
				if (prof.aligned() and prof.length(s) > 0)
					illegal = true;
				prof.pos(s) = MEM_t::NOT_PRESENT;
				return;
			}
			if (it == c_i.end()) {
				assert(it != c_i.begin());
				max_cut_id = std::max(max_cut_id, 1 + std::prev(it)->ref_id);
			} else
				max_cut_id = std::max(max_cut_id, it->ref_id);
		});

		prof.for_each_present_coord([&](size_t s) {
			if (prof.pos(s) < left_cut[max_cut_id][s]) {
				if (prof.aligned() and prof.length(s) > 0)
					illegal = true;
				prof.pos(s) = MEM_t::NOT_PRESENT;
			}
		});

		// mark as invalid if necessary
		size_t num_present = 0;
		prof.for_each_present_coord([&](size_t) {
			num_present++;
		});
		if (illegal or num_present < threshold)
			prof.seq.coords.clear();
	}

	// remove all marked with .len = 0
	{
		size_t s = 0;
		for (size_t i = 0; i < added.size(); i++) {
			if (not added[i].valid())
				continue;
			if (i != s)
				added[s] = std::move(added[i]);
			s++;
		}
		added.resize(s);
	}
	for (const profile_t& prof : added) {
		assert(prof.valid());
		size_t num_present = 0;
		prof.for_each_present_coord([&](size_t) { num_present++; });
		assert(num_present >= threshold);
	}
}

// returns remaining MEMs
// NOTE: This assumes that mems are compatible with the backbone
template <uint8_t heuristic = 0>
std::vector<profile_t> _insert_mems_into_chain(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& backbone,
	std::vector<profile_t>&& mems,
	const size_t threshold)
{
	if (mems.empty())
		return {};
	const size_t num_seq = sequences.size();
	check_backbone(num_seq, backbone);
	assert(2 * threshold > num_seq);

#ifndef NDEBUG
	for (const profile_t& prof : backbone) {
		assert(prof.valid());
		size_t num = 0;
		prof.for_each_present_coord([&](size_t) {
			num++;
		});
		assert(num >= threshold);
	}
	for (const profile_t& mem : mems) {
		assert(mem.valid());
		size_t num = 0;
		mem.for_each_present_coord([&](size_t) {
			num++;
		});
		assert(num >= threshold);
	}
#endif

	// sort by length
	std::vector<size_t> order(num_seq);
	std::iota(order.begin(), order.end(), 0u);

	{
		std::vector<size_t> covered(num_seq), remaining(num_seq), old_covered(num_seq);
		for (size_t s = 0; s < num_seq; s++)
			remaining[s] = sequences[s].size();
		for (const profile_t& prof : mems)
			prof.for_each_present_coord([&](size_t s) {
				covered[s] += prof.length(s);
			});
		for (const profile_t& prof : backbone)
			prof.for_each_present_coord([&](size_t s) {
				assert(remaining[s] >= prof.length(s));
				remaining[s] -= prof.length(s);
				old_covered[s] += prof.length(s);
			});

		std::sort(order.begin(), order.end(), [&](size_t l, size_t r) {
			static_assert(heuristic <= 1, "heuristic must be 0 or 1");
			if constexpr (heuristic == 0) {
				if (covered[l] != covered[r])
					return covered[l] > covered[r];
				if (remaining[l] != remaining[r])
					return remaining[l] > remaining[r];
				return old_covered[l] < old_covered[r];
			} else if constexpr (heuristic == 1) {
				if (remaining[l] != remaining[r])
					return remaining[l] > remaining[r];
				if (covered[l] != covered[r])
					return covered[l] > covered[r];
				return old_covered[l] < old_covered[r];
			}
		});
	}

	std::vector<profile_t> remaining_mems;
	{
		std::vector<uint32_t> block_id(mems.size());
		for (size_t i = 0; i < mems.size(); i++) {
			auto it = std::upper_bound(backbone.cbegin(), backbone.cend(), 0, [&](int, const profile_t& prof) -> bool {
				for (size_t s = 0; s < num_seq; s++) {
					if (not prof.present(s))
						continue;
					if (not mems[i].present(s))
						continue;
					if (prof.pos(s) != mems[i].pos(s))
						return mems[i].pos(s) < prof.pos(s);
					if (prof.length(s) > 0)
						return true;
					if (mems[i].length(s) > 0)
						return false;
				}
				assert(false);
				__builtin_unreachable();
				exit(1);
			});
			block_id[i] = std::distance(backbone.cbegin(), it);
		}

		std::vector<profile_t> permuted(mems.size()), permuted_rest;
		// #pragma omp parallel for
		for (size_t i = 0; i < permuted.size(); i++) {
			permuted[i].seq.len = mems[i].seq.len;
			permuted[i].seq.coords.resize(num_seq);
			for (size_t j = 0; j < num_seq; j++)
				permuted[i].seq.coords[j] = mems[i].seq.coords[order[j]];
			if (mems[i].aligned()) {
				permuted[i].m_aligned.resize(num_seq);
				for (size_t j = 0; j < num_seq; j++) {
					permuted[i].m_aligned[j] = std::move(mems[i].m_aligned[order[j]]);
				}
				mems[i].m_aligned.clear();
			}
		}
		std::tie(permuted, permuted_rest) = chain_mems_ordered_allow_gaps(
			std::move(permuted), [](const profile_t& permuted, size_t k) -> my_int {
				assert(permuted.present(0));
				return k * permuted.length(0);
			},
			threshold, std::move(block_id));
		// undo permutation
		mems.resize(permuted.size());
		// #pragma omp parallel for
		for (size_t i = 0; i < permuted.size(); i++) {
			mems[i].seq.len = permuted[i].seq.len;
			mems[i].seq.coords.resize(num_seq);
			for (size_t j = 0; j < num_seq; j++)
				mems[i].seq.coords[order[j]] = permuted[i].seq.coords[j];
			assert(mems[i].m_aligned.empty());
			if (permuted[i].aligned()) {
				mems[i].m_aligned.resize(num_seq);
				for (size_t j = 0; j < num_seq; j++)
					mems[i].m_aligned[order[j]] = std::move(permuted[i].m_aligned[j]);
			}
		}
		{
			auto tmp = std::move(permuted);
		} // clear permuted

		remaining_mems.resize(permuted_rest.size());
		// #pragma omp parallel for
		for (size_t i = 0; i < permuted_rest.size(); i++) {
			remaining_mems[i].seq.len = permuted_rest[i].seq.len;
			remaining_mems[i].seq.coords.resize(num_seq);
			for (size_t j = 0; j < num_seq; j++)
				remaining_mems[i].seq.coords[order[j]] = permuted_rest[i].seq.coords[j];
			if (permuted_rest[i].aligned()) {
				remaining_mems[i].m_aligned.resize(num_seq);
				for (size_t j = 0; j < num_seq; j++)
					remaining_mems[i].m_aligned[order[j]] = std::move(permuted_rest[i].m_aligned[j]);
			}
		}
	}

#ifndef NDEBUG
	{
		std::vector<size_t> last_pos(num_seq);
		for (size_t i = 0; i < backbone.size(); i++)
			backbone[i].for_each_present_coord([&](size_t s) {
				assert(last_pos[s] <= backbone[i].pos(s));
				last_pos[s] = backbone[i].pos(s) + backbone[i].length(s);
			});
	}
#endif
	remove_invalid(mems, threshold);
	if (not mems.empty()) {
		backbone = merge_chains(std::move(backbone), std::move(mems));
	}
	remove_invalid(backbone, threshold);
	check_backbone(num_seq, backbone);

	return remaining_mems;
}

template <size_t min_length = 0>
std::vector<profile_t> insert_mems_into_chain(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& backbone,
	std::vector<profile_t>&& mems,
	const size_t threshold,
	const std::string& id,
	tui_t::scoped_handle_t& handle)
{
	const size_t num_seq = sequences.size();

	check_backbone(num_seq, backbone);

	if constexpr (min_length > 0)
		mems.erase(
			std::remove_if(mems.begin(), mems.end(), [](const profile_t& mem) {
				return mem.seq.len < min_length;
			}),
			mems.end());

	check_backbone(num_seq, backbone);

	for (size_t round = 0; not mems.empty(); round++) {
		handle.refill(string_builder {} << id << ", round " << round << ": " << mems.size() << " MEMs, " << backbone.size() << " in backbone");
		const size_t old_backbone_size = backbone.size();

		make_consistent(num_seq, backbone, mems, threshold);
		mems = _insert_mems_into_chain<0>(sequences, backbone, std::move(mems), threshold);
		make_consistent(num_seq, backbone, mems, threshold);
		mems = _insert_mems_into_chain<1>(sequences, backbone, std::move(mems), threshold);

		if (backbone.size() == old_backbone_size)
			break;
		check_backbone(num_seq, backbone);
	}
	return mems;
}

std::atomic<size_t> num_added_mems = 0;
void try_to_complete_MEMs(
	const std::string& id,
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& mems,
	tui_t::scoped_handle_t& handle)
{
	const size_t num_seq = sequences.size();

	struct segment_t {
		size_t sequence;
		size_t start = 0, end;
		std::vector<uint32_t> enclosed;
	};
	std::vector<std::string_view> mem_patterns(mems.size());
	std::vector<uint32_t> mem_pattern_seq(mems.size());
	std::vector<segment_t> segments;
	{
		std::vector<segment_t> partials(num_seq);
		for (size_t i = 0; i < mems.size(); i++) {
			for (size_t s = 0; s < num_seq; s++)
				if (mems[i].present(s)) {
					if (mems[i].length(s) > 0) {
						mem_patterns[i] = sequences[s].substr(mems[i].pos(s), mems[i].length(s));
						mem_pattern_seq[i] = s;
					}
					if (not partials[s].enclosed.empty()) {
						partials[s].end = mems[i].pos(s);
						partials[s].sequence = s;
						segments.emplace_back(std::move(partials[s]));
					}
					partials[s].enclosed.clear();
					partials[s].start = mems[i].pos(s) + mems[i].length(s);
				} else if (not mems[i].aligned()) {
					partials[s].enclosed.emplace_back(i);
				}
			// assert(not mem_patterns[i].empty());
		}
		for (size_t s = 0; s < num_seq; s++)
			if (not partials[s].enclosed.empty()) {
				partials[s].end = sequences[s].size();
				partials[s].sequence = s;
				segments.emplace_back(std::move(partials[s]));
			}
	}

	std::vector<bool> need_profile_cleanup(mems.size(), false);
	for (size_t seg_i = 0; seg_i < segments.size(); seg_i++) {
		segment_t& seg = segments[seg_i];
		handle.refill(string_builder {} << id << ": expanding MEM " << seg_i
										<< '(' << seg.enclosed.size() << " MEMs, " << seg.end - seg.start << " bases in seq. " << seg.sequence << ") /"
										<< segments.size());
		std::vector<std::string_view> queries(seg.enclosed.size());
		for (size_t i = 0; i < seg.enclosed.size(); i++)
			queries[i] = mem_patterns[seg.enclosed[i]];
		assert(not queries.empty());
		auto ans = minimap2(id + '.' + std::to_string(seg_i) + '.' + std::to_string(seg.sequence),
			queries,
			sequences[seg.sequence].substr(seg.start, seg.end - seg.start));
		// find heaviest increasing subsequence of sufficiently good alignments
		assert(ans.size() == seg.enclosed.size());
		std::map<size_t, size_t> st; // end pos, i
		std::vector<size_t> score(ans.size());
		std::vector<size_t> predecessor(ans.size(), std::numeric_limits<size_t>::max());
		std::vector<size_t> length(ans.size());
		for (size_t i = 0; i < ans.size(); i++) {
			if (ans[i].mapq == 255 or ans[i].mapq < 30)
				continue;
			ans[i].start += seg.start;
			if (ans[i].alignment)
				length[i] = bases_consumed(ans[i].alignment->first);
			else
				length[i] = queries[i].size();
			auto it = st.upper_bound(ans[i].start);
			if (it != st.begin())
				score[i] = score[predecessor[i] = std::prev(it)->second];
			score[i] += queries[i].size();
			const size_t end = ans[i].start + length[i];
			it = st.lower_bound(end);
			if (it != st.end() and it->first == end and score[it->second] > score[i])
				continue;
			while (it != st.end() and score[it->second] <= score[i])
				it = st.erase(it);
			st.emplace_hint(it, end, i);
		}
		if (st.empty())
			continue;
		size_t last_start = std::numeric_limits<size_t>::max();
		[[maybe_unused]] size_t last_j = last_start;
		for (size_t i = std::prev(st.end())->second; i != std::numeric_limits<size_t>::max(); i = predecessor[i]) {
			const auto j = seg.enclosed[i];
			profile_t& mem = mems[j];
			if (ans[i].alignment) {
				const auto& [ref_ali, pat_ali] = *(ans[i].alignment);
				if (not mem.aligned()) {
					mem.m_aligned.resize(num_seq);
					const size_t bc = bases_consumed(pat_ali);
					mem.for_each_present_coord([&](size_t s) {
						mem.m_aligned[s] = std::make_pair(bc, pat_ali);
					});
				} else {
					need_profile_cleanup[j] = true;
				}
				mem.m_aligned[seg.sequence] = std::make_pair(length[i], ref_ali);
			} else if (mems[j].aligned()) {
				// we match perfectly to mem_pattern_seq[j], so just take their alignment
				mem.m_aligned[seg.sequence] = mem.m_aligned[mem_pattern_seq[j]];
			}
			assert(j < last_j);
			last_j = j;
			assert(ans[i].start + length[i] <= last_start);
			last_start = ans[i].start;
			mem.seq.coords[seg.sequence] = ans[i].start;
			num_added_mems++;
		}
	}
	for (size_t i = 0; i < mems.size(); i++) {
		if (not need_profile_cleanup[i])
			continue;
		std::vector<std::string_view> sequences;
		size_t total_len = 0;
		mems[i].for_each_present_coord([&](size_t s) {
			auto& t = mems[i].m_aligned[s].second;
			t.erase(std::remove(t.begin(), t.end(), '-'), t.end());
			total_len += t.size();
			sequences.emplace_back(t);
		});
		handle.refill(string_builder {} << id << ": aligning expanded anchor " << i << " (" << total_len << " bases in total)");
		auto res = align_external<true>(sequences, id + ".fix." + std::to_string(i));
		size_t pos = 0;
		mems[i].for_each_present_coord([&](size_t s) {
			assert(pos < res.size());
			mems[i].m_aligned[s].second = std::move(res[pos++]);
		});
	}
}

std::atomic<double> go2_time = 0;
std::atomic<double> st_time = 0, mum_time = 0;
std::atomic<size_t> num_zero_gaps = 0, num_sub_backbones = 0, num_alignable_gaps = 0;

bool split(
	const std::vector<std::string_view>& sequences,
	const std::string& id,
	std::vector<profile_t>& backbone,
	const size_t threshold,
	const bool rec_may_use_pfp,
	const bool may_align,
	const bool strict_bounded,
	thread_pool_t& thread_pool,
	tui_t::scoped_handle_t& handle)
{
	const size_t num_seq = sequences.size();
	check_backbone(num_seq, backbone);

	struct sub_backbone_query {
		size_t start_backbone, end_backbone;
		std::vector<std::pair<size_t, size_t>> subsequences; // (offset, length)
		bool may_align = false;
		bool strict_left = false;
		bool strict_right = false;
	};

	handle.refill(string_builder {} << id << ": computing sub_backbones of " << backbone.size() << " MEMs");

	std::vector<sub_backbone_query> sub_backbones;
	{
		// move window over backbone
		std::vector<size_t> seq_present(num_seq);
		size_t seq_cnt = 0;

		size_t next_uncovered = 0;
		std::vector<size_t> gap_start(num_seq);
		size_t pre_cut = 0;
		bool pre_strict_cut = strict_bounded;

		// (pre_cut, sub_backbones.size(), gap_start)
		std::optional<std::tuple<size_t, size_t, std::vector<size_t>>> last_strict_cut;
		if (strict_bounded) {
			last_strict_cut = std::make_tuple(pre_cut, sub_backbones.size(), gap_start);
		}

		const auto total_added = [&](
									 const std::vector<size_t>& l,
									 const std::vector<size_t>& r) {
			size_t res = 0;
			for (size_t s = 0; s < num_seq; s++) {
				assert(r[s] >= l[s]);
				res += r[s] - l[s];
			}
			return res;
		};

		size_t r = 0;
		for (size_t l = 0; l < backbone.size(); l++) {
			for (; r < backbone.size() and seq_cnt < num_seq; r++)
				backbone[r].for_each_present_coord([&](size_t s) {
					if (seq_present[s]++ == 0)
						seq_cnt++;
				});
			assert(r > l);
			// increase l as much as possible without destroying full coverage
			while (seq_cnt == num_seq and l + 1 < r) {
				bool ok = true;
				backbone[l].for_each_present_coord([&](size_t s) {
					assert(seq_present[s] > 0);
					if (seq_present[s] == 1)
						ok = false;
				});
				if (not ok)
					break;
				backbone[l].for_each_present_coord([&](size_t s) {
					seq_present[s]--;
				});
				l++;
			}

			// cut here
			if (l >= next_uncovered and seq_cnt == num_seq) {
				std::vector<size_t> block_end(num_seq, std::numeric_limits<size_t>::max());
				auto block_start = block_end;
				size_t total_enclosed = 0;
				for (size_t i = l; i < r; i++)
					backbone[i].for_each_present_coord([&](size_t s) {
						if (block_start[s] == std::numeric_limits<size_t>::max())
							block_start[s] = backbone[i].pos(s);

						if (block_end[s] < backbone[i].pos(s)) {
							total_enclosed += backbone[i].pos(s) - block_end[s];
						}
						block_end[s] = backbone[i].pos(s) + backbone[i].length(s);
					});
				for (size_t s = 0; s < num_seq; s++) {
					assert(block_start[s] != std::numeric_limits<size_t>::max());
					assert(block_start[s] <= block_end[s]);
					assert(block_end[s] != std::numeric_limits<size_t>::max());
				}

				const size_t gap_end_id = total_enclosed == 0 ? l : r;
				const auto& gap_end = total_enclosed == 0 ? block_start : block_end;
				if (total_enclosed < 1e7 and pre_cut < gap_end_id) {
					num_sub_backbones++;
					if (total_enclosed == 0)
						num_zero_gaps++;

					if (0 == *std::max_element(gap_start.begin(), gap_start.end()))
						sub_backbones.clear();

					if (total_enclosed == 0
						and not pre_strict_cut
						and last_strict_cut
						and total_added(std::get<2>(*last_strict_cut), gap_start) * 2 <= external_align_max) {
						gap_start = std::get<2>(*last_strict_cut);
						sub_backbones.resize(std::get<1>(*last_strict_cut));
						pre_cut = std::get<0>(*last_strict_cut);
						pre_strict_cut = true;
					}

					std::vector<std::pair<size_t, size_t>> subsequences(num_seq);
					for (size_t s = 0; s < num_seq; s++)
						subsequences[s] = std::make_pair(gap_start[s], gap_end[s] - gap_start[s]);

					if (may_align and total_enclosed == 0 and pre_strict_cut)
						num_alignable_gaps++;
					sub_backbones.emplace_back(
						pre_cut,
						gap_end_id,
						std::move(subsequences),
						may_align,
						pre_strict_cut,
						total_enclosed == 0);

					gap_start = std::move(total_enclosed == 0 ? block_end : block_start);
					pre_cut = total_enclosed == 0 ? r : l;
					next_uncovered = r;
					pre_strict_cut = total_enclosed == 0;
					if (total_enclosed == 0) {
						last_strict_cut = std::make_tuple(pre_cut, sub_backbones.size(), gap_start);
					}
				}
			}

			// remove backbone[l] from window
			backbone[l].for_each_present_coord([&](size_t s) {
				assert(seq_present[s] > 0);
				if (0 == --seq_present[s]) {
					assert(seq_cnt > 0);
					seq_cnt--;
				}
			});
		}
		if (*std::max_element(gap_start.cbegin(), gap_start.cend()) > 0) {
			assert(not sub_backbones.empty());
			bool last_covers_all = true;
			for (size_t s = 0; s < num_seq; s++)
				if (const auto& e = sub_backbones.back().subsequences[s]; e.first + e.second < sequences[s].size())
					last_covers_all = false;
			if (not last_covers_all) {
				if (not pre_strict_cut and last_strict_cut
					and total_added(std::get<2>(*last_strict_cut), gap_start) * 2 <= external_align_max) {
					gap_start = std::get<2>(*last_strict_cut);
					sub_backbones.resize(std::get<1>(*last_strict_cut));
					pre_cut = std::get<0>(*last_strict_cut);
					pre_strict_cut = true;
				}
				std::vector<std::pair<size_t, size_t>> subsequences(num_seq);
				for (size_t s = 0; s < num_seq; s++)
					subsequences[s] = std::make_pair(gap_start[s], sequences[s].size() - gap_start[s]);
				sub_backbones.emplace_back(pre_cut,
					backbone.size(),
					std::move(subsequences),
					may_align,
					pre_strict_cut,
					strict_bounded);
			}
		}
	}

#ifndef NDEBUG
	{
		std::vector<size_t> covered(num_seq);
		for (const profile_t& prof : backbone)
			prof.for_each_present_coord([&](size_t s) {
				covered[s] += prof.length(s);
			});

		std::ofstream out(id + ".subdivision");
		for (size_t s = 0; s < num_seq; s++)
			out << s << ": " << covered[s] << " / " << sequences[s].size() << " covered\n";
		out << "strict_bounded: " << (strict_bounded ? "true" : "false") << '\n';
		for (size_t k = 0; k < sub_backbones.size(); k++) {
			out << k << '\n';
			const auto& [l, r, tmp_sequence_coords, may_align_gap, strict_left, strict_right] = sub_backbones[k];
			out << '\t' << l << ' ' << r << " external " << (may_align_gap ? "allowed" : "disallowed")
				<< " left:  " << (strict_left ? "strict" : "non-strict")
				<< ", right: " << (strict_right ? "strict" : "non-strict")
				<< '\n';
			for (size_t s = 0; s < num_seq; s++) {
				const auto& [p, len] = tmp_sequence_coords[s];
				out << '\t' << s << " len: " << len << " [" << p << "," << p + len << ')' << '\n';
			}
		}
		out.close();
	}
#endif

	if (sub_backbones.empty()) {
		check_backbone(num_seq, backbone);
		return false;
	}

	std::mutex mutex;

	std::atomic<size_t> num_done = 0, num_started = 0;
	const auto refill_handle = [&] {
		std::lock_guard<std::mutex> lock(mutex);
		handle.refill(string_builder {} << id << ": "
										<< num_done << "/" << sub_backbones.size() << " finished, "
										<< (num_started - num_done) << " running");
	};
	refill_handle();
	std::vector<std::vector<profile_t>> results(sub_backbones.size());
	thread_pool.for_each_in_range(
		sub_backbones.size(), [&](size_t k) {
			num_started++;
			refill_handle();

			const auto& [l, r, tmp_sequence_coords, may_align_gap, strict_left, strict_right] = sub_backbones[k];
			const bool strict_bounded = strict_left and strict_right;
			// auto mhandle = tui.handle()
			std::vector<std::string_view> tmp_sequences(num_seq);
			for (size_t s = 0; s < num_seq; s++) {
				const auto& [p, len] = tmp_sequence_coords[s];
				assert(sequences[s].size() >= p + len);
				tmp_sequences[s] = sequences[s].substr(p, len);
			}

			const auto [total_length, remaining_length, max_length] = [&] {
				size_t tot = 0, maximum = 0;
				for (const auto& c : tmp_sequence_coords) {
					tot += c.second;
					maximum = std::max<size_t>(maximum, c.second);
				}
				size_t remaining = tot;
				for (size_t i = l; i < r; i++)
					backbone[i].for_each_present_coord([&](size_t s) {
						remaining -= backbone[i].length(s);
					});
				return std::make_tuple(tot, remaining, maximum);
			}();
			if (max_length == 0) {
				num_done++;
				return;
			}

			std::vector<profile_t> tmp(backbone.begin() + l, backbone.begin() + r);
			check_backbone(tmp_sequences.size(), tmp);
			for (profile_t& prof : tmp)
				prof.for_each_present_coord([&](size_t s) {
					assert(prof.pos(s) >= tmp_sequence_coords[s].first);
					prof.pos(s) -= tmp_sequence_coords[s].first;
				});
			check_backbone(tmp_sequences.size(), tmp);
			go2(tmp_sequences, tmp, threshold,
				id + "." + std::to_string(k),
				thread_pool,
				rec_may_use_pfp, may_align_gap, strict_bounded);
			for (profile_t& prof : tmp) {
				prof.for_each_present_coord([&](size_t s) {
					prof.pos(s) += tmp_sequence_coords[s].first;
				});
			}

			results[k] = std::move(tmp);

			num_done++;
			refill_handle();
		},
		true);

	handle.refill(string_builder {} << id << " merging after split...");
	auto old_backbone = std::move(backbone);
	backbone.clear();

	std::vector<profile_t> acc;
	const auto finish = [&] {
		// check_backbone(num_seq, acc);
		backbone.insert(backbone.end(),
			std::make_move_iterator(acc.begin()),
			std::make_move_iterator(acc.end()));
		// check_backbone(num_seq, backbone);
		acc.clear();
	};
	size_t next_i = 0;
	std::vector<size_t> last_pos(num_seq);
	bool last_strict = strict_bounded;
	for (size_t k = 0; k < sub_backbones.size(); k++) {
		const auto& [l, r, tmp_sequence_coords, may_align_gap, strict_left, strict_right] = sub_backbones[k];

		if (last_strict and strict_left)
			for (; next_i < l; next_i++) {
#ifndef NDEBUG
				old_backbone[next_i].for_each_present_coord([&](const size_t s) {
					assert(last_pos[s] <= old_backbone[next_i].pos(s));
					last_pos[s] = old_backbone[next_i].pos(s) + old_backbone[next_i].length(s);
				});
#endif
				acc.emplace_back(std::move(old_backbone[next_i]));
			}
		last_strict = strict_right;
		next_i = r;

		if (not strict_left) {
			if (acc.empty())
				acc = std::move(results[k]);
			else
				insert_mems_into_chain(sequences, acc, std::move(results[k]), threshold, id, handle);
		} else {
			finish();
			if (not strict_right) {
				acc = std::move(results[k]);
			} else {
				// check_backbone(num_seq, backbone);
				backbone.insert(backbone.end(),
					std::make_move_iterator(results[k].begin()),
					std::make_move_iterator(results[k].end()));
				// check_backbone(num_seq, backbone);
			}
		}
	}
	if (last_strict and strict_bounded)
		for (; next_i < old_backbone.size(); next_i++)
			acc.emplace_back(std::move(old_backbone[next_i]));
	finish();
	check_backbone(num_seq, backbone);

	return true;
}

bool try_without_empty(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& backbone,
	const std::string& id,
	bool may_use_pfp,
	bool may_align,
	bool strict_bounded,
	tui_t::scoped_handle_t& handle,
	thread_pool_t& thread_pool)
{
	const size_t num_seq = sequences.size();
	std::vector<bool> is_empty(num_seq);
	size_t max_len = 0;
	size_t total_len = 0;
	for (size_t s = 0; s < num_seq; s++) {
		max_len = std::max(max_len, sequences[s].size());
		total_len += sequences[s].size();
		if (sequences[s].empty())
			is_empty[s] = true;
	}
	// discard relatively small sequences without match
	if (not backbone.empty() and 2 * total_len > external_align_max) {
		for (size_t s = 0; s < num_seq; s++)
			if (sequences[s].size() <= 1e2)
				is_empty[s] = true;
		for (const profile_t& prof : backbone)
			prof.for_each_present_coord([&](size_t s) {
				if (prof.length(s) > 0)
					is_empty[s] = false;
			});
	}
	const size_t num_non_empty = std::count(is_empty.begin(), is_empty.end(), false);
	if (num_non_empty == 0)
		return true;
	if (num_non_empty == num_seq)
		return false;

	std::vector<std::string_view> non_empty_sequences;
	std::vector<size_t> new_seq_id(num_seq, std::numeric_limits<size_t>::max());
	for (size_t s = 0; s < num_seq; s++)
		if (not is_empty[s]) {
			new_seq_id[s] = non_empty_sequences.size();
			non_empty_sequences.emplace_back(sequences[s]);
		}
	assert(non_empty_sequences.size() == num_non_empty);
	for (profile_t& prof : backbone) {
		auto tmp = prof;
		for (size_t s = 0; s < num_seq; s++) {
			if (is_empty[s] or new_seq_id[s] == s)
				continue;
			assert(new_seq_id[s] <= s);
			prof.seq.coords[new_seq_id[s]] = prof.seq.coords[s];
			if (prof.aligned())
				prof.m_aligned[new_seq_id[s]] = std::move(prof.m_aligned[s]);
		}
		if (prof.aligned())
			prof.m_aligned.resize(num_non_empty);
		prof.seq.coords.resize(num_non_empty);
		for (size_t s = 0; s < num_seq; s++) {
			if (is_empty[s])
				continue;
			if (tmp.aligned())
				assert(tmp.m_aligned[s] == prof.m_aligned[new_seq_id[s]]);
			assert(tmp.pos(s) == prof.pos(new_seq_id[s]));
		}
	}

	const size_t new_threshold = num_non_empty / 2 + 1;
	// by discarding 0-occurrences in small sequences, we may have removed some coords
	remove_invalid(backbone, new_threshold);

	check_backbone(num_non_empty, backbone);

	if (not split(non_empty_sequences, id, backbone, new_threshold, may_use_pfp, may_align, strict_bounded, thread_pool, handle)) {
		handle.refill(string_builder {} << id << " recursing with " << num_non_empty
										<< " sequences because the rest are empty");
		go2(non_empty_sequences, backbone, new_threshold, id, thread_pool, may_use_pfp, may_align, strict_bounded);
		check_backbone(num_non_empty, backbone);
	}

	check_backbone(num_non_empty, backbone);
	std::vector<size_t> last_pos(num_seq);
	for (profile_t& prof : backbone) {
		if (prof.aligned())
			prof.m_aligned.resize(num_seq);
		prof.seq.coords.resize(num_seq);
		size_t real_s = num_seq;
		size_t width = 0;
		for (size_t s = num_non_empty; s > 0;) {
			s--;
			assert(real_s > s);
			while (is_empty[--real_s]) {
				prof.seq.coords[real_s] = last_pos[real_s];
			}
			assert(real_s >= s);
			prof.seq.coords[real_s] = prof.seq.coords[s];
			if (prof.aligned() and s != real_s)
				prof.m_aligned[real_s] = std::move(prof.m_aligned[s]);
			if (prof.aligned() and prof.present(real_s))
				width = prof.m_aligned[real_s].second.size();
		}
		while (real_s > 0) {
			real_s--;
			assert(is_empty[real_s]);
			prof.seq.coords[real_s] = last_pos[real_s];
		}
		if (prof.aligned()) {
			assert(width > 0);
			for (size_t s = 0; s < num_seq; s++)
				if (is_empty[s])
					prof.m_aligned[s] = std::make_pair(0, std::string(width, '-'));
		} else if ((width = prof.seq.len) > 0) {
			prof.m_aligned.resize(num_seq);
			for (size_t s = 0; s < num_seq; s++)
				if (is_empty[s])
					prof.m_aligned[s] = std::make_pair(0, std::string(width, '-'));
				else if (prof.present(s))
					prof.m_aligned[s] = std::make_pair(width, sequences[s].substr(prof.pos(s), width));
		}
		prof.for_each_present_coord([&](size_t s) {
			assert(last_pos[s] <= prof.pos(s));
			last_pos[s] = prof.pos(s) + prof.length(s);
			assert(last_pos[s] <= sequences[s].size());
		});
		check_profile(prof);
	}
	check_backbone(num_seq, backbone);
	try_to_complete_MEMs(id, sequences, backbone, handle);
	check_backbone(num_seq, backbone);

	return true;
}

bool fill_in_empty(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& profiles)
{
	const size_t num_seq = sequences.size();

	std::vector<std::vector<size_t>> start_positions(num_seq);
	for (const profile_t& prof : profiles)
		prof.for_each_present_coord([&](size_t s) {
			start_positions[s].emplace_back(prof.pos(s));
		});
	for (size_t s = 0; s < num_seq; s++)
		start_positions[s].emplace_back(sequences[s].size());
	std::vector<size_t> start_pos_idx(num_seq);

	std::vector<size_t> last_pos(num_seq);
	bool change = false;
	for (size_t i = 0; i < profiles.size(); i++) {
		profile_t& prof = profiles[i];
		size_t reference = 0;
		while (not prof.present(reference))
			reference++;
		size_t width = prof.seq.len;
		if (prof.aligned())
			width = prof.m_aligned[reference].second.size();

		for (size_t s = 0; s < num_seq; s++) {
			const size_t next_pos = start_positions[s][start_pos_idx[s]];
			if (prof.present(s)) {
				assert(next_pos == prof.pos(s));
				start_pos_idx[s]++;
				last_pos[s] = prof.pos(s) + prof.length(s);
				continue;
			}
			assert(next_pos >= last_pos[s]);
			if (next_pos > last_pos[s])
				continue;
			change = true;
			if (width > 0) {
				if (not prof.aligned()) {
					prof.m_aligned.resize(num_seq);
					prof.for_each_present_coord([&](size_t s) {
						prof.m_aligned[s].first = prof.seq.len;
						prof.m_aligned[s].second = sequences[s].substr(prof.pos(s), prof.seq.len);
					});
				}
				prof.m_aligned[s] = std::make_pair(0, std::string(prof.m_aligned[reference].second.size(), '-'));
			}
			prof.seq.coords[s] = last_pos[s];
		}
	}
	return change;
}

// search for consecutive and sufficiently long chains of MEMs that exclude a specific sequence.
// We declare this sequence as not present in this chain and "align" it arbitrarily
void fix_outliers(
	const std::string& id,
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& profiles,
	tui_t::scoped_handle_t& handle)
{
	const size_t num_seq = sequences.size();
	std::vector<size_t> last_pos(num_seq);
	std::vector<size_t> last_idx(num_seq, 0);

	std::vector<size_t> covered_pfx_sum(1, 0);
	std::vector<size_t> sufficiently_wide_pfx(1, 0);

	constexpr size_t min_skipped_dist = 1e9;
	constexpr size_t min_skipped_small_dist = 99;
	// const size_t min_skipped_small_coverage = std::min(num_seq - 1, (9 * num_seq + (10 - 1)) / 10); // >= 90%
	const size_t min_skipped_small_coverage = std::min(num_seq - 1, (6 * num_seq + (10 - 1)) / 10); // >= 60%
	constexpr size_t min_num_skipped_mems = 1;

	std::vector<profile_t> res;
	{ // add dummy profile for end of sequences
		profile_t tmp;
		tmp.seq.coords.resize(num_seq);
		tmp.seq.len = 0;
		for (size_t s = 0; s < num_seq; s++)
			tmp.seq.coords[s] = sequences[s].size();
		profiles.emplace_back(std::move(tmp));
	}
	for (size_t i = 0; i < profiles.size(); i++) {
		size_t min_length = std::numeric_limits<size_t>::max();
		std::vector<my_int> insert_pos(num_seq, MEM_t::NOT_PRESENT);
		{
			size_t num_covered = 0;
			profiles[i].for_each_present_coord([&](size_t s) {
				insert_pos[s] = last_pos[s];
				num_covered++;
			});
			sufficiently_wide_pfx.emplace_back(sufficiently_wide_pfx.back() + !!(num_covered >= min_skipped_small_coverage));
		}
		const auto check_width_since = [&](size_t last_idx) -> bool {
			return sufficiently_wide_pfx[i] > sufficiently_wide_pfx[last_idx];
		};
		bool non_empty = i + 1 == profiles.size();
		profiles[i].for_each_present_coord([&](size_t s) {
			if (profiles[i].length(s) > 0)
				non_empty = true;
		});
		std::vector<size_t> splitted;
		profiles[i].for_each_present_coord([&](size_t s) {
			const size_t skipped = profiles[i].pos(s) - last_pos[s];
			const size_t others_min_skipped = covered_pfx_sum.back() - covered_pfx_sum[last_idx[s]];
			const size_t num_mems_skipped = covered_pfx_sum.size() - 1 - last_idx[s];

			const bool split_here = num_mems_skipped >= min_num_skipped_mems
				and (skipped > 0 or non_empty)
				and (others_min_skipped >= min_skipped_dist
					or (others_min_skipped >= min_skipped_small_dist and check_width_since(last_idx[s])));
			if (split_here)
				splitted.emplace_back(s);

			last_idx[s] = covered_pfx_sum.size();
			if (profiles[i].length(s) > 0)
				min_length = std::min(min_length, profiles[i].length(s));
			last_pos[s] = profiles[i].pos(s) + profiles[i].length(s);
		});
		assert(i + 1 == profiles.size() or not non_empty or min_length < std::numeric_limits<size_t>::max());
		if (min_length == std::numeric_limits<size_t>::max())
			min_length = 0;
		bool ok = false;
		for (size_t s : splitted)
			if (profiles[i].pos(s) > insert_pos[s] and profiles[i].length(s) > 0)
				ok = true;
		if (ok) {
			profile_t tmp;
			tmp.seq.coords = insert_pos;
			tmp.seq.len = 0;
			res.emplace_back(std::move(tmp));
			for (size_t s : splitted)
				insert_pos[s] = profiles[i].pos(s);
		}
		covered_pfx_sum.emplace_back(covered_pfx_sum.back() + min_length);

		if (i + 1 == profiles.size()) {
			assert(not profiles[i].aligned());
			assert(profiles[i].seq.len == 0);
			for (size_t s = 0; s < num_seq; s++)
				assert(profiles[i].seq.coords[s] == sequences[s].size());
		} else
			res.emplace_back(std::move(profiles[i]));
	}
	profiles = std::move(res);
	check_backbone(num_seq, profiles);
	if (fill_in_empty(sequences, profiles)) {
		check_backbone(num_seq, profiles);
		fix_outliers(id, sequences, profiles, handle);
	}
	check_backbone(num_seq, profiles);
}

// TODO: remove, this never does anything anyway
void fix_unbalanced(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& backbone,
	const std::string& id,
	thread_pool_t& thread_pool)
{
	const size_t num_seq = sequences.size();

	std::vector<size_t> remaining_length(num_seq);
	size_t total_length = 0;
	for (size_t i = 0; i < num_seq; i++) {
		remaining_length[i] = sequences[i].size();
		total_length += remaining_length[i];
	}
	for (const profile_t& prof : backbone)
		prof.for_each_present_coord([&](size_t s) {
			remaining_length[s] -= prof.length(s);
		});

	const size_t max_length = *std::max_element(remaining_length.begin(), remaining_length.end());
	const size_t min_length = *std::min_element(remaining_length.begin(), remaining_length.end());

	// when the max difference in length is less than 2x, we do not expect a benefit when splitting
	if (total_length >= 1e6 or max_length >= std::max<size_t>(1, 2 * min_length))
		return;
	std::vector<bool> is_small(num_seq);
	std::vector<std::string_view> small_seq, large_seq;
	for (size_t i = 0; i < num_seq; i++) {
		is_small[i] = (max_length >= 2 * remaining_length[i]);
		(is_small[i] ? small_seq : large_seq).emplace_back(sequences[i]);
	}
	if (small_seq.empty() or large_seq.empty())
		return;
	const size_t small_threshold = small_seq.size() / 2 + 1;
	const size_t large_threshold = large_seq.size() / 2 + 1;

	std::vector<profile_t> small_backbone, large_backbone;
	for (profile_t& mem : backbone) {
		profile_t small_m, large_m;
		small_m.seq.len = large_m.seq.len = mem.seq.len;
		small_m.seq.coords.reserve(small_seq.size());
		large_m.seq.coords.reserve(large_seq.size());
		size_t small_ok = 0, large_ok = 0;
		for (size_t s = 0; s < num_seq; s++) {
			(is_small[s] ? small_m : large_m).seq.coords.emplace_back(mem.pos(s));
			if (mem.aligned())
				(is_small[s] ? small_m : large_m).m_aligned.emplace_back(std::move(mem.m_aligned[s]));
			if (mem.present(s))
				(is_small[s] ? small_ok : large_ok)++;
		}
		assert(small_m.num_seq() == small_seq.size());
		assert(large_m.num_seq() == large_seq.size());
		// assert(small_ok >= small_threshold or large_ok >= large_threshold);
		if (small_ok >= small_threshold)
			small_backbone.emplace_back(std::move(small_m));
		if (large_ok >= large_threshold)
			large_backbone.emplace_back(std::move(large_m));
	}
	backbone.clear();

	assert(small_threshold * 2 > small_seq.size());
	assert(large_threshold * 2 > large_seq.size());

	auto handle = tui.handle();
	handle.refill(string_builder {}
		<< id << " splitting " << max_length << " vs. " << min_length
		<< ": " << small_seq.size() << "," << large_seq.size() << " (small)");
	auto small_aligned = malign(small_seq, std::move(small_backbone), small_threshold, id + "_small", thread_pool);
	handle.refill(string_builder {}
		<< id << " splitting " << max_length << " vs. " << min_length
		<< ": " << small_seq.size() << "," << large_seq.size() << " (large)");
	auto large_aligned = malign(large_seq, std::move(large_backbone), large_threshold, id + "_large", thread_pool);

	handle.refill(string_builder {}
		<< id << " merging " << small_aligned.size() << ", " << large_aligned.size()
		<< " (total len: " << total_length << ")");
	auto merged = merge_external(id, std::move(small_aligned), std::move(large_aligned));
	assert(merged.size() == num_seq);
	std::vector<std::string> res(merged.size());
	size_t small_i = 0, large_i = small_seq.size();
	for (size_t i = 0; i < num_seq; i++)
		if (is_small[i])
			res[i] = std::move(merged[small_i++]);
		else
			res[i] = std::move(merged[large_i++]);

	profile_t prof;
	prof.seq.coords.resize(num_seq);
	prof.m_aligned.resize(num_seq);
	for (size_t s = 0; s < num_seq; s++) {
		prof.m_aligned[s].first = sequences[s].size();
		prof.m_aligned[s].second = std::move(res[s]);
	}
	check_profile(prof);
	backbone.emplace_back(std::move(prof));
	check_backbone(num_seq, backbone);
}

void go2(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>& backbone,
	const size_t threshold,
	const std::string& id,
	thread_pool_t& thread_pool,
	bool may_use_pfp,
	bool may_align,
	bool strict_bounded)
{
	const size_t num_seq = sequences.size();
	if (num_seq == 0)
		return;
	if (num_seq == 1) {
		backbone.clear();
		profile_t prof;
		prof.seq.coords.resize(1, 0);
		prof.seq.len = sequences[0].size();
		backbone.emplace_back(std::move(prof));
		return;
	}
	if (false and backbone.empty() and may_align) {
		const auto longest_prefix = find_longest_prefix(sequences);
		if (longest_prefix > 0) {
			std::vector<std::string_view> rest(num_seq);
			for (size_t s = 0; s < num_seq; s++)
				rest[s] = sequences[s].substr(longest_prefix);
			go2(rest, backbone, threshold, id, thread_pool, may_use_pfp, may_align, strict_bounded);

			for (auto& p : backbone)
				for (auto& c : p.seq.coords)
					c += longest_prefix;

			profile_t prof;
			prof.seq.len = longest_prefix;
			prof.seq.coords.resize(num_seq, 0);
			backbone.insert(backbone.begin(), std::move(prof));
			return;
		}
	}

	check_backbone(num_seq, backbone);

	auto handle = tui.handle();
	if (try_without_empty(sequences, backbone, id, may_use_pfp, may_align, strict_bounded, handle, thread_pool)) {
		check_backbone(num_seq, backbone);
		return;
	}
	if (may_align and strict_bounded) {
		size_t min_length = std::numeric_limits<size_t>::max(), max_length = 0;
		for (const auto& s : sequences) {
			min_length = std::min(min_length, s.size());
			max_length = std::max(max_length, s.size());
		}
		handle.refill(string_builder {} << id << " external [" << min_length << ',' << max_length << ']');
		auto aligned = align_external(sequences, id);
		if (aligned) {
			assert(aligned->size() == num_seq);
			profile_t prof;
			prof.seq.len = (*aligned)[0].size();
			prof.seq.coords.resize(num_seq, 0);
			for (size_t s = 0; s < num_seq; s++) {
				if (s + 1 < num_seq)
					assert((*aligned)[s].size() == (*aligned)[s + 1].size());

				prof.m_aligned.emplace_back(sequences[s].size(), std::move((*aligned)[s]));
			}
			check_profile(prof);
			backbone.clear();
			backbone.emplace_back(std::move(prof));
			check_backbone(num_seq, backbone);
			return;
		}
	}

	handle.refill(string_builder {} << id << " preparing");

	sys_timer timer;
	timer.start();

	assert(2 * threshold > num_seq);
	assert(threshold <= num_seq);

	double m_st_time, m_mum_time;

	std::vector<profile_t> mems;
	// [start of gap, end of gap), where a gap is already somehow aligned
	std::vector<std::vector<std::pair<size_t, size_t>>> gaps(num_seq);
	for (const profile_t& prof : backbone)
		prof.for_each_present_coord([&](size_t s) {
			gaps[s].emplace_back(prof.pos(s), prof.pos(s) + prof.length(s));
		});

	std::vector<size_t> covered(num_seq);

	const auto [use_pfp, max_len] = [&]() {
		size_t max_len = 0, sum_len = 0;
		for (size_t s = 0; s < num_seq; s++) {
			for (const auto& [l, r] : gaps[s])
				covered[s] += r - l;
			const size_t len = sequences[s].size() - covered[s];
			max_len = std::max(max_len, len);
			sum_len += len;
		}
		return std::make_pair(may_use_pfp and max_len > 1e4 and sum_len > 1e6, max_len);
	}();

	boundaries_t boundaries(num_seq, 0);

	if (use_pfp) {
		dict_builder_t D;
		const std::string parse_path = gap_fill_config.tmp_directory + "/" + id + FEX_parse;
		std::ofstream parse(parse_path, std::ios::out | std::ios::binary);
		const size_t w = 10, p = 20;
		handle.refill(string_builder {} << id << " PFP");
		for (size_t s = 0; s < num_seq; s++) {
			boundaries[s] = ((uint64_t)parse.tellp()) / sizeof(uint64_t);
			struct in_t {
				size_t num;
				const char* ptr;
				char peek() const
				{
					if (num == 0)
						return '\0';
					return *ptr;
				}
				bool get(char& c)
				{
					if (num == 0)
						return false;
					num--;
					c = *(ptr++);
					return true;
				}
			};
			const auto add_until = [&](size_t l, size_t r) {
				assert(l <= r);
				if (l == r)
					return;
				in_t in { .num = r - l, .ptr = &sequences[s][l] };
				add_sequence(in, w, p, D, parse);
			};
			size_t last = 0;
			for (const auto& [l, r] : gaps[s]) {
				assert(last <= l);
				add_until(last, l);
				last = r;
			}
			add_until(last, sequences[s].size());
		}
		const size_t parse_length = ((uint64_t)parse.tellp()) / sizeof(uint64_t);
		parse.close();

		handle.refill(string_builder {} << id << " refining parse");
		{
			std::vector<word_stats*> P_arr = calc_ranks(D, false);
			refine_parse(parse_path, D);
			store_dict(P_arr, id, false);

			auto tmp = std::move(D);
		} // clear dict

		handle.refill(string_builder {} << id << " constructing pfp-ST for parse of size " << parse_length);
		sys_timer st_timer;
		st_timer.start();
		suffix_tree_t ST;
		sdsl::construct(ST, parse_path, 4);
		m_st_time = st_timer.stop_and_get();
		handle.refill(string_builder {} << id << " finding MUMs in pfp-ST for parse of size " << parse_length);

		st_timer = sys_timer {};
		st_timer.start();
		mum::mums(
			boundaries, ST,
			[&](my_int len, std::vector<my_int>&& coords) {
				mems.emplace_back(len, std::move(coords));
			},
			threshold,
			1u, // match length, we filter after coord translation
			nullptr // threadpool
		);
		if (mems.empty()) {
			mum::rep_mems(
				boundaries, ST,
				[&](my_int len, std::vector<my_int>&& coords) {
					mems.emplace_back(len, std::move(coords));
				},
				threshold,
				1u,
				nullptr);
			#ifndef NO_DEBUG_OUTPUT
			std::cout << id << ": found " << mems.size() << " rMEMs with pfp" << std::endl;
			#endif
		}
		m_mum_time = st_timer.stop_and_get();

		{
			auto tmp = std::move(ST);
		}

		handle.refill(string_builder {} << id << " translating PFP coords of MEMs");
		parse_t P = read_parses(gap_fill_config.tmp_directory + "/" + id);
		DICT_t dict = load_dict_from_file(id);
		const auto parse_length_pfx = get_parse_length_pfx(P, dict);

		// translate coords
		for (profile_t& mem : mems) {
			size_t some_start = std::numeric_limits<size_t>::max();
			mem.for_each_present_coord([&](size_t s) {
				auto& c = mem.pos(s);
				if (some_start != std::numeric_limits<size_t>::max())
					for (size_t i = 0; i < mem.length(i); i++)
						assert(P[some_start + i] == P[c + i]);
				some_start = c;
				assert(c >= boundaries[s]);
				assert(c < (s + 1 < num_seq ? boundaries[s + 1] : P.size()));
				c = parse_length_pfx[c] - parse_length_pfx[boundaries[s]];
			});
			size_t len = 0;
			for (size_t i = 0; i < mem.seq.len; i++) {
				const size_t l = dict.get_length(P[some_start + i]);
				assert(l > 0);
				len += l;
			}
			mem.seq.len = len;
		}
		{
			const auto dict_path = id + FEX_dict;
			const auto dictx_path = id + FEX_dictx;
			std::remove(parse_path.c_str());
			std::remove(dict_path.c_str());
			std::remove(dictx_path.c_str());
		}
		if (true)
			mems.erase(
				std::remove_if(mems.begin(), mems.end(), [](const profile_t& mem) {
					return mem.seq.len < 100;
				}),
				mems.end());
	} else {
		std::vector<uint32_t> generated;

		handle.refill(string_builder {} << id << " concatenating gaps");
		size_t num_separators = 0;
		boundaries_t boundaries(num_seq);
		for (size_t s = 0; s < num_seq; s++) {
			boundaries[s] = generated.size();

			size_t last = 0;
			for (const auto& [l, r] : gaps[s]) {
				assert(last <= l);
				for (size_t i = last; i < l; i++)
					generated.emplace_back((uint8_t)sequences[s][i]);
				generated.emplace_back(256 + ++num_separators);
				last = r;
			}
			if (last < sequences[s].size()) {
				for (size_t i = last; i < sequences[s].size(); i++)
					generated.emplace_back((uint8_t)sequences[s][i]);
				generated.emplace_back(256 + ++num_separators);
			}
		}

		{
			size_t min_length = std::numeric_limits<size_t>::max(), max_length = 0;
			for (size_t s = 0; s < num_seq; s++) {
				const size_t len = (s + 1 < num_seq ? boundaries[s + 1] : generated.size()) - boundaries[s];
				min_length = std::min(min_length, len);
				max_length = std::max(max_length, len);
			}
			handle.refill(string_builder {} << id << " constructing ST for " << generated.size() << " symbols ["
											<< min_length << "," << max_length << ']');
		}
		sys_timer st_timer;
		st_timer.start();
		auto ST = construct_ST_im(std::move(generated), id);
		m_st_time = st_timer.stop_and_get();
		handle.refill(string_builder {} << id << " finding MUMs in ST for " << generated.size() << " symbols");

		st_timer = sys_timer {};
		st_timer.start();
		mum::mums(
			boundaries, ST,
			[&](my_int len, std::vector<my_int>&& coords) {
				mems.emplace_back(len, std::move(coords));
			},
			threshold,
			1u, // max_len >= 1e4 ? 100u : 1u,
			nullptr);
		if (mems.empty()) {
			mum::rep_mems(
				boundaries, ST,
				[&](my_int len, std::vector<my_int>&& coords) {
					mems.emplace_back(len, std::move(coords));
				},
				threshold,
				1u,
				nullptr);
			#ifndef NO_DEBUG_OUTPUT
			std::cout << id << ": found " << mems.size() << " rMEMs w/o pfp" << std::endl;
			#endif
		}
		m_mum_time = st_timer.stop_and_get();

		// #pragma omp parallel for
		for (profile_t& mem : mems)
			mem.for_each_present_coord([&](size_t s) {
				mem.pos(s) -= boundaries[s];
			});
	}

	// translate MEM coords
	{
		handle.refill(string_builder {} << id << " translating coords of " << backbone.size() << " MEMs");
		// (start of gap, offset)
		std::vector<std::vector<std::pair<size_t, size_t>>> gap_offset(num_seq);
		std::vector<size_t> last(num_seq), total_offset(num_seq), cur_pos(num_seq);
		for (const profile_t& prof : backbone) {
			prof.for_each_present_coord([&](size_t s) {
				gap_offset[s].emplace_back(cur_pos[s], total_offset[s]);

				cur_pos[s] += prof.pos(s) - last[s] + !use_pfp; // gap + sentinel
				last[s] = prof.pos(s) + prof.length(s);
				total_offset[s] += prof.length(s) - !use_pfp; // mem - sentinel
			});
		}
		for (size_t s = 0; s < num_seq; s++)
			gap_offset[s].emplace_back(cur_pos[s], total_offset[s]);

		for (profile_t& mem : mems) {
#ifndef NDEBUG
			size_t last_seq = num_seq, last_i = 0;
#endif
			mem.for_each_present_coord([&](size_t s) {
				auto it = std::upper_bound(gap_offset[s].begin(), gap_offset[s].end(), mem.pos(s),
					[](const my_int p, const std::pair<size_t, size_t>& v) {
						return p < v.first;
					});
				assert(it != gap_offset[s].begin());
				it = std::prev(it);
				assert(it->first <= mem.pos(s));
				assert(std::next(it) == gap_offset[s].end() or std::next(it)->first > mem.pos(s));
				mem.pos(s) += it->second;

#ifndef NDEBUG
				if (last_seq != num_seq) {
					for (size_t i = 0; i < mem.length(s); i++)
						assert(sequences[s][mem.pos(s) + i] == sequences[last_seq][last_i + i]);
				}
				last_seq = s, last_i = mem.pos(s);
#endif
			});
		}
	}

	const size_t old_backbone_size = backbone.size();
	check_backbone(num_seq, backbone);
	if (use_pfp and max_len > 1e6)
		mems = insert_mems_into_chain<100>(sequences, backbone, std::move(mems), threshold, id, handle);
	else
		mems = insert_mems_into_chain<0>(sequences, backbone, std::move(mems), threshold, id, handle);

	try_to_complete_MEMs(id, sequences, backbone, handle);
	check_backbone(num_seq, backbone);
	fix_outliers(id, sequences, backbone, handle);
	check_backbone(num_seq, backbone);

	const size_t num_mems_added = backbone.size() - old_backbone_size;

	go2_time += timer.stop_and_get();
	st_time += m_st_time, mum_time += m_mum_time;

	if (num_mems_added == 0 and not use_pfp) {
		size_t num_bases = 0;
		for (const auto& s : sequences)
			num_bases += s.size();
		#ifndef NO_DEBUG_OUTPUT
		std::cout << id << " not recursing because couldn't add mems to chain, "
				  << mems.size() << " MEMs not in chain (" << num_seq << " sequences, " << num_bases << " bases)"
				  << std::endl;
		#endif
		return;
	}
	const bool rec_may_use_pfp = may_use_pfp and num_mems_added > 0;

	if (not split(sequences, id, backbone, threshold, rec_may_use_pfp, may_align, strict_bounded, thread_pool, handle)) {
		handle.refill(string_builder {} << id << " recursing with backbone of " << backbone.size() << " MEMs");
		go2(sequences, backbone, threshold, id, thread_pool, rec_may_use_pfp, may_align, strict_bounded);
		check_backbone(num_seq, backbone);
		if (strict_bounded)
			fix_unbalanced(sequences, backbone, id, thread_pool);
		check_backbone(num_seq, backbone);
	}
}

std::vector<std::string> malign(
	const std::vector<std::string_view>& sequences,
	std::vector<profile_t>&& backbone,
	const size_t threshold,
	const std::string& id,
	thread_pool_t& thread_pool)
{
	const size_t num_seq = sequences.size();
	assert(num_seq > 0);
	if (num_seq == 1) {
		std::vector<std::string> res;
		res.emplace_back(sequences[0]);
		return res;
	}

	{
		size_t max_length = 0;
		for (size_t s = 0; s < num_seq; s++)
			max_length = std::max(max_length, sequences[s].size());
		size_t diff = 0;
		for (size_t s = 0; s < num_seq; s++)
			diff = max(diff, max_length - sequences[s].size());
		if (max_length + diff > 1e8) {
			std::vector<std::string> res(num_seq, "-");
			std::cout << "not aligning " << id << " because it's too large (" << max_length << "," << diff << ")" << std::endl;
			return res;
		}
	}

	go2(sequences, backbone, threshold, id, thread_pool, true /*may_use_pfp*/, true /*may_align*/, true /*strict_bounded*/);

	std::vector<std::string> res(num_seq);
	std::vector<size_t> cur_pos(num_seq); // position of the first char not consumed
	for (profile_t& mem : backbone) {
		size_t reference_seq = -1, reference_pos = -1;
		size_t max_pos = 0; // for alignment
		mem.for_each_present_coord([&](size_t s) {
			// consume all chars up to mem.coords[s], i.e.
			// append gaps[s][cur_pos[s]..mem.coords[s])

			assert(cur_pos[s] <= sequences[s].size());
			assert(mem.pos(s) + mem.length(s) <= sequences[s].size());
			if constexpr (output_remaining_gaps) {
				assert(sequences[s].size() >= cur_pos[s] + (mem.pos(s) - cur_pos[s]));
				res[s] += sequences[s].substr(cur_pos[s], mem.pos(s) - cur_pos[s]);
			} else
				res[s].resize(res[s].size() + mem.pos(s) - cur_pos[s], 'N');
			max_pos = std::max(max_pos, res[s].size());

			if (not mem.aligned()) {
				if (reference_seq != (size_t)-1) {
					assert(sequences[reference_seq].size() >= reference_pos);
					assert(sequences[s].size() >= mem.pos(s));
					assert(sequences[reference_seq].substr(reference_pos, mem.length(reference_seq)) == sequences[s].substr(mem.pos(s), mem.length(s)));
				}
				reference_seq = s, reference_pos = mem.pos(s);
			}
		});
		if (mem.aligned()) {
			mem.for_each_present_coord([&](size_t s) {
				assert(res[s].size() <= max_pos);
				res[s].resize(max_pos, '-');

				res[s] += std::move(mem.m_aligned[s].second);
				cur_pos[s] = mem.pos(s) + mem.length(s);
			});
		} else {
			assert(sequences[reference_seq].size() >= reference_pos + mem.length(reference_seq));
			const std::string_view str = sequences[reference_seq].substr(reference_pos, mem.length(reference_seq));
			assert(str.size() == mem.seq.len);

			// align and then append str
			mem.for_each_present_coord([&](size_t s) {
				assert(res[s].size() <= max_pos);
				res[s].resize(max_pos, '-');
				res[s] += str;

				cur_pos[s] = mem.pos(s) + mem.length(s);
			});
		}
	}

	// consume remaining chars
	size_t max_pos = 0;
	for (size_t s = 0; s < num_seq; s++) {
		assert(cur_pos[s] <= sequences[s].size());
		if constexpr (output_remaining_gaps) {
			assert(sequences[s].size() >= cur_pos[s]);
			res[s] += sequences[s].substr(cur_pos[s]);
		} else
			res[s].resize(res[s].size() + sequences[s].size() - cur_pos[s], 'N');
		max_pos = std::max(max_pos, res[s].size());
	}
	// make all have the same length
	for (size_t s = 0; s < num_seq; s++) {
		assert(res[s].size() <= max_pos);
		res[s].resize(max_pos, '-');
	}

	#ifndef NO_DEBUG_OUTPUT
	std::cout << id << " finished" << std::endl;
	#endif

	return res;
}

// require that MUMs are in at least "threshold" sequences
std::vector<std::string> malign(
	const std::vector<std::string_view>& sequences,
	const size_t threshold,
	const std::string& id,
	thread_pool_t& thread_pool)
{
	return malign(sequences, std::vector<profile_t> {}, threshold, id, thread_pool);
}
