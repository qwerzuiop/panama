#pragma once

#include <atomic>
#include <iostream>
#include <mutex>
#include <queue>
#include <sstream>
#include <algorithm>
#include <memory>
#include <ranges>

namespace VT100 {
	constexpr char const* LINE_UP = "\033[A";
	constexpr char const* CLEAR_LINE = "\33[2K";
} // namespace VT100

// T has to be an std::ostream instance (e.g. std::cout/std::cerr)
template<typename T>
T& reset_line(T& out)
{
	using namespace VT100;
	return out << CLEAR_LINE << '\r';
}
struct capture_cout_t : public std::ostream {
	std::ostringstream buffer;
	std::deque<char> rest;
	std::streambuf* old_buf = nullptr;
	capture_cout_t() : std::ostream(std::cout.rdbuf()) {
		old_buf = std::cout.rdbuf(buffer.rdbuf());
	}
	bool getline(std::string& line) {
		line = "";
		auto str = buffer.str(); buffer.str("");
		rest.insert(rest.end(), str.begin(), str.end());
		// perhaps a bit slow if the lines are very long
		auto p = std::find(rest.begin(), rest.end(), '\n');
		if (p != rest.end()) {
			line = std::string(rest.begin(), p);
			rest.erase(rest.begin(), std::next(p));
			return true;
		}
		return false;
	}
	~capture_cout_t() {
		std::cout.rdbuf(old_buf);
		for (std::string line; getline(line); )
			std::cout << line << std::endl;
	}
};
struct tui_t {
	struct handle_t {
		std::string m_content;
		std::mutex mutex;
		bool ignore = false;
		void refill(std::string&& new_content)
		{
			if (ignore)
				return;
			std::lock_guard<std::mutex> lock(mutex);
			m_content = std::move(new_content);
		}
	};
	struct scoped_handle_t {
		tui_t* tui_ptr = nullptr;
		handle_t* h_ptr = nullptr;
		void refill(std::string&& new_content)
		{
			h_ptr->refill(std::move(new_content));
		}
		scoped_handle_t() = default;
		scoped_handle_t(tui_t& tui)
			: tui_ptr(&tui)
			, h_ptr(tui.add_handle())
		{}
		scoped_handle_t(const scoped_handle_t&) = delete;
		scoped_handle_t(scoped_handle_t&& rhs) {
			*this = std::move(rhs);
		}
		scoped_handle_t& operator=(const scoped_handle_t&) = delete;
		scoped_handle_t& operator=(scoped_handle_t&& rhs) {
			if (this == &rhs)
				return *this;
			if (tui_ptr != nullptr)
				tui_ptr->remove_handle(h_ptr);
			tui_ptr = nullptr, h_ptr = nullptr;
			std::swap(rhs.h_ptr, h_ptr);
			std::swap(rhs.tui_ptr, tui_ptr);
			return *this;
		}
		~scoped_handle_t()
		{
			if (tui_ptr != nullptr) {
				tui_ptr->remove_handle(h_ptr);
				tui_ptr = nullptr;
			}
		}
	};
	std::mutex m_handles_mutex;
	std::vector<std::unique_ptr<handle_t>> m_handles;
	size_t lines_written = 0;
	bool m_ignore = false;
	scoped_handle_t handle()
	{
		return scoped_handle_t(*this);
	}
	handle_t* add_handle()
	{
		auto tmp = std::make_unique<handle_t>();
		tmp->ignore = m_ignore;
		auto res = tmp.get();
		std::lock_guard<std::mutex> lock(m_handles_mutex);
		m_handles.emplace_back(std::move(tmp));
		return res;
	}
	void remove_handle(handle_t* h)
	{
		std::lock_guard<std::mutex> lock(m_handles_mutex);
		m_handles.erase(
			std::remove_if(m_handles.begin(), m_handles.end(), [&](const auto& h_ptr) {
				return h_ptr.get() == h;
			}),
			m_handles.end());
	}
	void ignore()
	{
		m_ignore = true;
	}
	void no_ignore()
	{
		m_ignore = false;
	}
	void update_tui(std::ostream& out)
	{
		using namespace VT100;
		std::lock_guard<std::mutex> lock(m_handles_mutex);
		for (; lines_written > 0; lines_written--)
			out << '\r' << CLEAR_LINE << LINE_UP;
		out << CLEAR_LINE;

		for (const auto& h_ptr : m_handles | std::views::reverse) {
			if (h_ptr->ignore)
				continue;
			std::lock_guard<std::mutex> h_lock(h_ptr->mutex);
			out << h_ptr->m_content << '\n';
			lines_written += std::count(h_ptr->m_content.begin(), h_ptr->m_content.end(), '\n') + 1;
		}
		out << std::flush;
	}
	void update_tui(capture_cout_t& out)
	{
		using namespace VT100;
		std::lock_guard<std::mutex> lock(m_handles_mutex);
		for (; lines_written > 0; lines_written--)
			out << '\r' << CLEAR_LINE << LINE_UP;
		out << CLEAR_LINE;
		for (std::string line; out.getline(line); )
			out << "[cout] " << line << '\n';

		std::vector<std::string> content;
		for (size_t num_lines = 0, i = 0; num_lines < 20 and i < m_handles.size(); i++) {
			if (m_handles[i]->ignore)
				continue;
			std::lock_guard<std::mutex> h_lock(m_handles[i]->mutex);
			auto tmp = m_handles[i]->m_content;
			num_lines += std::count(tmp.begin(), tmp.end(), '\n') + 1;
			content.emplace_back(std::move(tmp));
		}

		for (const auto& c : content | std::views::reverse) {
			out << c << '\n';
			lines_written += std::count(c.begin(), c.end(), '\n') + 1;
		}
		out << std::flush;
	}
} tui;