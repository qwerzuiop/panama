#include <iostream>
#include <limits>

#include "common.hpp"
#include "chaining.hpp"
#include "system_timer.cpp"

using namespace std;

size_t TOTAL_BASE_COUNT;

//FIND BEST CHAIN OF MEMS
chain_t best_chain( chain_t&& mems, const vector<uint64_t> & parse_pfx, chain_t* discarded = nullptr){
	constexpr size_t oo = numeric_limits<size_t>::max();
	vector<size_t> pred( mems.size(), oo );
	size_t best_val = 0, best_idx = oo;


	auto score = [&](const MEM_t& mem) -> uint64_t { // length of MEM in bases
		const auto i = mem.coords[0]; // just choose one of the coords
		return parse_pfx[ i + mem.len ] - parse_pfx[ i ];
	};

	auto emit = [&](size_t mem, size_t val, size_t pre) { // output
		if (val > best_val){
			best_val = val;
			best_idx = mem;
		}
		pred[mem] = pre;
	};

	chain_mems_ordered<>(mems, score, emit );

	if (best_val == 0) return chain_t(0);

	vector<my_int> chain_indexes;
	for (size_t tmp = best_idx; tmp < oo; tmp = pred[tmp])
		chain_indexes.push_back(tmp);


	chain_t chain;
	chain.reserve(chain_indexes.size());


	for (auto i = chain_indexes.rbegin(); i != chain_indexes.rend(); ++i) {
		chain.push_back( move (mems[*i]) );
		mems[*i].len = 0;
	}

	{
		const size_t num_seq = chain[0].seq_num();
		std::vector<size_t> last_pos(num_seq);
		for (size_t i = 0; i < chain.size(); i++) {
			for (size_t s = 0; s < num_seq; s++)
				assert(last_pos[s] <= chain[i].pos(s));
			for (size_t s = 0; s < num_seq; s++)
				last_pos[s] = chain[i].pos(s) + chain[i].length(s);
		}
	}

	if (discarded != nullptr) {
		for (auto& mem : mems)
			if (mem.len > 0)
				discarded->emplace_back(std::move(mem));
	}

	return chain;
}


void print_chain_data( chain_t & chain , vector<uint64_t> & parse_length_pfx){
	const auto mem_length = [&](const MEM_t& mem) -> uint64_t { // length of MEM in bases
		const auto i = mem.coords[0]; // just choose one of the coords
		return parse_length_pfx[ i + mem.len ] - parse_length_pfx[ i ];
	};
	size_t total_len_parse = 0, total_len_bases = 0;
	for( auto mem : chain){
		total_len_parse += mem.len;
		total_len_bases += mem_length( mem );
	}
	cerr << "chain has\n- "
	<< chain.size() << " MEMS/MUMS\n- "
	<< total_len_parse << " x " << chain[0].coords.size() << " phrase symbols\n- "
	<< total_len_bases * chain[0].coords.size() << " base symbols " << " (" <<  (double)  total_len_bases * chain[0].coords.size()  / TOTAL_BASE_COUNT << ")"
	<< endl;
}


int main(int argc, char** argv){
	cout << "[1] prefix of input"  << endl;
	cout << "[2] file containing mems"  << endl;

	if(argc < 3) return 0;

	string in_pref    = argv[1];
	string mems_fname = argv[2];
	cout << "in_pref =" << in_pref << endl;
	cout << "mems =" << mems_fname << endl;

	cout << "loading parse, mems, dict .."  << flush;
	sys_timer st; st.start();
	auto parse     = read_parses(in_pref);
	auto mems      = load_mems_from_file(mems_fname);
	auto D         = load_dict_from_file(in_pref);
	cout << "\rTime for loading parse, mems, dict " << st.stop_and_get() << endl;
	cout << mems.size() << " mems/mums loaded" << endl;

	cout << "Calculating prefix sum of parse" << endl;
	auto parse_pfx = get_parse_length_pfx(parse, D);
	TOTAL_BASE_COUNT = parse_pfx.back();

	st = sys_timer(); st.start();
	cout << "chaining.."  << flush;
	chain_t discarded;
	mems = best_chain(std::move(mems), parse_pfx, &discarded);
	cout << "\rTime for chaining " << st.stop_and_get() << endl;

	print_chain_data(mems, parse_pfx);

	cout << "store chain to file" << endl;
	store_mems_to_file(mems, in_pref + FEX_CHAIN);

	#if 0
	{
		print_chain_data(discarded, parse_pfx);
		boundaries_t boundaries;
		sdsl::load_from_file(boundaries, in_pref + FEX_parseB);
		size_t num = 0;
		for (const auto& mem : discarded) {
			num++;
			assert(mem.coords.size() == boundaries.size());
			std::cerr << "discarded mem of length " << mem.len << std::endl;
			for (size_t i = 0; i < mem.coords.size(); i++) {
				assert(mem.coords[i] >= boundaries[i]);
				std::cerr << mem.coords[i] - boundaries[i] << ' ';
			}
			for (size_t i = 1; i < mem.coords.size(); i++)
				for (size_t l = 0; l < mem.len; l++)
					assert(parse[mem.coords[i] + l] == parse[mem.coords[0] + l]);
			std::cerr << std::endl <<  "phrase ids: ";
			for (size_t i = 0; i < mem.len; i++)
				std::cerr << ' ' << parse[mem.coords[0] + i];
			std::cerr << std::endl << "phrases:     ";
			for (size_t i = 0; i < mem.len; i++)
				std::cerr << ' ' << D(parse[mem.coords[0] + i]);
			std::cerr << std::endl;
			auto lo = mem.coords;
			auto hi = lo;
			for (auto& x : hi)
				x += mem.len;
			// find conflicting mems in chain
			for (const auto& other : mems) {
				std::vector<size_t> conflict_coords;
				bool l = (other.coords[0] < mem.coords[0]);
				for (size_t i = 0; i < mem.coords.size(); i++)
					if ((l and other.coords[i] + other.len > mem.coords[i]) or (not l and mem.coords[i] + mem.len > other.coords[i]))
						conflict_coords.emplace_back(i);
				if (conflict_coords.empty())
					continue;
				std::cerr << "conflicts with mem of length " << other.len << " @ " << conflict_coords << std::endl;
				std::cerr << "\tmem:   ";
				for (size_t i : conflict_coords)
					std::cerr << ' ' << mem.coords[i] - boundaries[i];
				std::cerr << "\n\tother: ";
				for (size_t i : conflict_coords)
					std::cerr << ' ' << other.coords[i] - boundaries[i];
				std::cerr << std::endl;
				for (size_t i = 0; i < mem.coords.size(); i++) {
					assert(other.coords[i] >= boundaries[i]);
					std::cerr << other.coords[i] - boundaries[i] << ' ';
				}
				std::cerr << std::endl;
				{
					size_t average_position = 0;
					for (size_t i = 0; i < mem.coords.size(); i++) {
						size_t p = parse_pfx[mem.coords[i]] - parse_pfx[boundaries[i]];
						average_position += p;
					}
					std::cerr << "average posision in bases: " << (average_position + mem.coords.size() / 2) / mem.coords.size() << std::endl;
					std::cerr << "position in sequence 0:    " << parse_pfx[mem.coords[0]] - parse_pfx[boundaries[0]] << std::endl;
				}
				for (size_t i = 0; i < other.len; i++)
					std::cerr << ' ' << parse[other.coords[0] + i];
				std::cerr << std::endl;
				for (size_t i = 0; i < lo.size(); i++) {
					lo[i] = min(lo[i], other.coords[i]);
					hi[i] = max(hi[i], other.coords[i] + other.len);
				}

				std::cerr << std::endl <<  "phrase ids: ";
				for (size_t i = 0; i < other.len; i++)
					std::cerr << ' ' << parse[other.coords[0] + i];
				std::cerr << std::endl << "phrases:     ";
				for (size_t i = 0; i < other.len; i++)
					std::cerr << ' ' << D(parse[other.coords[0] + i]);
				std::cerr << std::endl;
			}

			for (auto& x : lo)
				x--;
			for (auto& x : hi)
				x++;

			std::ofstream out(in_pref + ".discarded." + std::to_string(num - 1) + ".fa");
			for (size_t i = 0; i < lo.size(); i++) {
				out << ">" << i << '\n';
				for (size_t p = lo[i]; p < hi[i]; p++)
					out << D(parse[p]) << ' ';
				out << '\n';
			}
			out.close();
		}
	}
	#endif
	return 0;
}
