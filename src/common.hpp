#pragma once

#include <algorithm>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>
#include <fstream>
#include <memory>
#include <iostream>

#include <sdsl/suffix_trees.hpp>

#include "file_util.hpp"

template<typename... Ts>
struct overload : Ts... {
	using Ts::operator()...;
};
template<typename... Ts>
overload(Ts...) -> overload<Ts...>;

// FILE EXTENSIONS
const std::string FEX_parse  = ".parse";
const std::string FEX_parseB = ".parse.bound";
const std::string FEX_dict   = ".dict";
const std::string FEX_dictx  = ".dict.idx";
const std::string FEX_ST     = ".parse.ST";
const std::string FEX_MUMS   = ".mums";
const std::string FEX_CHAIN  = ".chain";
const std::string FEX_BLOCK  = ".blocks";

//TYPES
using my_int        = uint64_t;
using index_t       = uint64_t;

using boundaries_t  = sdsl::int_vector<64>;
using parse_t       = std::vector<my_int>;
struct DICT_t;

using coord_t		= std::pair<my_int,my_int>; // (parse index, offset in word)

using suffix_tree_t = sdsl::cst_sada< sdsl::csa_sada_int<sdsl::enc_vector<>, 4, 4>, sdsl::lcp_support_sada<>, sdsl::bp_support_sada<128, 16> >;

struct MEM_t;
using chain_t = std::vector<MEM_t>;


//STRUCTS

struct MEM_t {
	static constexpr my_int NOT_PRESENT = std::numeric_limits<my_int>::max();
	my_int len;
	std::vector<my_int> coords;
	MEM_t(my_int len, const std::vector<my_int>& c) : len(len), coords(c) {}
	MEM_t(my_int len, std::vector<my_int>&& c) : len(len), coords(std::move(c)) {}
	MEM_t() = default;
	MEM_t(const MEM_t&) = default;
	MEM_t(MEM_t&& rhs)
	{
		*this = std::move(rhs);
	}
	MEM_t& operator=(MEM_t&& rhs)
	{
		if (&rhs == this)
			return *this;
		std::swap(rhs.len, len);
		std::swap(rhs.coords, coords);
		return *this;
	}
	MEM_t& operator=(const MEM_t&) = default;
	bool present(size_t s) const
	{
		return coords[s] != NOT_PRESENT;
	}
	template <typename F>
	void for_each_present_coord(F&& f) const
	{
		for (size_t s = 0; s < coords.size(); s++)
			if (present(s))
				f(s);
	}
	size_t length(size_t) const
	{
		return len;
	}
	bool operator==(const MEM_t& rhs) const
	{
		return len == rhs.len and coords == rhs.coords;
	}
	size_t seq_num() const
	{
		return coords.size();
	}
	const my_int& pos(size_t i) const
	{
		return coords[i];
	}
	my_int& pos(size_t i)
	{
		return const_cast<my_int&>(const_cast<const MEM_t*>(this)->pos(i));
	}
	void invalidate()
	{
		coords.clear();
	}
	bool valid() const
	{
		return coords.empty();
	}
};

struct BLOCK_T{
	char type; // M = MEM,
	my_int length; //IN BASES
	std::vector< coord_t > start_off;

	BLOCK_T(){};

	BLOCK_T(const MEM_t & mem, my_int base_lenght):length(base_lenght){
		type = 'M';
		start_off.clear();
		for(auto idx : mem.coords)
			start_off.emplace_back(idx,0);
	};

	BLOCK_T(std::ifstream & ifs){
		type = read_binary<char>(ifs);
		size_t sz = read_binary<my_int>(ifs);
		if (not ifs) return;
		start_off.resize(sz);
		length = read_binary<my_int>(ifs);
		for (auto&[start,off] : start_off) {
			start = read_binary<my_int>(ifs);
			off = read_binary<my_int>(ifs);
		}
	}

	/** ith coordinate*/
	coord_t operator[](size_t i) const {
		return start_off[i];
	}

	/** number of coordinates*/
	size_t count(){
		return start_off.size();
	}
};

std::ostream& operator<<(std::ostream& out, const BLOCK_T & b){
	write_binary<char>(out, b.type);
	write_binary<my_int>(out, b.start_off.size());
	write_binary<my_int>(out, b.length);
	for(const auto&[start, off] : b.start_off) {
		write_binary<my_int>(out, start);
		write_binary<my_int>(out, off);
	}
	return out;
}
std::ostream& operator<<(std::ostream& out, const MEM_t & m){
	out << m.len << " @";
	for(auto i : m.coords)
		out << ' ' << i;
	return out;
}

// describes a substring of one sequence
// seq is the substring
// gaps denotes the positions of gaps in this substring,
// e.g. when seq = "abcd" and gaps = {2}, the substring consists
// of two sequences (contigs) "ab" and "cd" with a gap in between
struct substr_t {
	std::string seq;
	std::vector<size_t> gaps;
};

struct DICT_t{
	// concatenated dictionary
	std::string D;

	// maps symbol to start in dictionary
	sdsl::int_vector<64> D_IDX;

	// sentinels have length 0 and are thus lex. smallest. Therefore, to find out whether a symbol is
	// a sentinel, it suffices to check whether the symbol is smaller than num_sentinels
	size_t num_sentinels = 0;
	bool is_sentinel(size_t symb) const {
		return symb < num_sentinels;
	}

	/** @brief base lenght of the parse symbol*/
	size_t get_length(size_t symb) const{
		if (symb >= D_IDX.size())
			throw std::out_of_range(string_builder{} << "oor in DICT_T::get_length(" << symb << ") D_IDX.size() = " << D_IDX.size());
		// TODO: we can just append D.size() to D_IDX and avoid this branch here
		return   (  (symb+1 < D_IDX.size()) ? D_IDX[symb+1] : D.size() )  - D_IDX[symb];
	}

	/** @brief base string of parse symbol*/
	std::string_view operator()(size_t i) const {
		if (i >= D_IDX.size())
			throw std::out_of_range(string_builder{} << "oor in DICT_T::(" << i << ") D_IDX.size() = " << D_IDX.size());
		return std::string_view{D}.substr( D_IDX[i], get_length(i) );
	}

	/** @brief parse a parse to a sequence*/
	// NOTE: ignores separators
	std::string parse_to_seq( const parse_t & parse) const {
		std::string seq = "";
		for (auto c : parse )
			seq += (*this)(c);
		return seq;
	}

	/** @brief base string of parse symbol*/
	char operator()(my_int i, my_int j) const {
		if(j >= get_length(i) )
			throw std::out_of_range(string_builder{} << "oor in DICT_T::(" << i << ", " << j << ") get_length(i) = " << get_length(i));
		return D[ D_IDX[i] + j];
	}

	std::vector<std::string> parse_to_seqs(const parse_t& parse) const {
		std::vector<std::string> res;
		std::string cur;
		for (const auto& c : parse) {
			if (get_length(c) == 0) {
				if (not cur.empty())
					res.emplace_back(std::move(cur));
				cur = "";
			} else {
				cur += (*this)(c);
			}
		}
		if (not cur.empty())
			res.emplace_back(std::move(cur));
		return res;
	}
};

DICT_t load_dict_from_file(const std::string file_pref){
	DICT_t D;
	checked_ifstream_t dict_in(file_pref + FEX_dict, std::ios::in);
	dict_in >> D.D;
	dict_in.check_and_close();
	sdsl::load_vector_from_file(D.D_IDX, file_pref+FEX_dictx, 8);
	for (D.num_sentinels = 0;
		D.num_sentinels + 1 < D.D_IDX.size() and D.D_IDX[D.num_sentinels + 1] == 0;
		D.num_sentinels++)
	{}
	#ifndef NDEBUG
	for (size_t i = D.num_sentinels; i < D.D_IDX.size(); i++)
		assert(D.get_length(i) > 0);
	#endif
	return D;
}


//FUNCTIONS
template<typename T>
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) //DEBUG
{
	out << "<" << v.size() << ": ";
	for (size_t i = 0; i < v.size(); i++)
	{
		if (i > 0) out << ", ";
		out << v[i];
	}
	return out << " >";
}

template<typename T>
void print_plain_vector(std::ostream& out, const std::vector<T>& v) //DEBUG
{
	for (size_t i = 0; i < v.size(); i++)	{
		if (i) out << " ";
		out << v[i];
	}
}

template<typename T,typename V>
std::ostream& operator<<(std::ostream& out, const std::pair<T,V>& v)
{
	return out << '<' << v.first << ", " << v.second << '>';
}

void store_mems_to_file(const chain_t & mems, const std::string fname){
	std::ofstream mem_out(fname, std::ios::out);
	mem_out << mems.size() << ' ' << mems[0].coords.size() << '\n';
	for (const auto& mem : mems)
	{
		mem_out << mem.len;
		for (const auto& x : mem.coords) mem_out << ' ' << x;
		mem_out << '\n';
	}
	mem_out.close();
}

std::vector<MEM_t> load_mems_from_file(const std::string fname){
	std::vector<MEM_t> mems;
	checked_ifstream_t in(fname, std::ios::in);
	my_int num_mems, dim;
	in >> num_mems >> dim;
	mems.resize(num_mems);
	for (my_int i = 0; i < num_mems; i++)
	{
		in >> mems[i].len;
		mems[i].coords.resize(dim);
		for (auto& x : mems[i].coords) in >> x;
	}
	in.check_and_close();
	return mems;
}

parse_t read_parses(const std::string& file_prefix){
	parse_t parse;

	const std::string filename = file_prefix + FEX_parse;
	checked_ifstream_t parse_in(filename, std::ios::in | std::ios::binary);

	for (uint32_t tmp; parse_in.read(reinterpret_cast<char*>(&tmp), sizeof(tmp)) ; ){
		assert( tmp > 0 );
		parse.emplace_back(tmp - 1);
	}
	parse_in.check_and_close();
	return parse;
}

// NOTE: this has size parse.size() + 1, the last element is the length of the entire sequence
std::vector<uint64_t> get_parse_length_pfx(const parse_t & parse, const DICT_t & dict) {
	std::vector<uint64_t> parse_length_pfx;
	parse_length_pfx.reserve(parse.size() + 1);
	uint64_t sum = 0;
	for(auto p : parse){
		parse_length_pfx.push_back(sum);
		sum += dict.get_length(p);
	}
	parse_length_pfx.push_back(sum);
	return parse_length_pfx;
}



struct my_index_t{
	parse_t P;
	DICT_t D;
	std::vector<uint64_t> P_prefsum;
	boundaries_t boundaries;

	/*my_index_t(parse_t P, DICT_t D):P(P),D(D){
	 * P_prefsum = get_parse_length_pfx(P, D);
}*/
	my_index_t(){};

	my_index_t(std::string file_pref){
		P = read_parses(file_pref);
		D = load_dict_from_file(file_pref);
		P_prefsum = get_parse_length_pfx(P, D);
		sdsl::load_from_file(boundaries, file_pref+ FEX_parseB);
	}

	bool sentinel_at(size_t p_idx) const {
		return D.is_sentinel(P[p_idx]);
	}
	bool sentinel_at(const coord_t& coord) const {
		if (coord.second > 0) assert(not sentinel_at(coord.first));
		return coord.second == 0 and sentinel_at(coord.first);
	}

	char get_char(size_t p_idx, size_t off) const{
		return D( P[p_idx] , off );
	}

	char get_char(coord_t coord) const{
		return get_char(coord.first,coord.second);
	}
	// returns false if not possible
	// NOTE: if the return value is false, the coord is left in an undefined state
	template<bool ignore_gaps = false>
	bool shift_coord(coord_t & c, int64_t off) const{

		if(off < 0){
			off += c.second;
			c.second = 0;
		}

		while(off<0){
			if(c.first == 0) throw std::runtime_error("shift_coord, OFFSET TOO SMALL");
			const uint64_t prev_l = P_prefsum[c.first] - P_prefsum[c.first-1];
			if (not ignore_gaps and prev_l == 0) // sentinel
				return false;
			c.first --;
			off += prev_l;
		}
		// OFF >= 0
		c.second += off;
		return correct_coordinate<ignore_gaps>(c);
	}

	/** @brief if off not in [0 ,  P[start].lenght) adjust start and off.    */
	// returns false if not possible
	// NOTE: if the return value is false, the coord is left in an undefined state
	template<bool ignore_gaps = false>
	bool correct_coordinate(my_int & start, my_int & off) const{
		assert(ignore_gaps or not sentinel_at(start));
		if( start >= P.size() ) throw std::runtime_error("False coordinates in correct_coordinate (C)");
		if(off == 0) return true;
		assert(P_prefsum.size() == P.size() + 1);
		uint64_t l = P_prefsum[start + 1] - P_prefsum[start]; // = | P[start] |
		if (not ignore_gaps and l == 0) return false;
		while( off >= l){ // TODO: this can be slow when off is very large
			off -= l;
			start++;
			l = P_prefsum[start + 1] - P_prefsum[start];
			if (not ignore_gaps and l == 0) return false;
			if (off == 0)
				break; // account for coordinate immediately following the string  (this is required for exclusive coords)
			if(start >= P.size()) throw std::runtime_error("False coordinates in correct_coordinate (A)");
		}
		assert(ignore_gaps or not sentinel_at(start));
		return true;
	}
	// returns false if not possible
	// NOTE: if the return value is false, the coord is left in an undefined state
	template<bool ignore_gaps = false>
	bool correct_coordinate(coord_t & start_off) const{
		return correct_coordinate<ignore_gaps>(start_off.first, start_off.second);
	}

	size_t get_base_idx( size_t parse_idx, int offset = 0) const{
		if(parse_idx >= P_prefsum.size()) throw std::runtime_error("get_base_idx(): parse_idx >= P_prefsum.size()");
		return P_prefsum[parse_idx] + offset;
	}
	size_t get_base_idx(coord_t c) const{
		return get_base_idx(c.first,c.second);
	}

	size_t dist(coord_t c1, coord_t c2){
		return get_base_idx(c2) - get_base_idx(c1);
	}

	substr_t substr_with_gaps(coord_t c, size_t l) const {
		assert(c.first < P.size());
		assert(D(P[c.first]).size() >= c.second);
		substr_t out;
		out.seq = (std::string) D( P[c.first++]).substr(c.second);
		while (out.seq.size() < l){
			assert(c.first < P.size());
			if (sentinel_at(c.first)) { [[unlikely]]
				assert(D(P[c.first]).empty());
				out.gaps.emplace_back(out.seq.size());
			} else {
				assert(not D(P[c.first]).empty());
				out.seq += D(P[c.first]);
			}
			c.first++;
		}
		out.seq.resize(l);
		assert(out.gaps.empty() or out.gaps[0] > 0);
		return out;
	}
	/** c1 inclusive, c2 exclusive*/
	substr_t substr_with_gaps(coord_t c1, coord_t c2) const{
		size_t i1 = get_base_idx(c1);
		size_t i2 = get_base_idx(c2);
		assert(i1 <= i2);
		// if(i1 > i2) return "";
		auto res = substr_with_gaps(c1, i2-i1);
		assert(res.seq.size() == i2 - i1);
		return res;
	}

	// NOTE: This method assumes there is no gap in the substring. If this
	// is not guaranteed, use substr_with_gaps
	std::string substr(coord_t c, size_t l) const{
		assert(c.first < P.size());
		assert(D(P[c.first]).size() >= c.second);
		assert(not sentinel_at(c.first));
		std::string out = (std::string) D( P[c.first++]).substr(c.second);
		while (out.size() < l){
			assert(c.first < P.size());
			if (sentinel_at(c.first))
				throw std::logic_error(string_builder{} << "substr includes sentinel @ " << c.first);
			assert(not sentinel_at(c.first));
			out += D(P[c.first++]);
		}
		out.resize(l);
		return out;
	}
	template <typename It>
	It substr(coord_t c, size_t l, It out) const {
		assert(c.first < P.size());
		assert(D(P[c.first]).size() >= c.second);
		assert(not sentinel_at(c.first));
		size_t num;
		{
			std::string_view str = D(P[c.first++]).substr(c.second);
			num = std::min(str.size(), l);
			out = std::copy_n(str.begin(), num, out);
		}
		while (num < l){
			assert(c.first < P.size());
			if (sentinel_at(c.first))
				throw std::logic_error(string_builder{} << "substr includes sentinel @ " << c.first);
			assert(not sentinel_at(c.first));
			std::string_view str = D(P[c.first++]);
			const size_t n = std::min(str.size(), l - num);
			out = std::copy_n(str.begin(), n, out);
			num += n;
		}
		return out;
	}
	/** c1 inclusive, c2 exclusive*/
	// NOTE: This method assumes there is no gap in the substring. If this
	// is not guaranteed, use substr_with_gaps
	std::string substr(coord_t c1, coord_t c2) const{
		size_t i1 = get_base_idx(c1);
		size_t i2 = get_base_idx(c2);
		if(i1 > i2) return "";
		return substr(c1, i2-i1);
	}
};

/** @brief returns a dummy block for front*/
BLOCK_T dummy_block_front(const my_index_t& I)
{
	BLOCK_T b;
	b.type = 'F';
	b.length = 0;
	for (auto x : I.boundaries)
		b.start_off.emplace_back(x, 0);
	return b;
}

/** @brief returns a dummy block for back*/
BLOCK_T dummy_block_back(const my_index_t& I)
{
	BLOCK_T b;
	b.type = 'B';
	b.length = 0;
	for (size_t i = 1; i < I.boundaries.size(); i++)
		b.start_off.emplace_back(I.boundaries[i], 0);
	b.start_off.emplace_back(I.P.size(), 0);
	return b;
}

/** @brief returns sequences between two blocks
 * @param b1 front block
 * @param b2 back block
 * @param I index
 *
 * @return vector of sequences between b1 and b2
 */
std::vector<substr_t> gap_strings(const BLOCK_T& b1, const BLOCK_T& b2, const my_index_t& I)
{
	std::vector<substr_t> gs;
	gs.reserve(b1.start_off.size());
	for (size_t i = 0; i < b1.start_off.size(); i++) {
		coord_t c = b1[i];
		I.shift_coord<true>(c, b1.length);
		gs.emplace_back(I.substr_with_gaps(c, b2[i]));
	}
	return gs;
}

enum block_limit_e {
	BACK = 0,
	FRONT = 1,
};

struct limits_t{
	std::vector<size_t> data;

	limits_t (){};

	limits_t (const BLOCK_T & b, const block_limit_e front, const my_index_t & I){
		data = std::vector<size_t>(0);
		for(auto so : b.start_off){
			size_t val = I.get_base_idx(so) + (front == FRONT ? 0 : b.length);
			data.push_back( val );
		}
	}

	limits_t (const block_limit_e front, const my_index_t & I){
		data = std::vector<size_t>(0);
		for(size_t i = front == FRONT ? 0 : 1; i < I.boundaries.size(); i++)
			data.push_back( I.get_base_idx(I.boundaries[i]) );
		if(front == BACK)
			data.push_back (I.get_base_idx( I.P.size() ));
	}

	//MIN  { yi - xi |  x=this y=other   }
	int64_t min_dist(const limits_t & other){
		if(data.size() != other.data.size() || data.size() == 0)throw std::runtime_error("ERROR IN limits_t::min_dist");
		int64_t min_d = other.data[0] - data[0];
		assert(other.data[0] >= data[0]);
		for(size_t i = 1; i < data.size(); i++){
			assert(other.data[i] >= data[i]);
			int64_t d = other.data[i] - data[i];
			if(d < min_d) min_d = d;
		}
		return min_d;
	}

	int64_t max_dist(const limits_t & other){
		if(data.size() != other.data.size() || data.size() == 0)throw std::runtime_error("ERROR IN limits_t::min_dist");
		int64_t max_d = other.data[0] - data[0];
		assert(other.data[0] >= data[0]);
		for(size_t i = 1; i < data.size(); i++){
			assert(other.data[i] >= data[i]);
			int64_t d = other.data[i] - data[i];
			if(d > max_d) max_d = d;
		}
		return max_d;
	}
};

/** @brief corrected sdsl::construct_im
 * this now works with paralisation
 */
template <class t_index, class t_data>
void my_construct_im(t_index& idx, t_data data, uint8_t num_bytes, std::string id = "")
{
	using namespace sdsl;

	//string id =  to_string(omp_get_thread_num()) ;
	//string id =  to_string(omp_get_thread_num()) + "_"+ to_string(ID) ;
	std::string tmp_file = ram_file_name(util::to_string(util::pid()) + "_" + util::to_string(util::id()) + id);
	store_to_file(data, tmp_file);
	tMSS file_map;
	cache_config config;
	config.dir = "@";
	config.id = id;
	construct(idx, tmp_file, config, num_bytes);
	ram_fs::remove(tmp_file);
}

// NOTE: having write_to_sdsl_file as a separate function ensures that
// the container is freed as soon as possible
// if it's passed as an rvalue-reference
template <typename Container_t>
void write_to_sdsl_file(Container_t&& C, const std::string& path)
{
	// Oh god, I hate that I have to do this...
	sdsl::osfstream out(path, std::ios::binary | std::ios::trunc | std::ios::out);
	for (const auto& x : C)
		out.write(reinterpret_cast<const char*>(&x), sizeof(x));
	out.close();
}
template <typename Container_t>
suffix_tree_t construct_ST_im(Container_t&& S, std::string id = "")
{
	const std::string tmp_file = sdsl::ram_file_name(sdsl::util::to_string(sdsl::util::pid()) + "_" + sdsl::util::to_string(sdsl::util::id()) + id);
	write_to_sdsl_file(std::forward<Container_t>(S), tmp_file);
	sdsl::cache_config config;
	config.dir = "@", config.id = id;
	suffix_tree_t ST;
	sdsl::construct(ST, tmp_file, config, sizeof(S[0]));
	sdsl::ram_fs::remove(tmp_file);
	return ST;
}