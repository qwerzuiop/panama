#include <iostream>
#include <omp.h>

#include "common.hpp"
#include "config.hpp"
#include "mums.cpp"

#include "system_timer.cpp"

using namespace std;

int main(int argc, const char** argv){
	cout << "[1] prefix of input"  << endl;
	only_general_config(argc, argv);

	//load suffix tree and boundaries
	suffix_tree_t ST;
	boundaries_t  boundaries;
	cout << "loading ST and boundaries.."  << flush;
	{
		sys_timer st; st.start();
		sdsl::load_from_file(ST,         config.in_pref + FEX_ST);
		sdsl::load_from_file(boundaries, config.in_pref + FEX_parseB);
		cout << "\rTime for loading ST and boundaries " << st.stop_and_get() << endl;
		std::cout << "boundaries: " << boundaries << std::endl;
		std::cout << "ST.size(): "  << ST.size()  << std::endl;
	}

	//calculate mums
	chain_t mums;
	sys_timer st; st.start();
	mum::mums_par(boundaries,ST,
		    [&](const auto& len, const auto& coords) {
			    mums.emplace_back(len, coords);
			    std::cerr << '\r' << mums.size() << std::flush;
		    }
	);
	std::cout << "\rfound " << mums.size() << " MUMs" << std::endl;
	std::cout << "Time for finding MUMs: " << st.stop_and_get() << std::endl;

	// write MEMs to file
	{
		sys_timer st; st.start();
		store_mems_to_file(mums, config.in_pref + FEX_MUMS);
		std::cout << "Time for writing MEMs: " << st.stop_and_get() << std::endl;
	}
}
