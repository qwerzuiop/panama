#pragma once

#include <fstream>
#include <filesystem>

// exactly as ifstream, but throws exceptions if
// - file could not be opened, or
// - something goes wrong while reading (such as trying to read from an empty stream) (this is only checked upon destruction)
struct checked_ifstream_t : public std::ifstream {
	std::filesystem::path m_path;
	template<typename... Args>
	checked_ifstream_t(const std::filesystem::path& path, Args&&... args)
		: std::ifstream(path, std::forward<Args>(args)...)
		, m_path(path)
	{
		if (not is_open() or bad())
			throw std::runtime_error("Could not open \"" + m_path.string() + "\"");
	}
	void check() const {
		if (bad())
			throw std::runtime_error("Something went wrong reading \"" + m_path.string() + "\"");
	}
	void check_and_close() {
		check();
		close();
	}
	virtual ~checked_ifstream_t() = default;
};

template <typename... Args>
auto with_checked_ifstream(const std::filesystem::path& path, Args&&... args, auto&& f) {
	checked_ifstream_t in(path, std::forward<Args>(args)...);
	if constexpr (std::is_same_v<void, std::invoke_result_t<decltype(f), checked_ifstream_t&>>) {
		f(in);
		in.check_and_close();
	} else {
		auto res = f(in);
		in.check_and_close();
		return res;
	}
}

// a bit faster than std::getline() on a std::stringstream
template<typename container_t = std::string>
struct line_splitter_t {
	container_t m_value;
	size_t pos = 0;
	size_t len = 0;
	line_splitter_t() = default;
	line_splitter_t(container_t&& v) : m_value(std::move(v)), len(m_value.size()) {}
	line_splitter_t(container_t&& v, size_t n) : m_value(std::move(v)), len(n) {}

	operator bool() const {
		return pos < len;
	}

	std::string_view getline() {
		if (not *this)
			return std::string_view{};
		size_t next = pos;
		while (next < len and m_value[next] != '\n')
			next++;
		auto res = std::string_view{m_value.data(), len}.substr(pos, next - pos);
		pos = next + 1;
		return res;
	}
};

template <typename F, typename G>
void read_fasta_(G&& g, F&& f)
{
	std::string line;
	bool running = g(line);

	for (; running; ) {
		if (line.empty() or line[0] != '>')
			throw std::runtime_error("expected header at line start, found \"" + line + "\"");
		std::string header = std::move(line);
		std::string content;
		while ((running = g(line)) and (line.empty() or line[0] != '>'))
			content += line;
		f(std::move(header), std::move(content));
	}
}
template <typename container_t, typename F>
void read_fasta(line_splitter_t<container_t>& in, F&& f)
{
	read_fasta_<F>([&](std::string& line) -> bool {
		if (not in)
			return false;
		line = in.getline();
		return true;
	}, std::forward<F>(f));
}
template <typename F>
void read_fasta(std::istream& in, F&& f)
{
	read_fasta_<F>([&](std::string& line) -> bool {
		return !!std::getline(in, line);
	}, std::forward<F>(f));
}

std::string read_file(const std::string& path)
{
	return with_checked_ifstream(path, [&] (std::istream& in) -> std::string {
		return std::string((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	});
}

template<typename T>
std::ostream& write_binary(std::ostream& out, const T val)
{
	out.write(reinterpret_cast<const char*>(&val), sizeof(T));
	return out;
}

template<typename T>
T read_binary(std::istream& in)
{
	T val;
	in.read(reinterpret_cast<char*>(&val), sizeof(T));
	return val;
}

struct string_builder {
	std::ostringstream out;
	operator std::string() && {
		return std::move(out).str();
	}
	string_builder() = default;
	string_builder(const string_builder&) = delete;
	string_builder(string_builder&&) = default;
	string_builder& operator=(const string_builder&) = delete;
	string_builder& operator=(string_builder&&) = default;
};
template <typename T>
string_builder operator<<(string_builder&& sb, T&& t) 
{
	sb.out << t;
	return sb;
}
