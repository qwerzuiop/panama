#pragma once

#include <chrono>
#include <memory>
#include <mutex>
#include <thread>

class periodic_control_t {
	std::shared_ptr<std::timed_mutex> m_ctrl;
	std::unique_lock<std::timed_mutex> m_lock;
	std::thread m_thread;

public:
	periodic_control_t()
	{
		m_ctrl = std::make_shared<std::timed_mutex>();
		m_lock = std::unique_lock<std::timed_mutex>(*m_ctrl);
	}
	periodic_control_t(const periodic_control_t&) = delete;
	periodic_control_t(periodic_control_t&&) = default;
	periodic_control_t& operator=(const periodic_control_t&) = delete;
	periodic_control_t& operator=(periodic_control_t&&) = default;

	void stop()
	{
		if (not m_lock.owns_lock())
			return;
		m_lock.unlock();
		if (m_thread.joinable())
			m_thread.join();
	}
	~periodic_control_t()
	{
		stop();
	}

	template <typename F1, typename F2, typename T>
	friend std::enable_if_t<std::is_invocable_v<F1> and std::is_invocable_v<F2>, periodic_control_t> repeat_with_time(F1&& f1, F2&& f2, const T& t);
	template <typename F1, typename T>
	friend std::enable_if_t<std::is_invocable_v<F1>, periodic_control_t> repeat_with_time(F1&& f1, const T& t);
};

template <typename F1, typename F2, typename T>
[[nodiscard]] std::enable_if_t<std::is_invocable_v<F1> and std::is_invocable_v<F2>, periodic_control_t>
repeat_with_time(F1&& f1, F2&& f2, const T& t)
{
	periodic_control_t res;
	res.m_thread = std::thread([mtx = res.m_ctrl, f1 = std::forward<F1>(f1), f2 = std::forward<F2>(f2), t = t]() mutable {
		std::unique_lock<std::timed_mutex> lock { *mtx, std::defer_lock };
		while (not lock.try_lock_for(t)) {
			f1();
		}
		f2();
	});
	return res;
}
template <typename F, typename T>
[[nodiscard]] std::enable_if_t<std::is_invocable_v<F>, periodic_control_t>
repeat_with_time(F&& f, const T& t)
{
	periodic_control_t res;
	res.m_thread = std::thread([mtx = res.m_ctrl, f = std::forward<F>(f), t = t]() mutable {
		std::unique_lock<std::timed_mutex> lock { *mtx, std::defer_lock };
		while (not lock.try_lock_for(t)) {
			f();
		}
		f();
	});
	return res;
}
template <typename F1, typename F2>
[[nodiscard]] std::enable_if_t<std::is_invocable_v<F1> and std::is_invocable_v<F2>, periodic_control_t>
repeat(F1&& f1, F2&& f2)
{
	return repeat_with_time<F1, F2>(std::forward<F1>(f1), std::forward<F2>(f2), std::chrono::milliseconds(100));
}
template <typename F1>
[[nodiscard]] std::enable_if_t<std::is_invocable_v<F1>, periodic_control_t>
repeat(F1&& f1)
{
	return repeat_with_time<F1>(std::forward<F1>(f1), std::chrono::milliseconds(100));
}
