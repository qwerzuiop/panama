#include <bits/stdint-uintn.h>
#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <stack>
#include <limits>
#include <deque>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <algorithm> //sort
#include <cstdio> //remove

#include <ext/pb_ds/assoc_container.hpp> // better hashmap

#include "system_timer.cpp"
#include <sdsl/int_vector_buffer.hpp>

#include "common.hpp"

using namespace std;


// -----------------------------------------------------------------
// class to maintain a window in a string and its KR fingerprint
// FROM Big-BWT/newscan.cpp
struct KR_window {
	size_t wsize;
	std::unique_ptr<uint8_t[]> window;
	static constexpr uint64_t asize = 256;
	static constexpr uint64_t prime = 1999999973;
	uint64_t hash;
	uint64_t tot_char;
	uint64_t asize_pot;   // asize^(wsize-1) mod prime

	KR_window(size_t w): wsize(w) {
		asize_pot = 1;
		for(size_t i=1;i<wsize;i++)
			asize_pot = (asize_pot*asize)% prime; // ugly linear-time power algorithm
		// alloc and clear window
		window = std::make_unique<uint8_t[]>(wsize);
		reset();
	}

	// init window, hash, and tot_char
	void reset() {
		for(size_t i = 0; i < wsize; i++) window[i]=0;
		// init hash value and related values
		hash=tot_char=0;
	}

	uint64_t addchar(uint8_t c) {
		size_t k = tot_char++ % wsize;
		// complex expression to avoid negative numbers
		hash += (prime - (window[k]*asize_pot) % prime); // remove window[k] contribution
		hash = (asize*hash + c) % prime;      //  add char i
		window[k]=c;
		// cerr << get_window() << " ~~ " << window << " --> " << hash << endl;
		return hash;
	}
	/*/ debug only
	string get_window() {
		string w = "";
		int k = (tot_char-1) % wsize;
		for(int i=k+1;i<k+1+wsize;i++)
			w.append(1,window[i%wsize]);
		return w;
	}*/

	~KR_window() = default;
};
// -----------------------------------------------------------
// values of the wordeFreq map: word, its number of occurrences, and its rank
struct word_stats {
	string str;
	uint32_t occ;
	uint32_t rank;
	bool separator;
	word_stats([[maybe_unused]] uint64_t symbol) : str(""), occ(1), rank(0), separator(true) {}
	word_stats(const string& str):str(str){occ = 1; rank = 0;};
	word_stats(){occ = 0; rank = 0; str = "";};
};


// compute 64-bit KR hash of a string
// to avoid overflows in 64 bit aritmethic the prime is taken < 2**55
uint64_t kr_hash(const string & s) {
	uint64_t hash = 0;
	//const uint64_t prime = 3355443229;     // next prime(2**31+2**30+2**27)
	constexpr uint64_t prime = 27162335252586509; // next prime (2**54 + 2**53 + 2**47 + 2**13)
	for(size_t k=0;k<s.size();k++) {
		const uint64_t c = (uint8_t) s[k];
		assert(c < 256);
		hash = (256*hash + c) % prime;    //  add char k
	}
	return hash;
}



using file_stat      = tuple<string, uint64_t,uint64_t>; // FILENAME / LENGTH (GAPHICAL CHARACTERS) /  START IN PARSE
// using dict_builder_t = unordered_map<uint64_t, word_stats>;
using dict_builder_t = __gnu_pbds::cc_hash_table<uint64_t, word_stats>;

void add_word_to_dict_and_parse(dict_builder_t& D, ofstream & parse, const string & word){
	const uint64_t hash = kr_hash(word);
	parse.write(reinterpret_cast<const char*>(&hash), sizeof(hash));

	//const auto[it, success] = D.insert(hash, word);
	auto it = D.find(hash);
	const bool success = it == D.end();
	if (success)
		it = D.insert(std::make_pair(hash, word_stats{word})).first;
		
	if (not success) {
		it->second.occ++;
		if(it->second.occ <=0) {
			cerr << "Emergency exit! Maximum # of occurence of dictionary word exceeded\n";
			exit(1);
		}
		if(it->second.str != word) {
			cerr << "Emergency exit! Hash collision for strings:\n";
			cerr << D[hash].str << "\n  vs\n" <<  word << endl;
			exit(1);
		}
	}
}

// returns the number of graphical characters read
template <typename get_char_t>
uint64_t add_sequence(get_char_t&& in, const size_t w, const uint64_t p, dict_builder_t& D, std::ofstream& parse)
{
	std::string word;
	KR_window krw(w);

	const auto add_char = [&](char c) -> void {
		word.append(1,c);
		uint64_t hash = krw.addchar(c);
		if ((hash % p) == 0 and word.size() >= w) {
			add_word_to_dict_and_parse(D, parse, word);
			word.clear();
		}
		// krw.reset();
	};
	const auto add_separator = [&] {
		// add unique separator
		constexpr uint64_t MSB = uint64_t { 1 } << 63;
		const uint64_t separator = MSB | parse.tellp();
		parse.write(reinterpret_cast<const char*>(&separator), sizeof(separator));
		const auto [it, success] = D.insert(
			std::make_pair(separator, word_stats { separator }));
		if (not success)
			throw std::logic_error(
				"separator already used: "
				+ std::to_string(separator) + " : "
				+ std::to_string(separator & ~MSB));
	};

	uint64_t graphical = 0;
	
	for (char c; (c = in.peek()) != '>' and in.get(c);)
	{
		if (not std::isgraph(c))
			continue;
		graphical++;
		if (c >= 'a' && c <= 'z')
			c = c - 'a' + 'A';
		// c = toupper(c);
		add_char(c);
	}
	if (not word.empty()) {
		assert(graphical > 0);
		add_word_to_dict_and_parse(D, parse, word);
	}
	if (graphical > 0)
		add_separator();

	return graphical;
}

// .first is the sequence name from the FASTA file
// .second is the number of graphical chars read (0 iff error)
std::tuple<std::string, uint64_t> parse_sequence(std::istream& in, const size_t w, const uint64_t p, dict_builder_t& D, std::ofstream& parse)
{
	std::string seq_name;
	{ // get sequence name
		char c;
		while (in.get(c) and c != '>') {}
		if (c != '>') // could not find '>'
			return std::make_pair(std::move(seq_name), 0);
		std::getline(in, seq_name);
	}

	uint64_t graphical = add_sequence(in, w, p, D, parse);

	return std::make_pair(std::move(seq_name), graphical);
}


/**
 * @brief parses a file, in the parse a phrase is represented by its hash value.
 */
file_stat parse_file( string fname, size_t w, uint64_t p, dict_builder_t& D, ofstream & parse ){
	checked_ifstream_t file_in(fname);
	const uint64_t start = ((uint64_t)parse.tellp()) / sizeof(uint64_t);

	uint64_t graphical = 0;

	while (true) {
		auto[name, num_g] = parse_sequence(file_in, w, p, D, parse);
		if (name.empty())
			break;
		graphical += num_g;
	}

	return make_tuple( fname, graphical, start );
}

/**
 * @brief replace all hash values in the parse by their lex ranks.
 */
void refine_parse(string parse_fname, dict_builder_t& D){
	ifstream file_in(parse_fname, ios::in | ios::binary);
	ofstream file_out(parse_fname + ".tmp", ios::out | ios::binary);
	uint64_t hash;

	// std::cerr << parse_fname << ":" << std::endl;
	
	while(file_in.read(reinterpret_cast<char *>(&hash), sizeof(hash))){
		uint32_t rank = D[hash].rank + 1;
		// std::cerr << "\"" << D[hash].str << "\" : " << D[hash].rank + 1 << " : has 0 : " << (std::find(D[hash].str.begin(), D[hash].str.end(), '\0') != D[hash].str.end()) << std::endl;
		file_out.write(reinterpret_cast<char*>(&rank), sizeof(rank));
	}

	file_in.close();
	file_out.close();
	rename( (parse_fname + ".tmp").data(), parse_fname.data());
}


void analyse_dict(  dict_builder_t& D ){
	cout << "=== Analyse dict: " << endl;
	uint64_t d_length = 0, d_size = 0, d_count = 0, tot_size = 0;

	size_t num_sentinels = 0;

	for(auto& x: D) {
		if (x.second.str.empty())
			num_sentinels++;
		d_length += x.second.str.size();
		d_size++;
		tot_size += x.second.str.size() * x.second.occ;
		d_count += x.second.occ;
	}

	cout << "Number of entries in dict:    " << d_size << endl
	     << "Total length entries in dict: " << d_length << endl
	     << "Total length of parses:       " << d_count << endl
	     << "Number of sentinels:          " << num_sentinels << endl
	     << "Compressed size:              " << (d_length+d_count*4) << " (bytes)" << endl
	     << "Original size:                " << (tot_size) << " (bytes)"<< endl;
}

/**
 * @brief Transform map to
 */
vector<word_stats *>  calc_ranks(dict_builder_t& D_map, bool info_output = true){

	vector<word_stats *> P_arr(0);
	P_arr.reserve(D_map.size());

	if (info_output)
		cout << "=== Copy pointers to array: " << endl;
	for(auto& x: D_map) P_arr.push_back(&x.second);

	if (info_output)
		cout << "=== Sort pointers array: " << endl;
	sort(P_arr.begin(), P_arr.end(),[](const word_stats *a, const word_stats *b) -> bool{ return a->str < b->str;});

	if (info_output)
		cout << "=== Store ranks: " << endl;
	for(uint32_t rank = 0; rank < P_arr.size(); rank++) P_arr[rank]->rank = rank;

	return P_arr;
}

void store_dict(const vector<word_stats *>& P_arr, const string& out_fname, bool info_output = true){
	if (info_output)
		cout << "=== Write dict: " << endl;

	ofstream dict_out    (out_fname + FEX_dict, ios::out);
	ofstream dict_idx_out(out_fname + FEX_dictx, ios::out|ios::binary);

	uint64_t com_sum = 0;

	//write Dict and store ranks
	for (size_t rank = 0; rank < P_arr.size(); rank++){
		dict_out << P_arr[rank]->str;
		dict_idx_out.write(reinterpret_cast<char*>(&com_sum), sizeof(com_sum));
		com_sum += P_arr[rank]->str.size();
	}

	dict_out.close();
	dict_idx_out.close();
}

//############################################################################


/** @brief main method for pfp */

int build_parse(int w, int p, const string out_pref, const std::vector<string> in_files){

	string parse_fname = out_pref + FEX_parse;
	cout << "BUILD PARSE WITH" << endl
		<< "W " << w << endl
		<< "P " << p << endl
		<< "in="  << in_files << endl
	     << "out=" << out_pref << endl;

	dict_builder_t D_map;
	ofstream parse(parse_fname, ios::out | ios::binary);
	vector<file_stat>  stats(0);

	sys_timer st;
	st.start();

	if (in_files.size() == 1) {
		std::cout << "interpreting single FASTA file " << in_files[0] << " as collection of sequences" << std::endl;
		with_checked_ifstream(in_files[0], [&](std::istream& file_in) {
			while (true) {
				const uint64_t start = ((uint64_t)parse.tellp()) / sizeof(uint64_t);

				const auto[name, graphical] = parse_sequence(file_in, w, p, D_map, parse);
				if (name.empty())
					break;

				stats.emplace_back(name, graphical, start);

				std::cout << name << std::endl;
				std::cout << "Length (orig) = " << graphical << std::endl;
				std::cout << "Length (parse) = " << ( (uint64_t) parse.tellp()/8 - std::get<2>(stats.back()) ) << std::endl;
				std::cout << "Time for parsing " << get<0>(stats.back()) << " : " << st.stop_and_get() << endl;st = sys_timer(); st.start();
			}
		});
	} else {
		std::cout << "interpreting each FASTA file as single sequence" << std::endl;
		for(auto i : in_files){
			cout << "=== Parse file: " << i << endl;
			stats.push_back( parse_file(i,w,p,D_map,parse)  );
			cout << "Length (orig) = " << get<1>(stats.back()) << endl;
			cout << "Length (parse) = " << ( (uint64_t) parse.tellp()/8 - get<2>(stats.back()) ) << endl;
			cout << "Time for parsing " << get<0>(stats.back()) << " : " << st.stop_and_get() << endl;st = sys_timer(); st.start();
		}
	}
	boundaries_t boundaries(stats.size());
	for(size_t i = 0; i < stats.size(); i++) boundaries[i] = get<2>(stats[i]);
	sdsl::store_to_file(boundaries, out_pref + FEX_parseB);

	parse.close();

	analyse_dict(D_map);
	cout << "Time for analysing : " << st.stop_and_get() << endl;st = sys_timer(); st.start();
	vector<word_stats *> P_arr =  calc_ranks(D_map);
	cout << "Time for ranks : " << st.stop_and_get() << endl;st = sys_timer(); st.start();
	refine_parse(parse_fname, D_map);
	cout << "Time transform parses: " << st.stop_and_get() << endl;st = sys_timer(); st.start();
	store_dict(P_arr, out_pref);
	cout << "Time for storing : " << st.stop_and_get() << endl;st = sys_timer(); st.start();


	if(false) { //PRINT DATA FOR DEBUGGING PURPOSE ONLY
		string D;
		ifstream stream;
		stream.open(out_pref+".dict");
		stream >> D;
		stream.close();

		sdsl::int_vector<64> D_IDX;
		sdsl::load_vector_from_file(D_IDX, out_pref+".dict.idx", 8);

		sdsl::int_vector<32> P;
		sdsl::load_vector_from_file(P, out_pref+".parse", 4);


		cout <<  "D\n" << D << endl;
		cout << endl;
		cout << "IDX" << endl;
		for(size_t i = 0 ; i < D_IDX.size() ; i++ ) cout << D_IDX[i] << " ";
		cout << endl;
		cout << "P" << endl;
		for(size_t i = 0 ; i < P.size() ; i++ ) cout << P[i] << " ";
		cout << endl;

		cout << "boundaries" << endl;
		for(size_t i = 0 ; i < boundaries.size() ; i++ ) cout << boundaries[i] << " ";
		cout << endl;

		for(size_t t = 0; t < stats.size(); t++){

			cout << "stats " << get<0>(stats[t]) << " " << get<1>(stats[t])<< " " << get<2>(stats[t]) << endl;
			cout << "orig " << endl;
			string T;
			stream.open(get<0>(stats[t]));
			stream >> T;
			stream.close();
			cout << T << endl;

			cout << "decompressed " << endl;
			uint32_t pos = get<2>(stats[t]);
			uint32_t end   = (t+1< stats.size())? get<2>(stats[t+1]) : P.size();

			while(pos < end){
				uint32_t w = P[pos++] -1 ; //rank
				uint32_t l = D_IDX[w];
				uint32_t r = w+1 < D_IDX.size() ? D_IDX[w+1] : D.size() ;
				cout << D.substr(l, r-l);
			}

			cout << endl;
		}

		cout << "MAP" << endl;
		for(auto d : D_map){
			cout << d.first << " ->  " << d.second.str << endl;
		}
	}



	return 0;
}