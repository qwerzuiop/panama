#pragma once

#include <cassert>
#include <cctype>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include "SAM.hpp"
#include "common.hpp"
#include "config.hpp"
#include "file_util.hpp"
#include "subprocess.hpp"

struct profile_t {
	using aligned_t = std::vector<std::pair<size_t, std::string>>; // (num_bases, alignment)
	// NOTE: when aligned(), seq.len is meaningless and should not be used
	MEM_t seq;
	aligned_t m_aligned;
	size_t num_seq() const
	{
		return seq.coords.size();
	}
	bool aligned() const
	{
		assert(m_aligned.empty() or m_aligned.size() == seq.coords.size());
		return not m_aligned.empty();
	}
	size_t length(size_t i) const
	{
		if (aligned()) {
			assert(present(i) or m_aligned[i].first == 0);
			return m_aligned[i].first;
		}
		return seq.length(i);
	}
	const my_int& pos(size_t i) const
	{
		return seq.coords[i];
	}
	my_int& pos(size_t i)
	{
		return const_cast<my_int&>(const_cast<const profile_t*>(this)->pos(i));
	}
	bool present(size_t i) const
	{
		return seq.coords[i] != MEM_t::NOT_PRESENT;
	}
	void invalidate()
	{
		seq.coords.clear();
	}
	bool valid() const
	{
		return not seq.coords.empty();
	}
	template <typename F>
	void for_each_present_coord(F&& f) const
	{
		seq.for_each_present_coord<F>(std::forward<F>(f));
	}
	profile_t(profile_t&& rhs)
	{
		*this = std::move(rhs);
	}
	profile_t(const profile_t&) = default;
	profile_t& operator=(profile_t&& rhs)
	{
		if (&rhs == this)
			return *this;
		std::swap(rhs.m_aligned, m_aligned);
		std::swap(rhs.seq, seq);
		return *this;
	}
	profile_t& operator=(const profile_t&) = default;
	profile_t() = default;
	profile_t(size_t len, std::vector<my_int>&& coords)
		: seq(len, std::move(coords))
	{
	}
	bool operator==(const profile_t& rhs) const
	{
		return seq == rhs.seq and m_aligned == rhs.m_aligned;
	}
};

std::vector<std::string> aligner_commands(const MainAligner_t aligner, const char* filepath, const char* output_filepath, size_t size)
{
	switch (aligner) {
	case HAlign3:
		return {
			"java",
			"-jar", gap_fill_config.fmalign_directory + "/ext/halign3/share/halign-stmsa.jar",
			"-t", "128", // TODO: only do this for large gaps
			"-o", output_filepath,
			filepath
		};
	case FMAlign2:
		return {
			gap_fill_config.fmalign_directory + "/FMAlign2",
			"-i", filepath,
			"-p", (size <= 2e5 ? "mafft" :"halign3"),
			"-o", output_filepath
		};
	default:
		assert(false);
		__builtin_unreachable();
	}
}

// answers from left come before answers for right
std::vector<std::string> merge_external(
	const std::string& prefix,
	std::vector<std::string>&& left,
	std::vector<std::string>&& right)
{
	const std::string left_path = gap_fill_config.tmp_directory + "/" + prefix + ".l.fasta";
	const std::string right_path = gap_fill_config.tmp_directory + "/" + prefix + ".r.fasta";
	{
		std::ofstream out(left_path, std::ios::out);
		if (not out.is_open() or out.bad())
			throw std::runtime_error("could not write to \"" + left_path + "\"");
		for (size_t i = 0; i < left.size(); i++) {
			if (i + 1 < left.size())
				assert(left[i].size() == left[i + 1].size());
			out << "> left_" << i << '\n'
				<< std::move(left[i]) << '\n';
		}
		out.close();
	}
	{
		std::ofstream out(right_path, std::ios::out);
		if (not out.is_open() or out.bad())
			throw std::runtime_error("could not write to \"" + right_path + "\"");
		for (size_t i = 0; i < right.size(); i++) {
			if (i + 1 < right.size())
				assert(right[i].size() == right[i + 1].size());
			out << "> right_" << i << '\n'
				<< std::move(right[i]) << '\n';
		}
		out.close();
	}

	auto aligner = subprocess::Popen({ gap_fill_config.fmalign_directory + "/ext/mafft/linux/usr/bin/mafft-profile",
										 left_path,
										 right_path },
		subprocess::output { subprocess::PIPE }, subprocess::error { subprocess::PIPE });
	auto response = aligner.communicate();

	if (0) {
		std::ofstream out(prefix + ".output.fasta");
		out << std::string_view { response.first.buf.data(), response.first.length } << std::endl;
		out.close();
	}

	line_splitter_t<std::vector<char>> output(std::move(response.first.buf), response.first.length);
	std::vector<std::string> res(left.size() + right.size());
	std::string_view line = output.getline();
	for (size_t i = 0; i < res.size(); i++) {
		if (line.empty() or line[0] != '>')
			throw std::runtime_error("failed to parse " + prefix + " at " + std::to_string(i) + "/" + std::to_string(res.size()) + ", got \"" + (std::string)line + "\"");
		size_t p = 1;
		while (p < line.size() and std::isspace(line[p]))
			p++;
		size_t idx;
		if (line.substr(p, 5) == "left_") {
			idx = std::stoull((std::string)line.substr(p + 5));
		} else if (line.substr(p, 6) == "right_") {
			idx = std::stoull((std::string)line.substr(p + 6)) + left.size();
		} else
			throw std::runtime_error("failed to parse header in " + prefix + '\"' + (std::string)line + '\"');
		while (output and (line = output.getline(), true) and (line.empty() or line[0] != '>'))
			res[idx] += std::move(line);

		assert(not res[idx].empty());
	}
	for (size_t s = 1; s < res.size(); s++)
		assert(res[s].size() == res[s - 1].size());
	std::cout << prefix << " alignment is " << res[0].size() << " wide" << std::endl;

	std::remove(left_path.c_str());
	std::remove(right_path.c_str());

	return res;
}

struct minimap2_result_t {
	uint8_t mapq = 0;
	size_t start = 0;
	std::optional<std::pair<std::string, std::string>> alignment; // first is reference, second is pattern
};
std::atomic<size_t> num_perfect = 0;
std::vector<minimap2_result_t> minimap2(
	const std::string& id,
	std::vector<std::string_view> pat,
	std::string_view text)
{
	const std::string ref_path = id + ".ref.fa";
	std::ofstream out(ref_path);
	if (not out.is_open() or out.bad())
		throw std::runtime_error("could not write to \"" + ref_path + "\"");
	out << ">text\n"
		<< text << "\n";
	out.close();

	const std::string query_path = id + ".query.fq";
	out.open(query_path);
	if (not out.is_open() or out.bad())
		throw std::runtime_error("could not write to \"" + query_path + "\"");
	for (size_t i = 0; i < pat.size(); i++) {
		if (pat[i].empty())
			continue;
		out << "@query" << i << '\n'
			<< pat[i] << '\n'
			<< "+\n"
			<< std::string(pat[i].size(), '~')
			<< '\n';
	}
	out.close();

	auto aligner = subprocess::Popen({ gap_fill_config.minimap_directory + "/minimap2", "-a", ref_path, query_path },
		subprocess::output { subprocess::PIPE }, subprocess::error { subprocess::PIPE });
	auto response = aligner.communicate().first;

	std::string_view literal_output { response.buf.data(), response.length };
	auto results = parse_SAM(std::string(literal_output));

	// out.open(id + ".ans.sam");
	// out << std::string_view { response.buf.data(), response.length };

	std::vector<minimap2_result_t> res(pat.size());
	for (const SAM_alignment_t& ali : results) {
		if (ali.flag & SAM_FLAG_t::SEG_UNMAPPED)
			continue;
		if (ali.flag & SAM_FLAG_t::SEQ_REV_COMPL)
			continue;
		if (ali.mapq < 30)
			continue;
		if (ali.qname.substr(0, 5) != "query")
			throw std::runtime_error("qname in " + id + " is wrong");
		const size_t i = svtoT<size_t>(ali.qname.substr(5));

		if (res[i].mapq == 255 or res[i].mapq > ali.mapq)
			continue;

		minimap2_result_t tmp;
		tmp.mapq = ali.mapq;
		tmp.start = ali.pos;
		tmp.alignment = std::nullopt;
		CIGAR_t qual(ali.cigar);
		if (qual.perfect()) {
#ifndef NDEBUG
			auto substr = text.substr(tmp.start, pat[i].size());
			assert(substr == pat[i]);
#endif
			res[i] = std::move(tmp);
			num_perfect++;
			continue;
		}
		const auto fail = [&] {
			throw std::runtime_error(id + "\n" + std::string(ali.qname) + "\n" + qual.to_string());
		};
		std::pair<std::string, std::string> alignment;
		size_t ref_i = res[i].start, pat_i = 0;
		for (char c : qual) {
			if (c == 'M' or c == '=' or c == 'X') {
				if (not(ref_i < text.size()))
					fail();
				assert(ref_i < text.size());
				alignment.first += text[ref_i++];
				if (not(pat_i < pat[i].size()))
					fail();
				assert(pat_i < pat[i].size());
				alignment.second += pat[i][pat_i++];
			} else if (c == 'I' or c == 'S') {
				alignment.first += '-';
				assert(pat_i < pat[i].size());
				alignment.second += pat[i][pat_i++];
			} else if (c == 'D' or c == 'N') {
				assert(ref_i < text.size());
				alignment.first += text[ref_i++];
				alignment.second += '-';
			} else if (c == 'H' or c == 'P') {
				pat_i++;
				alignment.first += '-';
				alignment.second += '-';
			} else
				throw std::runtime_error(id + " " + std::string(ali.qname) + ": unknown symbol in CIGAR '" + c + '\'');
		}

		if (pat_i != pat[i].size()) {
			throw std::runtime_error(string_builder {} << id << " " << ali.qname
													   << ": consumed wrong number of pattern symbols: "
													   << pat_i << " but expected " << pat[i].size()
													   << "\nqual: " << qual.to_string()
													   << '\n'
													   << alignment.first
													   << '\n'
													   << alignment.second);
		}
		tmp.alignment = std::move(alignment);
		res[i] = std::move(tmp);
	}

	std::remove(ref_path.c_str());
	std::remove(query_path.c_str());

	return res;
}

std::string get_external(const std::string& prefix, std::string_view output, std::atomic<size_t>& num_fallback)
{
	const MainAligner_t main_aligner = gap_fill_config.external_aligner;

	const std::string filepath = gap_fill_config.tmp_directory + "/" + prefix;
	{
		std::ofstream out(filepath, std::ios::out);
		if (not out.is_open() or out.bad())
			throw std::runtime_error("could not write to \"" + filepath + "\"");
		out << output;
		out.close();
	}

	if (main_aligner != MainAligner_t::None) {
		const std::string output_filepath = filepath + ".ans";
		try {
			auto aligner = subprocess::Popen(aligner_commands(main_aligner, filepath.c_str(), output_filepath.c_str(), output.size()),
				subprocess::output { subprocess::PIPE }, subprocess::error { subprocess::PIPE });
			aligner.communicate();

			auto res = read_file(output_filepath);
			std::remove(output_filepath.c_str());
			std::remove(filepath.c_str());
			return res;
		} catch (const std::exception& e) {
			num_fallback++;
			if (num_fallback < 20)
				std::cout << main_aligner << " failed on " << filepath << ": " << e.what()
						  << "\n\t=> falling back to mafft"
						  << std::endl;
			std::remove(output_filepath.c_str());
		}
	}

	auto aligner = subprocess::Popen({ gap_fill_config.fmalign_directory + "/ext/mafft/linux/usr/libexec/mafft/disttbfast",
										 "-q", "0",
										 "-E", "1",
										 "-V", "-1.53",
										 "-s", "0.0",
										 "-W", "6",
										 "-O",
										 "-C", "4",
										 "-b", "62",
										 "-g", "0",
										 "-f", "-1.53",
										 "-Q", "100.0",
										 "-h", "0",
										 "-F",
										 "-X", "0.1",
										 "-i", filepath.c_str() },
		subprocess::output { subprocess::PIPE }, subprocess::error { subprocess::PIPE });
	auto response = aligner.communicate();

	// std::remove(filepath.c_str()); // delete temp. file

	return std::string { response.first.buf.data(), response.first.length };
}

// uses mafft via pipe
std::string mafft(std::string_view data)
{
	// write via pipe: WARNING does not work with disttbfast or FMAlign2
	auto aligner = subprocess::Popen(
		{ "/usr/bin/mafft", "--quiet", "-" }, subprocess::output { subprocess::PIPE }, subprocess::input { subprocess::PIPE });
	aligner.send(data.data(), data.size());

	auto response = aligner.communicate();
	std::string _aligner_stdout { response.first.buf.data(), response.first.length };
	return _aligner_stdout;
}

constexpr size_t external_align_max = 2e7;
constexpr size_t external_align_max_width = 5e4;

constexpr size_t mafft_align_max = 1e5;

size_t bases_consumed(const std::string_view& s) {
	size_t res = 0;
	for (char c : s)
		switch (c) {
			case 'A': case 'G': case 'T': case 'C':
			case 'a': case 'g': case 't': case 'c':
			res++;
			break;
			default:;
		}
	return res;
}

std::atomic<size_t> num_fallback = 0;
template<bool force = false>
std::conditional_t<force, std::vector<std::string>, std::optional<std::vector<std::string>>>
align_external(
	const std::vector<std::string_view>& sequences,
	const std::string& prefix)
{
	if (sequences.empty())
		return std::vector<std::string>();
	if (sequences.size() == 1)
		return std::vector<std::string>(1, std::string(sequences[0]));
	size_t total_length = 0, max_length = 0;
	for (const auto& s : sequences)
		total_length += s.size(), max_length = std::max(max_length, s.size());
	size_t total_deviation = 0;
	for (const auto& s : sequences)
		total_deviation += max_length - s.size();
	if constexpr (not force)
		if (total_deviation > external_align_max_width or (total_length > external_align_max or max_length > external_align_max_width))
			return std::nullopt;
	const std::string fasta_prefix = ">" + prefix + "_";

	line_splitter_t<std::string> aligner_stdout;
	size_t num_alignments_read = 0;

	std::vector<std::string> res(sequences.size());
	size_t width = 0;
	const auto try_read = [&] {
		for (auto& s : res)
			s.clear();
		width = 0;

		bool ok = true;
		read_fasta(aligner_stdout, [&](std::string&& header, std::string&& alignment) {
			num_alignments_read++;
			if (header.substr(0, fasta_prefix.size()) != fasta_prefix)
				throw std::runtime_error(string_builder {} << prefix << " got line \"" << header << "\" and expected prefix " << fasta_prefix);
			const size_t s = std::stoull(header.substr(fasta_prefix.size()));
			const size_t sz = bases_consumed(sequences[s]);
			const size_t bc = bases_consumed(alignment);
			if (bc != sz) {
				std::ofstream out(prefix + ".fail");
				out << sequences[s] << '\n' << alignment << '\n';
				out.close();
				ok = false;
				// throw std::runtime_error(string_builder{} << prefix << " @ seq " << bc << " but expected " << sz);
			}
			res[s] = std::move(alignment);
			if (bases_consumed(res[s]) != bases_consumed(sequences[s]))
				ok = false;
			if (width == 0)
				width = res[s].size();
			else if (width != res[s].size())
				ok = false;
		});
		return ok;
	};
	{
		std::ostringstream out; // TODO: maybe a std::string is faster here?
		for (size_t i = 0; i < sequences.size(); i++)
			if (sequences[i].empty())
				num_alignments_read++;
			else
				out << fasta_prefix << i << '\n'
					<< sequences[i] << '\n';
		aligner_stdout = line_splitter_t(get_external(prefix, out.rdbuf()->view(), num_fallback));
		if (not try_read()) {
			{
				std::ofstream in_file(prefix + ".in.fa");
				in_file << out.view();
				std::ofstream out_file(prefix + ".out.fa");
				out_file << aligner_stdout.m_value;
			}
			std::cout << prefix << " external produced invalid alignment!" << std::endl;
			std::cerr << prefix << " external produced invalid alignment!" << std::endl;
			assert(false);
			std::abort();

			if constexpr (not force)
				if (total_length > mafft_align_max) {
					std::cout << std::quoted(prefix) << ": external program produced invalid alignment, but gap too large for mafft" << std::endl;
					return std::nullopt;
				}
			std::cout << std::quoted(prefix) << ": external program produced invalid alignment, trying again with mafft" << std::endl;
			aligner_stdout = line_splitter_t(mafft(out.view()));
			if (not try_read())
				throw std::runtime_error("mafft produced an invalid alignment on " + prefix);
		}
	}
	assert(width > 0);
	for (size_t s = 0; s < sequences.size(); s++)
		if (sequences[s].empty()) {
			assert(res[s].empty());
			res[s].resize(width, '-');
		}
	if (num_alignments_read != res.size())
		throw std::runtime_error(string_builder {} << prefix << " expected " << res.size() << " alignments, but got " << num_alignments_read);
	for (auto& s : res)
		s.shrink_to_fit();
	return res;
}
