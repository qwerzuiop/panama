#include "system_timer.cpp"
#include <iostream>
#include <string>

#include "common.hpp"

using namespace std;


int main(int argc, char** argv){

	cout << "[1] prefix of input"  << endl;
	if(argc  < 2) return 0;

	string in_pref = argv[1];

	suffix_tree_t ST_P;

	sys_timer st; st.start();
	cout << "=== generate ST for parse:" << endl;
	sdsl::construct(ST_P, in_pref + FEX_parse , 4);
	cout << "Time for ST : " << st.stop_and_get() << endl; st = sys_timer();
	cout << "Store ST" << endl;
	sdsl::store_to_file(ST_P, in_pref+ FEX_ST);
}
