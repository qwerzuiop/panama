#include <cctype>
#include <cstring>
#include <iostream>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include <string>

#define NO_DEBUG_OUTPUT

#include "common.hpp"
#include "config.hpp"
#include "external.hpp"
#include "file_util.hpp"
#include "malign.hpp"
#include "system_timer.cpp"
#include "thread_pool.hpp"
#include "tui.hpp"
#include "trivial.hpp"


struct gap_align_t {
	std::vector<substr_t> gap;
	std::vector<prefix_align_res_t> pref;
};

gap_align_t prefix_align_gap(const my_index_t& I, const std::vector<BLOCK_T>& blocks, const size_t i)
{
	gap_align_t result;
	result.gap = gap_strings(
		i == 0
			? dummy_block_front(I)
			: blocks[i - 1],
		i < blocks.size()
			? blocks[i]
			: dummy_block_back(I),
		I);

	// find all sequences where the gap is non-empty
	for (size_t s = 0; s < result.gap.size(); s++)
		if (not result.gap[s].seq.empty())
			result.pref.emplace_back(s, std::string_view { result.gap[s].seq });

	if (not result.pref.empty()) // align equal prefixes of the non-empty gap
		align_prefixes(result.pref);
	return result;
}

std::vector<std::vector<std::string>> fill_gaps_external(const my_index_t& I, const std::vector<BLOCK_T>& blocks)
{
	constexpr size_t UNDEF = std::numeric_limits<size_t>::max();

	const size_t num_seq = I.boundaries.size();
	std::vector<std::vector<std::string>> gap_results(blocks.size() + 1);

	std::atomic<size_t> num_trivial = 0, num_single = 0, num_too_large = 0;
	std::atomic<size_t> num_done = 0, num_started = 0;
	std::atomic<size_t> sum_large_size = 0;

	std::filesystem::current_path(gap_fill_config.fmalign_directory);

	capture_cout_t cout_capture;
	std::mutex running_mutex;
	auto rep = repeat([&, status_handle = tui.handle()]() mutable {
		std::lock_guard<std::mutex> lock(running_mutex);
		status_handle.refill(string_builder {}
			<< "finished " << num_done << "(+" << (num_started - num_done) << ")/" << (blocks.size() + 1) << " gaps, "
			<< num_trivial << " of which were trivial, "
			<< num_single << " had width 1, "
			<< num_too_large << " were too large (" << sum_large_size << ")"
			#ifndef NO_DEBUG_OUTPUT
			<< "\ntimer for go2: " << go2_time << "s, of which for ST/MUMs: " << st_time << "s/" << mum_time << "s"
			<< "\nbackbone cuts: " << num_sub_backbones << ", of which have no gap: " << num_zero_gaps << ", alignable: " << num_alignable_gaps << ", minimap2 added: " << num_added_mems
			#endif
			<< "\nneeded to fall back " << num_fallback << " times");
		tui.update_tui(cout_capture);
	});
#ifdef NO_DEBUG_OUTPUT
	tui.ignore();
#endif

	thread_pool_t thread_pool(48);
	for (auto& x : gap_results)
		x.resize(num_seq);
	thread_pool.for_each_in_range(blocks.size() + 1, [&](const size_t i) {
		num_started++;
		assert(i < gap_results.size());

		auto gap = prefix_align_gap(I, blocks, i);

		if (gap.pref.empty()) {
			// special case, when the gap is empty in all sequences (TODO: should this even happen?)
			num_trivial++;
			num_done++;
			return;
		}

		if (const size_t prefix_alignment_length = gap.pref[0].alignment.size(); prefix_alignment_length > 0) {
			// there is a prefix all non-empty gaps share; we append it to the gap result
			for (size_t s = 0, j = 0; s < num_seq; s++) {
				if (j < gap.pref.size() and s == gap.pref[j].sequence_id) {
					assert(gap.pref[j].alignment.size() == prefix_alignment_length);
					gap_results[i][s] += gap.pref[j].alignment;
					j++;
				} else {
					gap_results[i][s] += std::string(prefix_alignment_length, '-');
				}
			}
		}
		// find sequences where the gap is non-empty after aligning equal prefixes
		std::vector<size_t> remaining; // maps to non_empty
		for (size_t j = 0; j < gap.pref.size(); j++)
			if (not gap.pref[j].rest.empty())
				remaining.emplace_back(j);

		if (remaining.empty()) {
			num_trivial++;
			num_done++;
			return;
		}

		// special case for gaps of length 1
		size_t max_len = 0, total_len = 0;
		for (size_t j : remaining) {
			max_len = std::max(max_len, gap.pref[j].rest.size());
			total_len += gap.pref[j].rest.size();
		}

		const std::string prefix = "gap_" + std::to_string(i);
		if (max_len == 1) {
			num_single++;
			for (size_t j : remaining) {
				const size_t s = gap.pref[j].sequence_id;
				assert(gap.pref[j].rest.size() == 1);
				gap_results[i][s] += gap.pref[j].rest[0];
			}
		} else if (auto aligned = [&] {
					   std::vector<std::string_view> sequences(remaining.size());
					   for (size_t k = 0; k < remaining.size(); k++) {
						   const size_t j = remaining[k];
						   sequences[k] = gap.pref[j].rest;
					   }
					   return align_external(sequences, prefix);
				   }()) {
			for (size_t k = 0; k < remaining.size(); k++) {
				const size_t j = remaining[k];
				const size_t s = gap.pref[j].sequence_id;
				gap_results[i][s] += std::move((*aligned)[k]);
			}
		} else {
			const auto num = num_too_large.fetch_add(1);

			const std::string id = "large_" + std::to_string(num);

			constexpr bool export_gap = false;

			if constexpr (export_gap) {
				std::ofstream out(id + ".fa");
				if (not out.is_open())
					throw std::runtime_error("Could not open " + id);
				auto prev_block = i == 0 ? dummy_block_front(I) : blocks[i - 1];
				for (size_t j : remaining) {
					coord_t c = prev_block[j];
					I.shift_coord<true>(c, prev_block.length);
					out << ">gap_" << i << "_seq_" << gap.pref[j].sequence_id << "_" << I.get_base_idx(c) - I.get_base_idx(I.boundaries[j]) << '\n';
					out << gap.pref[j].rest << '\n';
				}
				out.close();
			}

			std::vector<std::string_view> gaps;
			for (size_t j : remaining)
				gaps.emplace_back(gap.pref[j].rest);

			// std::cout << id << ": " << gaps.size() << " sequences" << std::endl;

			const size_t threshold = remaining.size() / 2 + 1;
			constexpr bool proper_align = true;
			std::vector<std::string> res = proper_align
				? malign(std::move(gaps), threshold, id, thread_pool)
				: [&] {
					  // just "align" by writing the sequences left aligned and filling up with '-'
					  std::vector<std::string> tmp(gaps.size());
					  for (size_t i = 0; i < gaps.size(); i++) {
						  assert(gaps[i].size() <= max_len);
						  tmp[i].resize(max_len, '-');
						  std::copy(gaps[i].begin(), gaps[i].end(), tmp[i].begin());
					  }
					  return tmp;
				  }();

			{
				size_t total_len = 0;
				for (size_t j : remaining)
					total_len += gap.pref[j].rest.size();
				sum_large_size += total_len;
			}

			if constexpr (export_gap) {
				std::ofstream out(id + ".aligned.fa");
				if (not out.is_open())
					throw std::runtime_error("Could not open " + id + ".aligned.fa");
				for (size_t k = 0; k < remaining.size(); k++) {
					const size_t j = remaining[k];
					out << ">gap_" << i << "_seq_" << gap.pref[j].sequence_id << '\n';
					out << res[k] << '\n';
				}
				out.close();
			}

			for (size_t k = 0; k < remaining.size(); k++) {
				const size_t j = remaining[k];
				const size_t s = gap.pref[j].sequence_id;

				// assert(res[k].size() >= max_len);
				gap_results[i][s] += std::move(res[k]);
			}
		}

#ifndef NDEBUG
		if (0) { // just some validation, check that the number of 'A','C','G','T's are correct
			for (size_t s = 0; s < num_seq; s++) {
				const auto id = [&](char c) -> size_t {
					switch (std::toupper(c)) {
					case 'A':
						return 0;
					case 'C':
						return 1;
					case 'G':
						return 2;
					case 'T':
						return 3;
					default:
						return 4;
					}
				};
				std::array<size_t, 4> expected = { 0, 0, 0, 0 };
				auto count = expected;
				for (char c : gap.gap[s].seq)
					if (auto i = id(c); i < 4)
						expected[i]++;
				for (char c : gap_results[i][s])
					if (auto i = id(c); i < 4)
						count[i]++;
				assert(count == expected);
			}
		}
#endif

		size_t alignment_width = UNDEF;
		for (size_t j : remaining) {
			const size_t s = gap.pref[j].sequence_id;
			if (alignment_width == UNDEF)
				alignment_width = gap_results[i][s].size();
			else if (alignment_width != gap_results[i][s].size())
				throw std::runtime_error(string_builder {} << "gap " << i << " alignment size differ: " << alignment_width << " vs. " << gap_results[i][s].size());
		}
		assert(alignment_width != UNDEF);
		for (auto& g : gap_results[i]) {
			assert(g.size() <= alignment_width);
			g.resize(alignment_width, '-');
		}
		num_done++;
	});
	thread_pool.stop();

	std::filesystem::current_path(config.original_working_directory);
	for (auto& s : gap_results)
		s.shrink_to_fit();
	return gap_results;
}

void write_alignment(const std::string& path, const my_index_t& I, const std::vector<BLOCK_T>& blocks, std::vector<std::vector<std::string>>&& gap_alignments)
{
	std::cout << "writing alignment..." << std::endl;

	std::ofstream out(path, std::ios::out);
	if (not out.is_open() or out.bad())
		throw std::runtime_error("Could not open \"" + path + "\"");

	const size_t buf_size = 1u << 25;
	const auto buf = std::make_unique<char[]>(buf_size);
	out.rdbuf()->pubsetbuf(buf.get(), buf_size);

	assert(gap_alignments.size() == blocks.size() + 1);

	const size_t num_seq = I.boundaries.size();
	// write sequence after sequence; this is probably slower but saves a lot of RAM
	for (size_t s = 0; s < num_seq; s++) {
		out << ">" << s << '\n';
		for (size_t i = 0; i <= blocks.size(); i++) {
			if (i > 0)
				I.substr(blocks[i - 1][0], blocks[i - 1].length, std::ostreambuf_iterator<char>(out));
			out << std::move(gap_alignments[i][s]);
		}
		out << '\n';
	}

	out.close();
}

int main(int argc, const char** argv)
{
#ifndef NDEBUG
	std::cerr << "WARNING: RUNNING IN DEBUG MODE, THIS MAY BE SLOW" << std::endl;
	std::cerr << "\t(compile with -DNDEBUG to disable debug mode)" << std::endl;
#endif
	cout << "Assumes existence of " << FEX_parse << " " << FEX_parseB << " " << FEX_dictx << " " << FEX_BLOCK << endl;
	cout << "[1] prefix of input" << endl;
	cout << "    (optional) [-d <path to working directory>]\n";
	cout << "               [-fmalign <directory in which FMAlign2 is>]\n";
	cout << "               [-minimap <directory in which minimap2 is>]\n";

	gap_fill_config = fill_gaps_config_t(argc, argv);

	cout << "loading parse, chain, dict .." << flush;
	sys_timer st;
	st.start();
	my_index_t I(config.in_pref);
	cout << "\rTime for loading parse, dict " << st.stop_and_get() << endl;

	// READ BLOCKS
	checked_ifstream_t blocks_in(config.in_pref + FEX_BLOCK);
	vector<BLOCK_T> B;
	while (true) {
		BLOCK_T b(blocks_in);
		if (!blocks_in)
			break;
		B.push_back(std::move(b));
	}
	blocks_in.check_and_close();

	st = sys_timer();
	st.start();
	auto gaps = fill_gaps_external(I, B);
	std::cout << "\nTime for filling gaps: " << st.stop_and_get() << std::endl;

	st = sys_timer();
	st.start();
	write_alignment(config.in_pref + ".aligned.fa", I, B, std::move(gaps));
	std::cout << "\nTime for writing alignment: " << st.stop_and_get() << std::endl;

	return 0;
}
