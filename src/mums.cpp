#pragma once

#include "thread_pool.hpp"
#include "common.hpp"
namespace mum{

template<typename ST, typename F>
void mums(const boundaries_t & boundaries, const ST& st,  const F& emit, const index_t length_threshold = 0){

	//std::cout << "MUM" << std::endl;

	const auto test_node = [&](const typename ST::node_type&  v) -> void{
		size_t len = st.depth(v);
		if(len < length_threshold) return;
		std::vector<index_t> coords(boundaries.size(), MEM_t::NOT_PRESENT);
		size_t i = st.lb(v);
		auto left_ctx = st.csa.bwt[i];
		bool left_max = false;

		const auto parse_id = [&](const index_t& i) {
			const auto it = std::upper_bound( boundaries.begin(), boundaries.end(), i );
			assert(it != boundaries.begin());
			assert(it == boundaries.end() or *it > i);
			assert(*std::prev(it) <= i);
			return std::distance(boundaries.begin(), std::prev(it));
		};

		for(; i <= st.rb(v); i++){
			const auto j = st.csa[i];
			size_t k = parse_id( j );
			if  (coords[k] != MEM_t::NOT_PRESENT)	return; //NOT UNIQUE
			else	coords[k]  = j;
			left_max = left_max || st.csa.bwt[i] != left_ctx;
		}
		if(left_max) emit( len ,std::move(coords));
	};


	for (auto it = st.begin_bottom_up(); it != st.end_bottom_up(); it++)
		if( st.size(*it) == boundaries.size())
			test_node(*it);
}

template<typename ST, typename F>
void mums(const boundaries_t & boundaries, const ST& st,  const F& emit, const index_t length_threshold, const size_t match_threshold){

	//std::cout << "MUM" << std::endl;

	const auto test_node = [&](typename ST::node_type  v) -> bool{
		size_t len = st.depth(v);
		if(len < length_threshold) return false;
		std::vector<index_t> coords(boundaries.size(),MEM_t::NOT_PRESENT);
		size_t i = st.lb(v);
		auto left_ctx = st.csa.bwt[i];
		bool left_max = false;

		const auto parse_id = [&](const index_t& i) {
			const auto it = std::upper_bound( boundaries.begin(), boundaries.end(), i );
			assert(it != boundaries.begin());
			assert(it == boundaries.end() or *it > i);
			assert(*std::prev(it) <= i);
			return std::distance(boundaries.begin(), std::prev(it));
		};

		for(; i <= st.rb(v); i++){
			size_t k = parse_id( st.csa[i] );
			if  (coords[k] != MEM_t::NOT_PRESENT )	return false; //NOT UNIQUE
			else	coords[k]  = st.csa[i];
			left_max |=  (st.csa.bwt[i] != left_ctx);
		}
		if(left_max) emit( len ,std::move(coords));
		return left_max;
	};


	for (auto it = st.begin_bottom_up(); it != st.end_bottom_up(); it++){
		size_t size = st.size(*it);
		if( size <= boundaries.size() &&  size >= match_threshold)
			test_node(*it);
	}
	using it_type = typename ST::const_iterator;
	for (auto it = it_type(&st,st.root(),0,1); it != it_type(&st,st.root(),1,1); ){
		size_t size = st.size(*it);
		if( size  > boundaries.size() || it.visit() > 1 ) {it++;} // >k -> CANT BE MUM  // visit > 1 -> already tested
		else if( size <= boundaries.size() && size >= match_threshold ) { if (test_node(*it)) it.skip_subtree(); else it++; }
		else it.skip_subtree(); // -> size() <= k -> all children will be to small
	}
}


// TODO: if MUM occurs more than once in a sequence, don't immediately discard it
template<typename ST, typename F>
void mums(const boundaries_t& boundaries, const ST& st, const F& emit, const index_t match_threshold, const size_t match_length, thread_pool_t* threadpool){
	using node_type = typename ST::node_type;
	const size_t num_seq = boundaries.size();

	std::mutex emit_mutex;
	const auto test_node = [&](node_type  v){
		const size_t len = st.depth(v);
		std::vector<index_t> coords(num_seq, MEM_t::NOT_PRESENT);
		size_t i = st.lb(v);
		auto left_ctx = st.csa.bwt[i];
		bool left_max = false;

		const auto parse_id = [&boundaries](const index_t& i) {
			const auto it = std::upper_bound( boundaries.begin(), boundaries.end(), i );
			assert(it != boundaries.begin());
			assert(it == boundaries.end() or *it > i);
			assert(*std::prev(it) <= i);
			return std::distance(boundaries.begin(), std::prev(it));
		};

		for(; i <= st.rb(v); i++){
			size_t k = parse_id( st.csa[i] );
			if  (coords[k] != MEM_t::NOT_PRESENT )	return false; //NOT UNIQUE
			else	coords[k]  = st.csa[i];
			if (not left_max and st.csa.bwt[i] != left_ctx)
				left_max = true;
		}
		if(left_max and len >= match_length) {
			const std::lock_guard<std::mutex> lock(emit_mutex);
			emit( len , std::move(coords));
		}
		else left_max = false;
		return left_max;
	};

	//TRAVERSE SUBTREE BELOW V IN TOP DOWN MANNER USING BFS
	const auto bfs = [&](const node_type v) -> void {
		std::stack<node_type, std::vector<node_type>> q;
		q.emplace(v);
		while (not q.empty()) {
			const auto v = q.top();
			q.pop();

			const size_t size = st.size(v);
			if (size < match_threshold)
				return; //IF INTERVAL IS ALREADY TO SMALL, DONT LOOK AT CHILDREN
			if (size <= num_seq and test_node(v))
				return;
			for (const auto& c : st.children( v ))
				q.emplace(c);
				// self(self, c);
		}
	};

	const auto dfs_par = [&](auto&& self, const node_type v, const index_t par_depth) -> void {
		size_t size = st.size(v);
		if (size < match_threshold)
			return;
		if(size <= num_seq and test_node(v))
			return;

		std::vector< node_type > children;
		for (const auto& c : st.children( v )) children.emplace_back(c);

		threadpool->for_each_in_range(children.size(), [&](const size_t c_i) {
			const auto& c = children[c_i];
			if (par_depth > 0) self(self, c, par_depth - 1);
			else bfs(c);
		});
	};

	if (threadpool)
		dfs_par(dfs_par, st.root(), 1);
	else
		bfs(st.root());
}
template<typename ST, typename F>
void rep_mems(const boundaries_t& boundaries, const ST& st, const F& emit, const index_t match_threshold, const size_t match_length, thread_pool_t* threadpool){
	using node_type = typename ST::node_type;
	using char_type = typename ST::char_type;
	const size_t num_seq = boundaries.size();

	std::mutex emit_mutex;
	const auto test_node = [&](node_type  v) -> bool {
		const size_t len = st.depth(v);
		if (len < match_length)
			return false;
		std::vector<std::vector<std::pair<index_t, char_type>>> coords(num_seq);

		const auto parse_id = [&](const index_t& i) {
			const auto it = std::upper_bound( boundaries.begin(), boundaries.end(), i );
			assert(it != boundaries.begin());
			assert(it == boundaries.end() or *it > i);
			assert(*std::prev(it) <= i);
			return std::distance(boundaries.begin(), std::prev(it));
		};

		for(size_t i = st.lb(v); i <= st.rb(v); i++){
			const auto p = st.csa[i];
			size_t k = parse_id(p);
			coords[k].emplace_back(p, st.csa.bwt[i]);
		}

		bool emitted = false;

		for (size_t s = 0; s < num_seq; s++)
			std::sort(coords[s].begin(), coords[s].end());

		const auto dfs = [&](auto&& self, const std::vector<size_t>& l, const std::vector<size_t>& r) -> void {
			{
				size_t num_non_empty = 0;
				for (size_t s = 0; s < num_seq; s++)
					num_non_empty += (l[s] < r[s]);
				if (num_non_empty < match_threshold)
					return;
			}
			std::vector<size_t> mid(l.size());
			for (size_t s = 0; s < num_seq; s++)
				mid[s] = l[s] + (r[s] - l[s]) / 2;
			self(self, l, mid);

			{
				std::vector<index_t> crds(num_seq, MEM_t::NOT_PRESENT);
				char_type last_char{};
				bool first = true;
				bool left_max = false;
				size_t num = 0;
				for (size_t s = 0; s < num_seq; s++) {
					if (mid[s] >= r[s])
						continue;
					num++;
					crds[s] = coords[s][mid[s]].first;
					if (not left_max) {
						const auto c = coords[s][mid[s]].second;
						if (first) {
							first = false;
							last_char = c;
						} else
							left_max = last_char != c;
					}
					mid[s]++;
				}
				if (num < match_threshold)
					return;
				if (left_max) {
					const std::lock_guard<std::mutex> lock(emit_mutex);
					emit(len , std::move(crds));
					emitted = true;
				}
			}

			self(self, mid, r);
		};
		std::vector<size_t> l(num_seq), r(num_seq);
		for (size_t s = 0; s < num_seq; s++)
			r[s] = coords[s].size();
		dfs(dfs, l, r);

		return emitted;
	};

	//TRAVERSE SUBTREE BELOW V IN TOP DOWN MANNER USING DFS
	const auto dfs = [&](auto&& self, const node_type v) -> void {
		const size_t size = st.size(v);
		if (size < match_threshold)
			return; //IF INTERVAL IS ALREADY TO SMALL, DONT LOOK AT CHILDREN
		if (not test_node(v))
			return;
		for (const auto& c : st.children( v ))
			self(self, c);
	};

	const auto dfs_par = [&](auto&& self, const node_type v, const index_t par_depth) -> void {
		size_t size = st.size(v);
		if (size < match_threshold)
			return;
		if (not test_node(v))
			return;

		std::vector< node_type > children;
		for (const auto& c : st.children( v )) children.emplace_back(c);

		threadpool->for_each_in_range(children.size(), [&](const size_t c_i) {
			const auto& c = children[c_i];
			if (par_depth < 1) self(self, c, par_depth + 1);
			else dfs(dfs, c);
		});
	};

	if (threadpool)
		dfs_par(dfs_par, st.root(), 0);
	else
		dfs(dfs, st.root());
}

template<typename ST, typename F>
void mums_par(const boundaries_t& boundaries, const ST& st, const F& emit, const index_t length_threshold = 0){
	using node_type = typename ST::node_type;
	const index_t k = boundaries.size();

	std::mutex emit_mutex;

	const auto test_node = [&](node_type  v) -> void{
		size_t len = st.depth(v);
		if(len < length_threshold) return;
		std::vector<index_t> coords(boundaries.size(),MEM_t::NOT_PRESENT);
		size_t i = st.lb(v);
		assert(st.rb(v) - i + 1 == boundaries.size());
		auto left_ctx = st.csa.bwt[i];
		bool left_max = false;

		const auto parse_id = [&](const index_t& i) {
			const auto it = std::upper_bound( boundaries.begin(), boundaries.end(), i );
			assert(it != boundaries.begin());
			assert(it == boundaries.end() or *it > i);
			assert(*std::prev(it) <= i);
			return std::distance(boundaries.begin(), std::prev(it));
		};

		for(; i <= st.rb(v); i++){
			size_t k = parse_id( st.csa[i] );
			if  (coords[k] != MEM_t::NOT_PRESENT )	return; //NOT UNIQUE
			else	coords[k]  = st.csa[i];
			if (not left_max and st.csa.bwt[i] != left_ctx)
				left_max = true;
		}
		if(left_max) {
			const std::lock_guard<std::mutex> lock(emit_mutex);
			emit( len , std::move(coords));
		}
	};


	//TRAVERSE SUBTREE BELOW V IN TOP DOWN MANNER USING ITERATORS
	const auto& traverse_subtree_topdown = [&]( const node_type v ){
		using it_type = typename ST::const_iterator;
		for (auto it = it_type(&st,v,0,1); it != it_type(&st,v,1,1); ){
			size_t size = st.size(*it);
			if( size  > k || it.visit() > 1 ) {it++; continue;} // >k -> CANT BE MUM  // visit > 1 -> already tested
			if( size == k) test_node(*it);
			it.skip_subtree(); // -> size() <= k -> all children will be to small
		}
	};


	//TRAVERSE SUBTREE BELOW V IN TOP DOWN MANNER USING DFS
	const auto dfs = [&](auto&& self, const node_type v) -> void {
		size_t size = st.size(v);
		if     (size  < k) return;
		else if(size == k) test_node(v);
		else	for (const auto& c : st.children( v )) self(self, c);
	};


	//TRAVERSE SUBTREE BELOW V IN BOTTOM UP MANNER USING ITERATORS   <-- FASTEST, BUT NOT BY MUCH
	const auto& traverse_subtree_bottomup = [&]( const node_type v ){
		using it_type = typename ST::const_bottom_up_iterator;
		size_t size = st.size(v);
		if      (size  < k) return;
		else if (size == k) test_node(v);
		else	for (auto it = it_type(&st, st.leftmost_leaf(v)); it != it_type(&st,v); it++)
			if( st.size(*it) == k)
				test_node(*it);
	};

	const auto dfs_par = [&](auto&& self, const node_type v, const index_t par_depth) -> void {
		size_t size = st.size(v);
		if     (size  < k) return;
		else if(size == k) test_node(v);
		else {
			std::vector< node_type > children;
			for (const auto& c : st.children( v ))
				children.emplace_back(c);

			#pragma omp parallel for
			for (index_t c_i = 0; c_i < children.size(); c_i++){
				const auto& c = children[c_i];
				if  (par_depth < 1)	self(self, c, par_depth + 1);
				else 			dfs(dfs, c); //traverse_subtree_topdown(c); //dfs(dfs, c); //traverse_subtree_bottomup(c);
			}
		}
	};

	//dfs(dfs, st.root());
	dfs_par(dfs_par, st.root(), 0);
}



template<typename ST, typename F>
void mums_par(const boundaries_t& boundaries, const ST& st, const F& emit, const index_t length_threshold, const size_t match_threshold){
	std::cout << "MUM PAR" << std::endl;
	using node_type = typename ST::node_type;
	const index_t k = boundaries.size();
	const index_t k_min = match_threshold > k? k :  match_threshold;

	std::mutex emit_mutex;

	const auto test_node = [&](node_type  v){
		size_t len = st.depth(v);
		if(len < length_threshold) return false;
		std::vector<index_t> coords(boundaries.size(),MEM_t::NOT_PRESENT);
		size_t i = st.lb(v);
		auto left_ctx = st.csa.bwt[i];
		bool left_max = false;

		const auto parse_id = [&](const index_t& i) {
			const auto it = std::upper_bound( boundaries.begin(), boundaries.end(), i );
			assert(it != boundaries.begin());
			assert(it == boundaries.end() or *it > i);
			assert(*std::prev(it) <= i);
			return std::distance(boundaries.begin(), std::prev(it));
		};

		for(; i <= st.rb(v); i++){
			size_t k = parse_id( st.csa[i] );
			if  (coords[k] != MEM_t::NOT_PRESENT )	return false; //NOT UNIQUE
			else	coords[k]  = st.csa[i];
			if (not left_max and st.csa.bwt[i] != left_ctx)
				left_max = true;
		}
		if(left_max) {
			const std::lock_guard<std::mutex> lock(emit_mutex);
			emit( len , std::move(coords));
		}
		return left_max;
	};

	//TRAVERSE SUBTREE BELOW V IN TOP DOWN MANNER USING DFS
	const auto dfs = [&](auto&& self, const node_type v) -> void {
		size_t size = st.size(v);
		if     (size  < k_min)              return; //IF INTERVAL IS ALREADY TO SMALL, DONT LOOK AT CHILDREN
		else if(size <= k) if(test_node(v)) return; //IF INTERVAL IS ALREADY MUM, DONT LOOK AT CHILDREN
		for (const auto& c : st.children( v )) self(self, c);
	};

	const auto dfs_par = [&](auto&& self, const node_type v, const index_t par_depth) -> void {
		size_t size = st.size(v);
		if     (size  < k_min) return;
		else if(size  <= k) if(test_node(v)) return;

		std::vector< node_type > children;
		for (const auto& c : st.children( v )) children.emplace_back(c);

		#pragma omp parallel for
		for (index_t c_i = 0; c_i < children.size(); c_i++){
			const auto& c = children[c_i];
			if  (par_depth < 1)	self(self, c, par_depth + 1);
			else 			dfs(dfs, c);
		}
	};

	//dfs(dfs, st.root());
	dfs_par(dfs_par, st.root(), 0);
}

}
