#pragma once

#include <vector>
#include <iterator>
#include <type_traits>
#include <cassert>

template<typename It, typename F>
void radix_sort(It begin, It end, const F& q)
{
	using T = std::remove_cvref_t<decltype(q(*begin))>;
	static_assert(std::is_integral_v<T>, "type for radix sort must be integral");
	static_assert(std::is_unsigned_v<T>, "type for radix sort must be unsigned");

	const size_t n = std::distance(begin, end);
	if (n < 256)
	{
		std::sort(begin, end, [&](const auto& lhs, const auto& rhs) { return q(lhs) < q(rhs); });
		return;
	}

	std::vector< std::pair<size_t, T> > elems(n);
	for (size_t i = 0; i < n; i++)
		elems[i] = std::make_pair(i, q(begin[i]));
	std::vector< std::pair<size_t, T> > new_elems(n);

	for (size_t s = 0; s < sizeof(T); s++)
	{
		// count
		std::vector<size_t> cnt(256, 0);
		for (size_t i = 0; i < n; i++)
			cnt[ (elems[i].second >> (s*8)) & 255u ]++;

		// find start
		for (size_t s = 0, i = 0; i < cnt.size(); i++)
		{
			s += cnt[i];
			cnt[i] = s - cnt[i];
		}

		for (size_t i = 0; i < n; i++)
		{
			const size_t g = (elems[i].second >> (s*8)) & 255u;
			new_elems[cnt[g]++] = std::move(elems[i]);
		}

		std::swap(elems, new_elems);
	}

	// free new_elems
	{ auto tmp = std::move(new_elems); }

	std::vector old(std::make_move_iterator(begin), std::make_move_iterator(end));

	for (size_t i = 0; i < n; i++)
		begin[i] = std::move( old[elems[i].first] );

	assert(std::is_sorted(begin, end, [&](const auto& lhs, const auto& rhs) { return q(lhs) < q(rhs); }));
}
