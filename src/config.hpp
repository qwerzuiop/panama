#pragma once

#include "file_util.hpp"
#include <cassert>
#include <cstring>
#include <filesystem>
#include <map>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>
#include <omp.h>

struct option_parser_t {
	std::map<std::string, std::vector<const char*>> remaining_args;
	option_parser_t(int argc, const char** args)
	{
		while (argc-- > 0) {
			const std::string arg = *args++;
			if (arg.empty() or arg[0] != '-')
				throw std::invalid_argument(string_builder {} << std::quoted(arg) << ": option must start with -");
			std::vector<const char*> tmp;
			while (argc > 0) {
				const size_t l = strlen(*args);
				if (l > 0 and (*args)[0] == '-')
					break;
				tmp.emplace_back(*args++), argc--;
			}
			auto res = remaining_args.emplace(arg, std::move(tmp));
			if (not res.second)
				throw std::invalid_argument(string_builder {}
					<< std::quoted(arg) << " occured twice");
		}
	}
	bool exists(const std::string& key) const
	{
		return remaining_args.count(key) > 0;
	}
	void consume_flag(const std::string& key)
	{
		if (not exists(key))
			throw std::invalid_argument(string_builder {}
				<< std::quoted(key) << " missing");
		auto it = remaining_args.find(key);
		assert(it != remaining_args.end());
		if (not it->second.empty())
			throw std::invalid_argument(string_builder {}
				<< std::quoted(key) << " does not accept arguments");
		remaining_args.erase(it);
	}
	std::string consume_string(const std::string& key)
	{
		if (not exists(key))
			throw std::invalid_argument(string_builder {}
				<< std::quoted(key) << " missing");
		auto it = remaining_args.find(key);
		assert(it != remaining_args.end());
		if (it->second.size() != 1)
			throw std::invalid_argument(string_builder {}
				<< std::quoted(key) << " requires exactly one argument");
		std::string res = it->second[0];
		remaining_args.erase(it);
		return res;
	}
	template <typename T>
	T consume_int(const std::string& key)
	{
		auto s = consume_string(key);
#define TRY_CONSUME_INT(t, f)                                                                       \
	if constexpr (std::is_same_v<T, t>)                                                             \
		try {                                                                                       \
			return f(s);                                                                            \
		} catch (std::exception & e) {                                                              \
			throw std::invalid_argument(string_builder {}                                           \
				<< std::quoted(key) << " requires an argument of type " << #t << ": " << e.what()); \
		}                                                                                           \
	else
		TRY_CONSUME_INT(int, std::stoi)
		TRY_CONSUME_INT(long, std::stol)
		TRY_CONSUME_INT(long long, std::stoll)
		TRY_CONSUME_INT(unsigned long, std::stoul)
		TRY_CONSUME_INT(unsigned long long, std::stoull)
		static_assert(std::is_same_v<T, int>, "no parse function for type known");
#undef TRY_CONSUME_INT
	}
	void require_empty()
	{
		if (remaining_args.empty())
			return;
		throw std::invalid_argument(string_builder {}
			<< "unknown option: " << std::quoted(remaining_args.begin()->first));
	}
	~option_parser_t()
	{
		require_empty();
	}
	template <typename T>
	T parse_options(std::initializer_list<T> options, const std::string& val)
	{
		for (const T& x : options) {
			const std::string key = string_builder {} << x;
			if (key == val)
				return x;
		}
		throw std::invalid_argument(string_builder {}
			<< std::quoted(val) << " is not a valid option");
	}
};

enum MainAligner_t {
	HAlign3,
	FMAlign2,
	None
};
std::ostream& operator<<(std::ostream& out, const MainAligner_t ali)
{
	switch (ali) {
	case HAlign3:
		return out << "HAlign3";
	case FMAlign2:
		return out << "FMAlign2";
	case None:
		return out << "None";
	}
	assert(false);
	__builtin_unreachable();
}

struct general_config_t {
	const char* call = nullptr;
	std::string in_pref;
	std::string original_working_directory;
	unsigned long num_threads;
	general_config_t() = default;
	general_config_t(int& argc, const char**& argv)
	{
		num_threads = (std::thread::hardware_concurrency() + 1) / 2;
		omp_set_num_threads(num_threads);
		original_working_directory = std::filesystem::current_path();
		call = *argv++, argc--;
		if (argc < 1)
			throw std::invalid_argument("prefix of input is required");
		in_pref = *argv++, argc--;
	}
	void parse(option_parser_t& parser)
	{
		if (parser.exists("-t")) {
			num_threads = parser.consume_int<unsigned long>("-t");
			omp_set_num_threads(num_threads);
		}
	}
};
inline general_config_t config;
inline void only_general_config(int argc, const char** argv)
{
	config = general_config_t(argc, argv);
	option_parser_t parser(argc, argv);
	config.parse(parser);
}

struct fill_gaps_config_t {
	std::string tmp_directory;
	std::string fmalign_directory;
	std::string minimap_directory;
	MainAligner_t external_aligner = FMAlign2;

	fill_gaps_config_t() = default;
	fill_gaps_config_t(int argc, const char** argv)
	{
		config = general_config_t(argc, argv);
		option_parser_t parser(argc, argv);
		config.parse(parser);
		tmp_directory = config.original_working_directory;
		fmalign_directory = config.original_working_directory + "/FMAlign2";
		minimap_directory = config.original_working_directory + "/minimap2";
		if (parser.exists("-d"))
			tmp_directory = parser.consume_string("-d");
		if (parser.exists("-fmalign"))
			fmalign_directory = parser.consume_string("-fmalign");
		if (parser.exists("-minimap"))
			minimap_directory = parser.consume_string("-minimap");
		if (parser.exists("-main-aligner"))
			external_aligner = parser.parse_options({ FMAlign2, HAlign3, None }, parser.consume_string("-aligner"));
	}
};
inline fill_gaps_config_t gap_fill_config;

struct mem_finding_config_t {
	mem_finding_config_t() = default;
	size_t min_mem_len = 100;
	size_t max_count = 5;
	mem_finding_config_t(int argc, const char** argv)
	{
		config = general_config_t(argc, argv);
		option_parser_t parser(argc, argv);
		config.parse(parser);
		if (parser.exists("-memlen"))
			min_mem_len = parser.consume_int<unsigned long>("-memlen");
		if (parser.exists("-maxcount"))
			max_count = parser.consume_int<unsigned long>("-maxcount");
	}
};
inline mem_finding_config_t mem_finding_config;
