#pragma once

#include <cassert>
#include <tuple>
#include <charconv>
#include <cstdint>
#include <stdexcept>
#include <string_view>
#include <vector>
#include <sstream>

template <typename T>
T svtoT(std::string_view sv)
{
	T result{};
	auto [ptr, err] = std::from_chars(sv.data(), sv.data() + sv.size(), result);
	if (ptr != sv.data() + sv.size())
		throw std::invalid_argument("svtoT failed");
	return result;
}

enum SAM_FLAG_t : uint16_t {
	MULT_SEG = 0x1, // template has multiple segments in sequencing
	SEG_ALIGNED = 0x2, // each segment is properly aligned
	SEG_UNMAPPED = 0x4, // segment unmapped
	NEXT_SEG_UNMAPPED = 0x8, // next segment in the template unmapped
	SEQ_REV_COMPL = 0x10, // SEQ is reverse complemented
	NEXT_SEQ_COMPL = 0x20, // SEQ of the next segment in the template is reverse complemented
	FIRST_SEG = 0x40, // is first segment in template
	LAST_SEQ = 0x80, // is last segment in template
	SECONDARY = 0x100, // secondary alignment
	FILTERS_UNPASSED = 0x200, // not passed filters, e.g. platform/vendor quality control
	PCR_OR_DUPLICATE = 0x400, // PCR or optical duplicate
	SUPPL_ALIGNMENT = 0x800 // supplementary alignment
};
struct CIGAR_t {
	std::vector<std::pair<char, uint32_t>> seq;
	bool empty() const
	{
		return seq.empty();
	}
	CIGAR_t() = default;
	CIGAR_t(std::string_view str)
	{
		for (size_t i = 0; i < str.size();) {
			uint32_t num = 1;
			if (std::isdigit(str[i])) {
				for (num = 0; std::isdigit(str[i]); i++)
					num = 10 * num + (str[i] - '0');
			}
			assert(i < str.size());
			assert(not std::isdigit(str[i]));
			seq.emplace_back(str[i++], num);
		}
		assert(to_string() == str);
	}
	bool perfect() const
	{
		return seq.size() == 1 and seq[0].first == '=';
	}
	struct iterator_t {
		size_t remaining_blocks = 0;
		const std::pair<char, uint32_t>* ptr = nullptr;
		std::pair<char, uint32_t> cur{'\0', 0};
		bool operator==(const iterator_t& rhs) const
		{
			if (remaining_blocks == 0 and cur.second == 0)
				return rhs.remaining_blocks == 0 and cur.second == 0;
			return std::tie(remaining_blocks, ptr, cur) == std::tie(rhs.remaining_blocks, rhs.ptr, rhs.cur);
		}
		bool operator!=(const iterator_t& rhs) const
		{
			return not(*this == rhs);
		}
		char operator*()
		{
			return cur.first;
		}
		iterator_t& operator++()
		{
			if (0 == --cur.second and remaining_blocks > 0) {
				cur = *ptr++;
				remaining_blocks--;
			}
			return *this;
		}
	};
	iterator_t begin() const
	{
		auto res = iterator_t { seq.size(), seq.data(), std::make_pair(0, 0) };
		if (res.remaining_blocks > 0) {
			res.cur = *res.ptr++;
			res.remaining_blocks--;
		}
		return res;
	}
	iterator_t end() const
	{
		return iterator_t {};
	}
	std::string to_string() const {
		std::string res;
		bool first = true;
		std::pair<char, uint32_t> cur;
		for (char c : *this)
			if (first) {
				cur.first = c;
				cur.second = 1;
				first = false;
			} else if (c == cur.first)
				cur.second++;
			else {
				res += std::to_string(cur.second);
				res += cur.first;
				cur.first = c, cur.second = 1;
			}
		if (cur.second > 0) {
			res += std::to_string(cur.second);
			res += cur.first;
		}
		return res;
	}
};
struct SAM_alignment_t {
	std::string line;
	std::string_view qname; // query template name
	uint16_t flag; // bitwise flag
	std::string_view rname; // references sequence name
	size_t pos; // 0-based (because we translated)
	uint8_t mapq; // mapping quality (-10 * log_10(Prob that mapping is wrong))
	std::string_view cigar;
	std::string_view rnext; // reference name of mate/next read
	int pnext; // position of mate/next read
	size_t tlen; // template length
	std::string_view seq; // segment sequence
	std::string_view qual; // ASCII of phred-scaled base quality + 33
	SAM_alignment_t() = default;
	SAM_alignment_t(std::string&& str)
	{
		line = std::move(str);
		size_t p = 0;
		auto step = [&]() -> std::string_view {
			const size_t start = p;
			while (p < line.size() and line[p] != '\t')
				p++;
			auto res = std::string_view { line }.substr(start, p - start);
			if (p < line.size())
				p++; // skip '\t'
			return res;
		};
		qname = step();
		flag = svtoT<uint16_t>(step());
		rname = step();
		pos = svtoT<size_t>(step()) - 1;
		mapq = svtoT<uint8_t>(step());
		cigar = step();
		rnext = step();
		pnext = svtoT<int>(step());
		tlen = svtoT<size_t>(step());
		seq = step();
		qual = step();
	}
};

std::vector<SAM_alignment_t> parse_SAM(std::istream& in)
{
	std::vector<SAM_alignment_t> res;
	for (std::string line; std::getline(in, line);) {
		if (line.empty() or line[0] == '@')
			continue;
		SAM_alignment_t alignment(std::move(line));
		res.emplace_back(std::move(alignment));
	}
	return res;
}
std::vector<SAM_alignment_t> parse_SAM(std::string in)
{
	std::istringstream str(std::move(in));
	return parse_SAM(str);
}
