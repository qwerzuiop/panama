#include <iostream>
#include "common.hpp"
#include "system_timer.cpp"

using namespace std;


int main(int argc, char** argv){
	cout << "[1] prefix of input"  << endl;
	if(argc < 2) return 0;
	string in_pref = argv[1];
	cout << " =" << in_pref << endl;
	sys_timer st; st.start();
	vector<MEM_t> mems = load_mems_from_file(in_pref + FEX_CHAIN);
	auto   P = read_parses(in_pref);
	DICT_t D = load_dict_from_file(in_pref);
	std::ofstream blocks_out(in_pref+FEX_BLOCK, std::ios::out | std::ios::binary);
	size_t count = 0;

	for(const auto& mem : mems){
		my_int len = 0;
		for(my_int i = 0; i < mem.len; i++)
			len += D.get_length(P[mem.coords.front()+i]);
		BLOCK_T b(mem, len);
		blocks_out << b;
		count++;
		cout << "\r" << count << flush;
	}
	blocks_out.close();
	cout << "\nTime for transforming to blocks: " << st.stop_and_get() << endl;
	return 0;
}
