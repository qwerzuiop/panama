#pragma once

//for unordered map

// generic hash function
void hash_combine(size_t&) {}
template<typename T, typename... Rest>
void hash_combine(size_t& s, const T& v, const Rest&... rest)
{
	s ^= std::hash<T>{}(v) + 0x9e3779b9 + (s << 6) + (s >> 2);
	hash_combine(s, rest...);
}
// std::hash for std::pair
template<typename A, typename B>
struct std::hash< std::pair<A,B> > {
	size_t operator()(const std::pair<A,B>& p) const
	{
		size_t res = 0;
		hash_combine(res, p.first, p.second);
		return res;
	}
};
// std::hash for std::deque
template<typename T>
struct std::hash< std::deque<T> > {
	size_t operator()(const std::deque<T>& p) const
	{
		size_t res = 0;
		for (const auto& x : p)
			hash_combine(res, x);
		return res;
	}
};

