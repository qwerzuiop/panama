#include <iostream>
#include "common.hpp"
#include "config.hpp"
#include "system_timer.cpp"
#include <algorithm> //reverse


using namespace std;

/** @biref shifts the front of block b by off*/
bool shift_front(BLOCK_T & b, int off, const my_index_t & I){
	auto tmp = b; // copy for possible reset after failure
	b.length -= off;
	for(my_int i = 0; i < b.start_off.size(); i ++){
		if (not I.shift_coord(b.start_off[i], off)) {
			b = std::move(tmp);
			return false;
		}
	}
	return true;
}


/** @biref shifts the back of block b by off*/
void shift_back(BLOCK_T & b, int off){
	b.length += off;
}

enum col_check_result_e {
	NOT_EQUAL = 0,
	ALL_EQUAL = 1,
	ILLEGAL_POSITION = 2 // only when there is a gap (sentinel)
};

/** @biref checks if all starting symbols og block are equal*/
col_check_result_e all_first_equal( const BLOCK_T & b, my_index_t & I){
	if (I.sentinel_at(b.start_off.front())) [[unlikely]]
		return ILLEGAL_POSITION;
	const char c = I.get_char(b.start_off.front());
	for(uint32_t i = 1; i < b.start_off.size(); i++)
		if (I.sentinel_at(b.start_off[i])) [[unlikely]]
			return ILLEGAL_POSITION;
		else if (c != I.get_char(b.start_off[i]))
			return NOT_EQUAL;
	return ALL_EQUAL;
}

/** @biref checks if all ending symbols og block are equal*/
col_check_result_e all_last_equal(const BLOCK_T & b, my_index_t & I) {
	pair<my_int, my_int> coord = b.start_off.front();
	if (not I.shift_coord(coord,b.length-1)) [[unlikely]]
		return ILLEGAL_POSITION;
	const char c = I.get_char(coord);
	for(uint32_t i = 1; i < b.start_off.size(); i++){
		coord = b.start_off[i];
		if (I.sentinel_at(coord) or not I.shift_coord(coord, b.length-1)) [[unlikely]]
			return ILLEGAL_POSITION;
		else if (c != I.get_char(coord))
			return NOT_EQUAL;
	}
	return ALL_EQUAL;
}

/** @biref shifts front as long as letters are equal*/
size_t extend_blocks_front( BLOCK_T & block, limits_t & lim, my_index_t & I){
	assert(block.start_off.size()>0);
	int max_steps = lim.min_dist( limits_t(block, block_limit_e::FRONT,I ) );
	assert(max_steps >= 0);

	int steps = 0;
	while(steps < max_steps){
		steps++;
		if (not shift_front(block,-1,I)) {
			steps--;
			break;
		}
		if(all_first_equal(block,I) != ALL_EQUAL){ //shifted too far
			[[maybe_unused]] const bool res = shift_front(block,1,I);
			assert(res);
			steps--;
			break;
		}
	}
	return steps;
}

/** @biref shifts back as long as letters are equal*/
size_t extend_blocks_back( BLOCK_T & block, limits_t & lim, my_index_t & I){
	assert(block.start_off.size()>0);
	int max_steps = limits_t(block, block_limit_e::BACK ,I).min_dist(lim);
	assert(max_steps >= 0);
	int steps = 0;
	while(steps < max_steps){
		steps++;
		shift_back(block,+1);
		if(all_last_equal(block,I) != ALL_EQUAL){ //shifted too far
			shift_back(block,-1);
			steps--;
			break;
		}
	}
	return steps;
}

/** @biref returns minimum distance between two blocks*/
int min_dist(BLOCK_T & b1, BLOCK_T & b2, my_index_t & I){
	return limits_t(b1,block_limit_e::BACK,I).min_dist(limits_t(b2,block_limit_e::FRONT,I));
}

uint32_t find_offset_to_first_eq_col_before(BLOCK_T block, my_index_t & I, uint32_t max_steps, uint32_t max_gap_size){
	uint32_t shift = 0;
	if(!max_steps) return 0;

	if (not shift_front(block,-1,I))
		return 0;
	max_steps--;
	shift++;

	assert(all_first_equal(block,I) != ALL_EQUAL);//OTHERWISE INCORRECT BLOCK
	if (all_first_equal(block, I) == ILLEGAL_POSITION)
		return 0;

	while(max_steps && shift <= max_gap_size ){
		if (not shift_front(block,-1,I))
			return 0;
		max_steps--;
		shift++;

		if (auto res = all_first_equal(block,I); res == ILLEGAL_POSITION)
			return 0;
		else if (res == ALL_EQUAL)
			return shift;
	}
	return 0;
}

uint32_t find_offset_to_first_eq_col_after(BLOCK_T block, my_index_t & I, uint32_t max_steps, uint32_t max_gap_size){
	uint32_t shift = 0;
	if(!max_steps) return 0;
	shift_back(block,1);
	max_steps--;
	shift++;

	assert(all_last_equal(block,I) != ALL_EQUAL);//OTHERWISE INCORRECT BLOCK
	if (all_last_equal(block, I) == ILLEGAL_POSITION)
		return 0;

	while(max_steps && shift <= max_gap_size ){
		shift_back(block,1);
		max_steps--;
		shift++;

		if (auto res = all_last_equal(block,I); res == ILLEGAL_POSITION)
			return 0;
		else if (res == ALL_EQUAL)
			return shift;
	}
	return 0;
}

BLOCK_T block_before(BLOCK_T block, limits_t & lim, my_index_t & I, uint32_t max_gap_size, uint32_t min_block_size ){
	assert(block.start_off.size()>0);


	int max_steps = lim.min_dist( limits_t(block, block_limit_e::FRONT,I ) );

	uint32_t shift = find_offset_to_first_eq_col_before(block, I ,max_steps, max_gap_size);

	if(shift == 0 or not shift_front(block, -shift, I))
		return BLOCK_T(MEM_t(),0); //NO MORE BLOCK;

	block.length = 1;

	extend_blocks_front(block,lim,I);

	if(block.length < min_block_size) return BLOCK_T(MEM_t(),0); //NO MORE BLOCK;

	return block;
};

BLOCK_T block_after(BLOCK_T block, limits_t & lim, my_index_t & I, uint32_t max_gap_size, uint32_t min_block_size ){
	assert(block.start_off.size()>0);
	int max_steps = limits_t(block, block_limit_e::BACK ,I).min_dist(lim);

	uint32_t shift = find_offset_to_first_eq_col_after(block, I ,max_steps, max_gap_size);

	if(shift == 0) return BLOCK_T(MEM_t(),0); //NO MORE BLOCK;

	shift_back(block,shift);

	if (not shift_front(block, (block.length-1) ,I))
		return BLOCK_T(MEM_t(), 0);

	assert(block.length == 1);

	extend_blocks_back(block,lim,I);

	if(block.length < min_block_size) return BLOCK_T(MEM_t(),0); //NO MORE BLOCK;

	return block;
};


/** \return vector of >adjacent< blocks before block and behind lim*/
vector<BLOCK_T> blocks_before(BLOCK_T block, limits_t & lim, my_index_t & I, uint32_t max_gap_size, uint32_t min_block_size){
	vector<BLOCK_T> blocks;
	while(true){
		block =  block_before(block,lim,I,max_gap_size,min_block_size);
		if(block.length > 0) //TODO VALID
			blocks.push_back(block);
		else
			break;
	}
	std::reverse(blocks.begin(), blocks.end());
	return blocks;
}

/** \return vector of >adjacent< blocks after block and before lim*/
vector<BLOCK_T> blocks_after(BLOCK_T block, limits_t & lim, my_index_t & I, uint32_t max_gap_size, uint32_t min_block_size){

	vector<BLOCK_T> blocks;
	while(true){
		block =  block_after(block,lim,I,max_gap_size,min_block_size);
		if(block.length > 0) //TODO VALID
			blocks.push_back(block);
		else
			return blocks;
	}
}




int main(int argc, const char** argv){
	cout << "Assumes existens of " << FEX_parse << " "<< FEX_parseB<< " "<< FEX_dictx << " "<< FEX_BLOCK << endl;
	cout << "Extendeds blocks horitontally if possible" << endl;
	cout << "[1] prefix of input"  << endl;
	only_general_config(argc, argv);

	cout << "loading parse, chain, dict .."  << flush;
	sys_timer st; st.start();
	my_index_t I(config.in_pref);
	cout << "\rTime for loading parse, dict " << st.stop_and_get() << endl;

	st = sys_timer(); st.start();

	//READ BLOCKS
	ifstream blocks_in(config.in_pref + FEX_BLOCK);
	vector<BLOCK_T> B;
	while(true){
		BLOCK_T b(blocks_in);
		if(!blocks_in) break;
		B.push_back(std::move(b));
	}
	blocks_in.close();


	vector<BLOCK_T> new_B;

	// ------------ PAREMTER FOR NEW BLOCKS -----------------
	uint32_t max_gap_size		= 1;
	uint32_t min_new_block_size	= 10;
	// ------------------------------------------------------

	// EXTEND BLOCKS
	for(uint32_t i = 0; i < B.size(); i++){
		limits_t f_lim = (i==0)            ? limits_t(block_limit_e::FRONT,I) : limits_t(new_B.back(),block_limit_e::BACK,I);
		limits_t b_lim = (i == B.size()-1) ? limits_t(block_limit_e::BACK,I)  : limits_t(B[i+1],block_limit_e::FRONT,I);

		extend_blocks_front(B[i],f_lim,I);
		extend_blocks_back(B[i],b_lim,I);

		vector<BLOCK_T> Bb = blocks_before(B[i], f_lim, I, max_gap_size, min_new_block_size);
		vector<BLOCK_T> Ba = blocks_after (B[i], b_lim, I, max_gap_size, min_new_block_size);


		new_B.insert(new_B.end(), std::make_move_iterator(Bb.begin()), std::make_move_iterator(Bb.end()));
		new_B.emplace_back(std::move(B[i]));
		new_B.insert(new_B.end(), std::make_move_iterator(Ba.begin()), std::make_move_iterator(Ba.end()));

		cout << "\r" << i << flush;
	}

	//WRITE BLOCKS
	ofstream blocks_out(config.in_pref + FEX_BLOCK);
	for(const auto& b : new_B) blocks_out<<b;
	blocks_out.close();

	cout << "\nTime for finding new blocks " << st.stop_and_get() << endl;
	cout << "Added blocks count " <<  (new_B.size()-B.size() ) << endl;
	return 0;
}
