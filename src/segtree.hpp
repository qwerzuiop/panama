#pragma once

#include <vector>
#include <functional>

template<typename T, T unit = T{}>
class segtree_t {
	size_t n;
	std::vector< T > m_s;
	std::function<T(const T& l, const T& r)> m_f;
public:
	segtree_t() = default;
	template<typename F>
	segtree_t(size_t n, F&& f) : n(n), m_s(2 * n, unit), m_f(std::forward<F>(f)) {}
	const T& get_cur(size_t i) const
	{
		return m_s[i + n];
	}

	void update(size_t i, const T& val)
	{
		for (m_s[i += n] = val; i /= 2; )
			m_s[i] = m_f(m_s[i*2], m_s[i*2+1]);
	}
	// query [b,e)
	T query(size_t b, size_t e) const
	{
		assert(e <= n and b < n);
		T ra = unit, rb = unit;
		for (b += n, e += n; b < e; b /= 2, e /= 2)
		{
			if (b % 2 != 0) ra = m_f(ra, m_s[b++]);
			if (e % 2 != 0) rb = m_f(m_s[--e], rb);
		}
		return m_f(ra, rb);
	}
	// query [0,e)
	T query(size_t e) const
	{
		assert(e <= n);
		T ra = unit, rb = unit;
		size_t b = n;
		for (e += n; b < e; b /= 2, e /= 2)
		{
			if (b % 2 != 0) ra = m_f(ra, m_s[b++]);
			if (e % 2 != 0) rb = m_f(m_s[--e], rb);
		}
		return m_f(ra, rb);
	}
};


template<typename T, T unit = T{}>
class fenwick_tree_t {
	size_t n;
	std::vector< T > fwt;
	std::function<T(const T& l, const T& r)> m_f;
	#ifndef NDEBUG
	segtree_t<T,unit> m_check;
	#endif
public:
	fenwick_tree_t() = default;
	template<typename F>
	fenwick_tree_t(size_t n, F&& f)
		: n(n)
		, fwt(n, unit)
		, m_f(std::forward<F>(f))
		#ifndef NDEBUG
		, m_check(n, m_f)
		#endif
	{}

	// applies a[i] = f(a[i], val) 
	void update(size_t i, const T& val)
	{
		#ifndef NDEBUG
		m_check.update(i, val);
		#endif
		if (i == 0)
		{
			fwt[0] = m_f(fwt[0], val);
			return;
		}
		for ( ; i < n; i += i&-i)
		{
			fwt[i] = m_f(fwt[i], val);
		}
	}
	// query [0..i)
	T query(const size_t i) const
	{
		if (i == 0)
			return unit;
		T res = fwt[0];
		for (size_t p = i-1; p > 0; p &= p-1)
		{
			res = m_f(res, fwt[p]);
		}
		assert(res == m_check.query(i));
		return res;
	}
};
