#pragma once

#include <algorithm>
#include <cassert>
#include <limits>
#include <list>
#include <sdsl/int_vector.hpp>
#include <unordered_set>
#include <vector>

#include "common.hpp"

namespace MGA {
// not actually from MGA, but using their algorithm

using char_t = uint32_t;
using position_set = std::vector<std::pair<my_int, char_t>>; // (position i, left context = S[i-1])
using position_partition = std::vector<position_set>;

inline void
validate_mems(const std::vector<MEM_t>& mems, const std::vector<char_t>& parse)
{
	for (const auto& [l, coords] : mems) {
		bool left_maximal = false, right_maximal = false;
		// check left/right maximality
		for (index_t i = 0; i < coords.size(); i++) {
			left_maximal |= (coords[i] == 0);
			if (i > 0 and not left_maximal)
				left_maximal |= (parse[coords[i] - 1] != parse[coords[i - 1] - 1]);

			right_maximal |= (coords[i] + l >= parse.size());
			if (i > 0 and not right_maximal)
				right_maximal |= (parse[coords[i] + l] != parse[coords[i - 1] + l]);
		}
		assert(left_maximal and right_maximal);

		// check equality
		for (index_t i = 1; i < coords.size(); i++) {
			for (index_t k = 0; k < l; k++)
				assert(parse[coords[i - 1] + k] == parse[coords[i] + k]);
		}
	}
}

template <typename F, typename A, typename B>
void emit_mems(const size_t num_seq, F&& emit, const my_int len, A&& a, B&& b, size_t& remaining_emits)
{
	std::vector<my_int> res(num_seq);
	std::vector<char_t> left_ctx(num_seq);
	std::vector<std::array<std::pair<bool, std::unordered_set<char_t>>, 3>> impossible_vis(num_seq);
	const auto go = [&](auto&& self, const uint8_t SRC, const bool left_maximal, const size_t i) -> bool {
		if (remaining_emits == 0)
			return false;

		assert(i <= num_seq);
		if (i == num_seq) {
			if (SRC == 3 and left_maximal) {
				remaining_emits--;
				emit(len, static_cast<const std::vector<my_int>&>(res));
				return true;
			}
			return false;
		}

		if (i > 0) {
			assert(SRC > 0);
			if (left_maximal and impossible_vis[i][SRC - 1].first)
				return false;
			else if (not left_maximal and impossible_vis[i][SRC - 1].second.contains(left_ctx[i - 1]))
				return false;
		}


		bool success = false;
		for (uint8_t from = 1; from <= 2; from++) {
			char_t last_c = 0; // init value doesn't matter because last_success is true if initialized properly
			bool last_success = true;
			for (const auto& [s, c] : (from == 1 ? a(i) : b(i))) {
				res[i] = s;
				left_ctx[i] = c;

				const bool new_left_max = i == 0 ? s == 0 : (left_maximal or left_ctx[i] != left_ctx[i - 1]);

				if (not last_success and last_c == c)
					continue;

				last_c = c;
				last_success = self(self,
					SRC | from,
					new_left_max,
					i + 1);
				success |= last_success;
			}
		}
		if (not success and i > 0) {
			if (left_maximal)
				impossible_vis[i][SRC - 1].first = true;
			else
				impossible_vis[i][SRC - 1].second.emplace(left_ctx[i - 1]);
		}
		return success;
	};
	go(go, 0, false, 0);
}
template <typename F>
void emit_mems(F&& emit, const my_int len, const position_partition& a, const position_partition& b, size_t& remaining_emits)
{
	assert(a.size() == b.size());
	// if there is no position in one sequence, there is no MEM
	for (size_t i = 0; i < a.size(); i++)
		if (a[i].empty() and b[i].empty())
			return;
	emit_mems(
		a.size(),
		emit,
		len,
		[&](size_t i) { return a[i]; },
		[&](size_t i) { return b[i]; },
		remaining_emits);
}
template <typename F>
void emit_mems(F&& emit, const my_int len, const position_partition& a, const position_partition& b, size_t& remaining_emits, const size_t min_coverage)
{
	assert(a.size() == b.size());
	std::vector<size_t> non_empty;
	for (size_t i = 0; i < a.size(); i++)
		if (not a.empty() or not b.empty()) {
			non_empty.emplace_back(i);
		} else if (non_empty.size() + a.size() - i - 1 < min_coverage) {
			return;
		}
	assert(non_empty.size() >= min_coverage);
	std::vector<my_int> tmp(a.size(), MEM_t::NOT_PRESENT);
	emit_mems(
		non_empty.size(),
		[&](my_int len, const std::vector<my_int>& coords) {
			for (size_t i = 0; i < non_empty.size(); i++)
				tmp[non_empty[i]] = coords[i];
			emit(len, static_cast<const std::vector<my_int>&>(tmp));
		},
		len,
		[&](size_t i) { return a[non_empty[i]]; },
		[&](size_t i) { return b[non_empty[i]]; },
		remaining_emits);
}

std::atomic_uint64_t num_visited;

template <typename ST, typename F, typename Q>
void find_mems(
	const boundaries_t& boundaries,
	const ST& st,
	const Q& true_len,
	const F& emit,
	const index_t length_threshold,
	const index_t max_count)
{
	using node_type = typename ST::node_type;
	const my_int k = boundaries.size();

	// map index in concatenated parses to index of parse it stems from
	const auto parse_id = [&](const my_int& i) {
		const auto it = std::upper_bound(boundaries.begin(), boundaries.end(), i);
		assert(it != boundaries.begin());
		assert(it == boundaries.end() or *it > i);
		assert(*std::prev(it) <= i);
		return std::distance(boundaries.begin(), std::prev(it));
	};

	const auto handle_leaf = [&](const node_type v) -> position_partition {
		position_partition me(k);

		const auto sa_i = st.lb(v);
		const auto i = st.csa[sa_i]; // == st.sn(v);

		me[parse_id(i)].emplace_back(i, st.csa.bwt[sa_i]);
		return me;
	};

	std::mutex emit_mutex;
	const auto dfs = [&](auto&& self, const node_type v, const my_int p_depth, my_int w_lcp) -> position_partition {
		if (st.is_leaf(v)) {
			if (w_lcp < length_threshold)
				return position_partition {};
			return handle_leaf(v);
		}

		const auto l = st.depth(v); // longest common prefix of two prefixes from different children
		const auto repr = st.csa[st.lb(v)]; // some suffix from this subtree

		for (index_t i = p_depth; i < l and w_lcp < length_threshold; i++)
			w_lcp += true_len(repr + i);

		position_partition me;
		if (w_lcp >= length_threshold)
			me.resize(k);

		size_t max_emits = max_count + 1;
		std::vector<MEM_t> emitted;

		for (const auto& c : st.children(v)) {
			auto p_c = self(self, c, l, w_lcp);

			if (w_lcp >= length_threshold) {
				emit_mems([&](const auto& l, const auto& x) {
					emitted.emplace_back(l, x);
				},
					l, me, p_c, max_emits);

				for (index_t i = 0; i < k; i++) { // O(k)
					// me[i].splice( me[i].end(), std::move(p_c[i]) ); // O(1)
					// a linear insert is fine here, we iterate over me[i] anyway
					// and position_set as vector is much more cache-efficient
					me[i].insert(me[i].end(), p_c[i].begin(), p_c[i].end());
					// sort by character for last-char optimization in emit_mems
					std::sort(me[i].begin(), me[i].end(), [&](const auto& lhs, const auto& rhs) -> bool {
						return lhs.second < rhs.second;
					});
					auto tmp = std::move(p_c[i]); // clear p_c
				}
			}
		}
		if (w_lcp >= length_threshold)
			num_visited.fetch_add(1, std::memory_order_relaxed);
		if (max_emits > 0) {
			const std::lock_guard<std::mutex> lock(emit_mutex);
			for (auto& [l, ds] : emitted)
				emit(l, std::move(ds));
		}
		return me;
	};

	{ // parallel walk over root
		std::vector<node_type> children;
		for (const auto& c : st.children(st.root()))
			children.emplace_back(c);
#pragma omp parallel for
		for (const auto& c : children)
			dfs(dfs, c, 0, 0);
	}
}

template <typename ST, typename F, typename Q>
void find_mems(
	const boundaries_t& boundaries,
	const ST& st,
	const Q& true_len,
	const F& emit,
	const index_t coverage_threshold,
	const index_t length_threshold,
	const index_t max_count)
{
	using node_type = typename ST::node_type;
	const my_int k = boundaries.size();

	// map index in concatenated parses to index of parse it stems from
	const auto parse_id = [&](const my_int& i) {
		const auto it = std::upper_bound(boundaries.begin(), boundaries.end(), i);
		assert(it != boundaries.begin());
		assert(it == boundaries.end() or *it > i);
		assert(*std::prev(it) <= i);
		return std::distance(boundaries.begin(), std::prev(it));
	};

	const auto handle_leaf = [&](const node_type v) -> position_partition {
		position_partition me(k);

		const auto sa_i = st.lb(v);
		const auto i = st.csa[sa_i]; // == st.sn(v);

		me[parse_id(i)].emplace_back(i, st.csa.bwt[sa_i]);
		return me;
	};

	std::mutex emit_mutex;
	const auto dfs = [&](auto&& self, const node_type v, const my_int p_depth, my_int w_lcp) -> position_partition {
		if (st.is_leaf(v)) {
			if (w_lcp < length_threshold)
				return position_partition {};
			return handle_leaf(v);
		}

		const auto l = st.depth(v); // longest common prefix of two prefixes from different children
		const auto repr = st.csa[st.lb(v)]; // some suffix from this subtree

		for (index_t i = p_depth; i < l and w_lcp < length_threshold; i++)
			w_lcp += true_len(repr + i);

		position_partition me;
		if (w_lcp >= length_threshold)
			me.resize(k);

		size_t max_emits = max_count + 1;
		std::vector<MEM_t> emitted;

		for (const auto& c : st.children(v)) {
			auto p_c = self(self, c, l, w_lcp);

			if (w_lcp >= length_threshold) {
				emit_mems([&](const auto& l, const auto& x) {
					emitted.emplace_back(l, x);
				},
					l, me, p_c, max_emits, coverage_threshold);

				for (index_t i = 0; i < k; i++) { // O(k)
					// me[i].splice( me[i].end(), std::move(p_c[i]) ); // O(1)
					// a linear insert is fine here, we iterate over me[i] anyway
					// and position_set as vector is much more cache-efficient
					me[i].insert(me[i].end(), p_c[i].begin(), p_c[i].end());
					// sort by character for last-char optimization in emit_mems
					std::sort(me[i].begin(), me[i].end(), [&](const auto& lhs, const auto& rhs) -> bool {
						return lhs.second < rhs.second;
					});
					auto tmp = std::move(p_c[i]); // clear p_c
				}
			}
		}
		if (w_lcp >= length_threshold)
			num_visited.fetch_add(1, std::memory_order_relaxed);
		if (max_emits > 0) {
			const std::lock_guard<std::mutex> lock(emit_mutex);
			for (auto& [l, ds] : emitted)
				emit(l, std::move(ds));
		}
		return me;
	};

	{ // parallel walk over root
		std::vector<node_type> children;
		for (const auto& c : st.children(st.root()))
			children.emplace_back(c);
#pragma omp parallel for
		for (const auto& c : children)
			dfs(dfs, c, 0, 0);
	}
}

} // namespace MGA
