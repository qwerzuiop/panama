#include "pfp.cpp"
using namespace std;



int main(int argc, char** argv){
	if(argc < 5){
		cout << "[1] window size" << endl;
		cout << "[2] modul"  << endl;
		cout << "[3] prefix for output"  << endl;
		cout << "[4] ... [n] input files"  << endl;
		cout << "---" << endl;
		cout << "output is written in files " << FEX_parse << " " << FEX_parseB << " " << FEX_dict <<  " " << FEX_dictx << endl;
		exit(0);
	}

	int w = atoi(argv[1]);
	int p = atoi(argv[2]);
	string out_pref = argv[3];
	vector<string> input_files(0);
	for(int i = 4; i < argc; i++) input_files.emplace_back(argv[i]);
	return build_parse(w,p,out_pref,input_files);
}



